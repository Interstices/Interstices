module.exports = {
	// TODO: env setting should be per package, since some packages are only meant to run on node while others
	//       are only meant to run on browser, while others can run on both.
	env: {
		es2021: true,
		node: true,
		browser: true,
	},
	extends: [
		'xo',
		'xo-react',
	],
	rules: {
		'brace-style': ['error', '1tbs', { allowSingleLine: true }],
		// We allow 2-statement lines like new Promise(resolve => { server.listen(3000, resolve); });
		'max-statements-per-line': ['error', { max: 2 }],
		'object-curly-spacing': ['error', 'always'],
		'react/react-in-jsx-scope': 'off',
		'react/prop-types': 'off', // TODO: enforce prop-types
		'no-return-await': 'off', // See https://github.com/eslint/eslint/issues/12246 and https://github.com/standard/standard/issues/1442
	},
	ignorePatterns: ['**/dist/**'],
};
