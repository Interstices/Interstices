Interstices
===============

An object-oriented architecture for [linked-data](https://en.wikipedia.org/wiki/Linked_data) files and applications.

Setup
-------

Run `npx lerna init --independent` under project root.
Then run `npx lerna bootstrap --hoist --strict` under project root.
