import { required, ListFormat } from '@interstices/base-formats';

export const TestFolderFormat = {
	type: 'Folder',
	name: required(String),
	children: required(ListFormat),
	timestamp: required(Number),
};

export const TestTextFragmentFormat = {
	type: 'TextFragment',
	name: required(String),
	content: required(String),
	timestamp: required(Number),
};

export const TestFolderTreeFormat = {
	type: 'FolderTree',
	rootFolder: required(TestFolderFormat),
	createdTS: required(Number),
};

// TODO: use files/fragments to specify formats, and we won't need to install them manually like this.
export default function installTestFormats(scope) {
	return scope.addFormats([TestFolderFormat, TestTextFragmentFormat, TestFolderTreeFormat]);
}
