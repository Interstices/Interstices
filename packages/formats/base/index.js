
/**
 * Currently all formats are expressed in plain javascript, and all validation is done at runtime.
 * TODO: generate typescript definitions.
 * TODO: can we use any existing javascript libraries for defining schemas? Eg protobufs? GraphQL? React Proptypes?
 *
 * Format fields are allowed to take the following values:
 * 	* `String`
 * 	* `Boolean`
 * 	* `Number`
 * 	* `Any`
 * 	* some other format
 *
 * The `type` field is special and is always a string literal.
 */

export const enumType = (...formats) => formats; // Convert to array.
export const required = fieldVal => fieldVal;
export const optional = fieldVal => Array.isArray(fieldVal) ? enumType(...fieldVal, null) : enumType(fieldVal, null);

export const Any = { // Utility data-type, similar to typescripts "any"
	type: 'Any',
};

export const METADATA_SUBJECT_KEY = 'subjectFragment';
// Base template for all metadata formats (like ListMetadataFormat)
export const MetadataFormat = {
	type: 'Mapping',
	[METADATA_SUBJECT_KEY]: required(Any),
};

// Lists formats

// IMPORTANT: Right now `head`, `next` and `value` are hard-coded iteration keys used in base-list-handler
//            and fast-sets-handler. Custom iteration keys are NOT SUPPORTED!
const ListItemFormat = {
	type: 'ListItem',
	next: null, // `null` here is just a placeholder before we assign it a real value right afterwards.
	value: required(Any),
};
ListItemFormat.next = optional(ListItemFormat);

export { ListItemFormat };

// Should these formats extend some sort of base class/type?
export const ListFormat = {
	type: 'List',
	head: optional(ListItemFormat),
};

export const ListMetadataFormat = {
	type: 'ListMetadata',
	[METADATA_SUBJECT_KEY]: required(ListFormat),
	tail: optional(ListItemFormat),
};

export const ListItemMetadataFormat = {
	type: 'ListItemMetadata',
	[METADATA_SUBJECT_KEY]: required(ListItemFormat),
	previous: optional(ListItemFormat),
};

// Client Forking

export const ExternalLinkMetadata = {
	type: 'EXTERNAL_FRAG_LINK_METADATA',
	[METADATA_SUBJECT_KEY]: required(Any),
	forkedFrom: required(Any),
};

export const ClientForkMetadata = {
	type: 'ClientForkMetadata',
	[METADATA_SUBJECT_KEY]: required(Any),
	fork: required(Any),
};

// Todo Lists

export const TodoItemContentFormat = {
	type: 'TodoItemContent',
	completed: required(Boolean),
	content: optional(String),
};

// Same structure as ListItemFormat.
const TodoItemFormat = {
	type: 'TodoItem',
	next: null,
	value: required(TodoItemContentFormat),
};
TodoItemFormat.next = optional(TodoItemFormat);

export { TodoItemFormat };

// Same structure as ListFormat.
export const TodoListFormat = {
	type: 'TodoList',
	head: optional(TodoItemFormat),
};

export const TodoFormat = {
	type: 'Todo',
	title: required(String),
	items: required(TodoListFormat),
};

export const TodoListMetadataFormat = {
	type: 'TodoListMetadata',
	[METADATA_SUBJECT_KEY]: required(TodoListFormat),
	tail: optional(TodoItemFormat),
};

export const TodoItemMetadataFormat = {
	type: 'TodoItemMetadata',
	[METADATA_SUBJECT_KEY]: required(TodoItemFormat),
	previous: optional(TodoItemFormat),
};

export const ListFormatGroup = {
	list: ListFormat,
	item: ListItemFormat,
	listMetadata: ListMetadataFormat,
	itemMetadata: ListItemMetadataFormat,
};

export const TodoListFormatGroup = {
	list: TodoListFormat,
	item: TodoItemFormat,
	listMetadata: TodoListMetadataFormat,
	itemMetadata: TodoItemMetadataFormat,
};

// Sets internally use a list

const SetItemFormat = {
	type: 'SetItem',
	next: null, // `null` here is just a placeholder before we assign it a real value right afterwards.
	value: required(Any),
};
SetItemFormat.next = optional(SetItemFormat);

export { SetItemFormat };

// Should these formats extend some sort of base class/type?
export const SetFormat = {
	type: 'Set',
	head: optional(SetItemFormat),
};

export const SetMetadataFormat = {
	type: 'SetMetadata',
	[METADATA_SUBJECT_KEY]: required(SetFormat),
	tail: optional(SetItemFormat),
};

export const SetItemMetadataFormat = {
	type: 'SetItemMetadata',
	[METADATA_SUBJECT_KEY]: required(SetItemFormat),
	previous: optional(SetItemFormat),
};

export const SetFormatGroup = {
	list: SetFormat,
	item: SetItemFormat,
	listMetadata: SetMetadataFormat,
	itemMetadata: SetItemMetadataFormat,
};

// Forking

export const FORK_MAPPING_KEY = 'fork';

export const ForkMapping = {
	type: 'ForkMapping',
	[METADATA_SUBJECT_KEY]: required(Any),
	[FORK_MAPPING_KEY]: required(Any),
};

// Session Backup

export const ClientSessionBackupMapping = {
	type: 'CLIENT_SESSION_BACKUP_MAPPING',
	[METADATA_SUBJECT_KEY]: required(Any),
	backup: required(Any),
};

// TODO: use files/fragments to specify formats, and we won't need to install them manually like this.
export default function installBaseFormats(scope) {
	return scope.addFormats([
		Any,
		ListFormat,
		ListItemFormat,
		ListMetadataFormat,
		ListItemMetadataFormat,
		ExternalLinkMetadata,
		ClientForkMetadata,
		TodoItemContentFormat,
		TodoItemFormat,
		TodoListFormat,
		TodoFormat,
		TodoListMetadataFormat,
		TodoItemMetadataFormat,
		SetFormat,
		SetItemFormat,
		SetMetadataFormat,
		SetItemMetadataFormat,
		ForkMapping,
		ClientSessionBackupMapping,
	]);
}
