import Button from '@mui/material/Button';

export default () => (
	<Button variant="contained" color="primary">
		Hello World
	</Button>
);
