import { useCallback, useContext, useMemo } from 'react';
import { IntersticesContext, hooks as baseHooks } from '@interstices/base-ui';
import List, { useList } from '@interstices/list';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import TextField from '@mui/material/TextField';
import { TodoItemContentFormat, TodoItemFormat, TodoListFormat, TodoListMetadataFormat, TodoItemMetadataFormat } from '@interstices/base-formats';
const { useFragmentContent, useDetachedContent } = baseHooks;

const LIST_FORMATS = {
	list: TodoListFormat,
	item: TodoItemFormat,
	listMetadata: TodoListMetadataFormat,
	itemMetadata: TodoItemMetadataFormat,
};

const TodoListEmptyComponent = () => <i>Nothing here...</i>;

const TodoListComponent = ({ root }) => {
	const scope = useContext(IntersticesContext);
	const { create } = scope.API.getAll();

	const { loading, content, error } = useFragmentContent(root);
	const { listHandler, loading: loadingList, error: loadListError } = useList(content?.items, LIST_FORMATS);
	const context = useMemo(() => ({ listHandler }), [listHandler]);

	const transformProps = useCallback(
		// Provide a key for each component https://reactjs.org/docs/reconciliation.html#recursing-on-children
		({ value, listNode }) => ({ fragment: value, key: value.getIdString(), context, listNode }),
		[context],
	);

	const addItem = useCallback(
		async () => {
			if (!listHandler) {
				return;
			}

			const todoItem = await create({ type: TodoItemContentFormat.type, completed: false, content: '' });
			listHandler.push(todoItem);
		},
		[listHandler, create],
	);

	if (loading || loadingList) {
		return (
			<div>loading...</div>
		);
	}

	if (error || loadListError || !content || !listHandler) {
		return (
			<pre>{JSON.stringify(error, null, 2) || 'Unable to render Todo list root.'}</pre>
		);
	}

	return (
		<div style={{ padding: '16px' }}>
			<h1>{content.title}</h1>
			<List
				container={content.items}
				ItemComponent={TodoItem}
				EmptyComponent={TodoListEmptyComponent}
				transformProps={transformProps}/>
			<Button variant="outlined" color="primary" onClick={addItem}>Add item</Button>
		</div>
	);
};

const TodoItem = ({ fragment, context, listNode }) => {
	const { content: fragmentContent, loading, update } = useDetachedContent(fragment);
	const { listHandler } = context;

	const onTextChange = useCallback(event => {
		update({ content: event.target.value });
	}, [update]);

	const onToggle = useCallback(() => {
		update({ completed: !fragmentContent.completed });
	}, [fragmentContent, update]);

	const onRemove = useCallback(() => {
		listHandler.remove(listNode);
	}, [listHandler, listNode]);

	if (loading) {
		return <p>loading...</p>;
	}

	if (!fragmentContent) {
		return <pre>unable to load fragment content</pre>;
	}

	const style = {
		minWidth: '100px',
	};

	return (
		<div style={style}>
			<Checkbox checked={fragmentContent.completed} onChange={onToggle}/>
			<TextField variant="standard" value={fragmentContent.content} onChange={onTextChange}/>
			<button type="button" onClick={onRemove}>X</button>
		</div>
	);
};

export default TodoListComponent;
