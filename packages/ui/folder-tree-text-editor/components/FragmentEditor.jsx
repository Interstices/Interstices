import { useCallback } from 'react';
import { hooks } from '@interstices/base-ui';
const { useDetachedContent } = hooks;

const FragmentEditor = ({ fragment }) => {
	const { content: fragmentContent, loading, update } = useDetachedContent(fragment);

	const onTextAreaChange = useCallback(event => {
		update({ content: event.target.value, timestamp: Date.now() });
	}, [update]);

	if (loading) {
		return <p>loading...</p>;
	}

	if (!fragmentContent) {
		return <pre>unable to load fragment content</pre>;
	}

	const style = {
		width: 'calc(100% - 2*16px)',
		minHeight: '200px',
	};

	return (
		<div>
			<h2>{fragmentContent.name}</h2>
			<textarea value={fragmentContent.content} style={style} onChange={onTextAreaChange}/>
		</div>
	);
};

export default FragmentEditor;
