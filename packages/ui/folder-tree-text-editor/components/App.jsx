import { useState } from 'react';
import { DirectoryTree } from './DirectoryTree.jsx';
import FragmentEditor from './FragmentEditor.jsx';

const App = ({ root }) => {
	const [activeFragment, setActiveFragment] = useState(null);

	const context = { activeFragment, setActiveFragment };

	return (
		<>
			<h1>Interstices</h1>
			<p>Click on any fragment to view and edit it.</p>
			{root && <DirectoryTree fragment={root} context={context}/>}
			{activeFragment && <FragmentEditor fragment={activeFragment} context={context}/>}
		</>
	);
};

export default App;
