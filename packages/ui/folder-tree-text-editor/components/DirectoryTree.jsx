import { useMemo, useCallback } from 'react';
import { hooks as baseHooks } from '@interstices/base-ui';
import { TestFolderFormat } from '@interstices/test-formats';
import List from '@interstices/list';
const { useFragmentContent } = baseHooks;

const FolderEmptyComponent = () => <i>Nothing here...</i>;

const FolderNode = ({ context, content }) => {
	const { timestamp, name, children } = content;

	const displayDate = useMemo(() => new Date(timestamp).toString(), [timestamp]);

	const transformProps = useCallback(
		// Provide a key for each component https://reactjs.org/docs/reconciliation.html#recursing-on-children
		({ value }) => ({ fragment: value, key: value.getIdString(), context }),
		[context],
	);

	return (
		<div style={{ borderLeft: '1px solid black' }}>
			<div>
				<strong>{name}</strong><span style={{ color: 'grey' }}>{displayDate}</span>
			</div>
			<div style={{ padding: '16px' }}>
				<List
					container={children}
					ItemComponent={DirectoryTree}
					EmptyComponent={FolderEmptyComponent}
					transformProps={transformProps}/>
			</div>
		</div>
	);
};

const FragmentNode = ({ context, fragment, content }) => {
	const { activeFragment, setActiveFragment } = context;
	const { timestamp, name } = content;
	const displayDate = useMemo(() => new Date(timestamp).toString(), [timestamp]);
	const style = {
		cursor: 'pointer',
		color: activeFragment && fragment.equals(activeFragment) ? '#4521e6' : 'black',
	};

	return (
		<div style={style} onClick={() => setActiveFragment(fragment)}>
			<strong>{name}</strong><span style={{ color: 'grey' }}>{displayDate}</span>
		</div>
	);
};

const DirectoryTree = ({ context, fragment }) => {
	const { loading, content, error } = useFragmentContent(fragment);

	if (loading) {
		return (
			<div>loading...</div>
		);
	}

	if (error || !content) {
		return (
			<pre>{JSON.stringify(error, null, 2) || 'folder or fragment is undefined'}</pre>
		);
	}

	return (
		content.type === TestFolderFormat.type
			? <FolderNode folder={fragment} content={content} context={context}/>
			: <FragmentNode fragment={fragment} content={content} context={context}/>
	);
};

export {
	FolderNode,
	FragmentNode,
	DirectoryTree,
};

export default DirectoryTree;
