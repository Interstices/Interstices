
// Box shadow borders are useful if you have a dynamic border (eg border on hover),
// because the box shadow will not affect the size or shift the content.
// This is also useful if you want to align bordered elements with non-bordered elements.
// With box-shadow borders, as long as each element has the same font size and line height,
// then they will have the same height.
// The only downside is that you can't do things like dashed borders.
export const boxShadowBorder = (size, color) => `
	border: none;
	box-shadow: inset 0 0 0 ${size}px ${color};
`;

export const fontStyle = (theme, style) => `
	font-size: ${theme.fontSizes[style]};
	font-family: ${theme.fontFamilies[style]};
	line-height: ${theme.lineHeights[style]}
`;
