
const PARAGRAPH = 'p'; // Paragraphs, content, body
const HEADING = 'h1'; // Titles, headings
const BLOCK = 'block'; // Buttons, inputs

export const STYLES = { PARAGRAPH, HEADING, BLOCK };

export const light = {
	colors: {
		primary: '#4521e6',
	},
	fontSizes: {
		[HEADING]: '32px',
		[PARAGRAPH]: '16px',
		[BLOCK]: '16px',
	},
	lineHeights: {
		[HEADING]: '2',
		[PARAGRAPH]: '1.4',
		[BLOCK]: '1.6',
	},
	fontFamilies: {
		[HEADING]: 'sans-serif',
		[PARAGRAPH]: 'sans-serif',
		[BLOCK]: 'sans-serif',
	},
};
