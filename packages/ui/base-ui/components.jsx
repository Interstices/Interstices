import styled from '@emotion/styled';
import { boxShadowBorder, fontStyle } from './mixins';
import { STYLES } from './themes';
const { PARAGRAPH, HEADING, BLOCK } = STYLES;

export const Box = styled.div`
`;

export const Button = styled.button`
	${({ theme }) => fontStyle(theme, BLOCK)};
	margin: 0 8px;
	border: none;
	padding: 2px 14px;
	border-radius: 2px;
	cursor: pointer;
`;

export const PrimaryButton = styled(Button)`
	background-color: ${({ theme }) => theme.colors.primary};
	color: white;

	&:hover{
		background-color: white;
		color: ${({ theme }) => theme.colors.primary};
		${({ theme }) => boxShadowBorder(1, theme.colors.primary)};
	}
`;

export const Input = styled.input`
	${({ theme }) => fontStyle(theme, BLOCK)};
	${({ theme }) => boxShadowBorder(1, theme.colors.primary)}
	border-radius: 2px;
	padding: 2px 6px;
	padding-right: 0; /* to make it obvious when text overflows */
`;

export const Page = styled.div`
	margin: 0;
	padding: 16px;
	position: relative;
`;

export const Text = styled.span`
	${({ theme }) => fontStyle(theme, PARAGRAPH)};
`;

export const Paragraph = styled.p`
	${({ theme }) => fontStyle(theme, PARAGRAPH)};
`;

export const Table = styled.table``;

export const TableBody = styled.tbody``;

export const TableRow = styled.tr``;

export const TableCell = styled.td`
	${({ theme }) => fontStyle(theme, BLOCK)};
`;

export const Row = styled(Box)`
	display: flex;
	flex-flow: row nowrap;
`;

export const CircularButton = styled(Button)`
	width: 32px;
	height: 32px;
	background-color: ${({ theme }) => theme.colors.primary};
	color: white;
	border-radius: 50%;
	font-size: 18px;
	line-height: 32px;
	padding: 0;
	margin: 0;
	text-align: center;
`;
