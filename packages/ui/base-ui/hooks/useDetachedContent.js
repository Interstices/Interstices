/**
 * This hook is generally used for text inputs and text areas.
 * We decouple the input value from the fragment because writing to fragments
 * is asynchronous, and we need updates to the text area to be synchronous,
 * because React doesn't like asynchronous changes to text inputs
 * (See https://dev.to/kwirke/solving-caret-jumping-in-react-inputs-36ic)
 *
 * However this makes it very difficult to incorporate external changes,
 * eg if a text value is being edited by a collaborator at the same time.
 * Not to mention if the user is writing to a local store eg using LokiJS core,
 * writes are so fast that they are basically synchronous.
 *
 * So in the future we may just have to manually control the caret position so
 * it doesn't jump around when we make asynchronous updates to the text value.
 */

import { useCallback, useEffect, useContext, useState, useRef } from 'react';
import { IntersticesContext } from '@interstices/base-ui';

// TODO: error handling
function useDetachedContent(fragment) {
	// We don't use the usecontent hook here because we don't need to subscribe
	// to changes, since the input value is decoupled from the fragment content anyways.
	const [loading, setLoading] = useState(true);
	const [content, setContent] = useState(null);

	const scope = useContext(IntersticesContext);
	const { read, update } = scope.API.getAll();
	const workingFragmentRef = useRef(null); // Use a ref so we don't trigger un-necessary re-renders

	useEffect(() => {
		(async () => {
			const workingFragment = workingFragmentRef.current;
			// Detect when we change to a different fragment with a different id
			if (!workingFragment || !fragment.equals(workingFragment)) {
				workingFragmentRef.current = fragment;
				setLoading(true);
				const result = await fragment.$(read);
				setContent(result);
				setLoading(false);
			}
		})();
	}, [fragment]);

	const writeToBoth = useCallback(newValues => {
		const newContent = { ...content, ...newValues };
		setContent(newContent);
		const workingFragment = workingFragmentRef.current;
		update(workingFragment, newContent);
	}, [content, update]);

	return { content, loading, update: writeToBoth };
}

export default useDetachedContent;
