import useFragmentContent from './useFragmentContent.js';
import useDetachedContent from './useDetachedContent.js';

export {
	useFragmentContent,
	useDetachedContent,
};
