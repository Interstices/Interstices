import IntersticesContext from '../interstices-context.js';
import { useState, useEffect, useContext } from 'react';

function areItemsEqual(scope, fragment1, fragment2) {
	const { isFragment } = scope.API.getAll();

	if (isFragment(fragment1) && isFragment(fragment2)) {
		return fragment1.equals(fragment2);
	}

	return fragment1 === fragment2;
}

function areArraysEqual(scope, arr1, arr2) {
	return Array.isArray(arr1)
		&& Array.isArray(arr2)
		&& arr1.length === arr2.length
		&& arr1.filter((val, index) => !areItemsEqual(scope, val, arr2[index])).length === 0;
}

/**
 * Fetches the result of an async function, and re-fetches if the function or arguments change.
 * Modeled after Apollo's useQuery
 *
 * @param {*} asyncFn
 * @param {*} args
 * @param {*} skip if true, asyncFn will not be called. Once false, will call asyncFn again if it or args has changed.
 * @param {*} responsive if true, will return loading once asyncFn or args changes. If false, will return stale output until new output is available.
 * @returns {object} an object containing { loading, request, result, error }
 */
function useAsyncFn(asyncFn, args, skip, responsive = false) {
	const scope = useContext(IntersticesContext);

	const [result, setResult] = useState(null);
	const [error, setError] = useState(null);

	const [fulfilledRequest, setFulfilledRequest] = useState();

	const fulfilledRequestArr = fulfilledRequest && [fulfilledRequest.asyncFn, ...fulfilledRequest.args];
	const currentRequestArr = [asyncFn, ...args];
	const isResultStale = !areArraysEqual(scope, currentRequestArr, fulfilledRequestArr);

	// TODO: handle unmount before asyncFn finishes
	//       See https://www.digitalocean.com/community/tutorials/how-to-handle-async-data-loading-lazy-loading-and-code-splitting-with-react#step-2-%E2%80%94-preventing-errors-on-unmounted-components
	useEffect(() => {
		if (skip) {
			return;
		}

		if (!isResultStale) {
			// Current request is already fulfilled. This can happen if skip was toggled
			// on and off without changing the requested asyncFn or args
			return;
		}

		// TODO: batch all concurrent set_ calls https://medium.com/swlh/react-state-batch-update-b1b61bd28cd2
		asyncFn(...args)
			.then(res => {
				setResult(res);
				setError(null);
			}).catch(err => {
				console.error(err);
				setResult(null);
				setError(err);
			})
			.finally(() => {
				setFulfilledRequest({ asyncFn, args });
			});
	}, [asyncFn, ...args, skip]);

	if (responsive) {
		return isResultStale
			? { loading: true, request: null, result: null, error: null }
			: { loading: false, request: fulfilledRequest, result, error };
	}

	return { loading: !fulfilledRequest, request: fulfilledRequest, result, error };
}

/**
 * @param {*} fragment
 * @param {*} responsive if true, will report loading when re-fetching. If false, will return
 *                       stale output while re-fetching. You can check if output is stale
 *                       by checking if input fragment and output fragment are equal.
 * @returns an object containing { loading, content, contentSrc, error }
 */
function useFragmentContent(fragment, responsive = false) {
	const scope = useContext(IntersticesContext);
	const { read, isFragment } = scope.API.getAll();

	const [subscribedFragment, setSubscribedFragment] = useState(false);
	const [updatedContent, setUpdatedContent] = useState(null); // From subscription
	const [updatedContentSrc, setUpdatedContentSrc] = useState(null); // What fragment the updatedContent corresponds to

	const dontFetch = subscribedFragment !== fragment; // Wait until subscription started before fetching the fragment

	const {
		loading: initialContentLoading,
		request: initialContentSrc,
		result: initialContent,
		error: initialContentError,
	} = useAsyncFn(read, [fragment], dontFetch, responsive);

	useEffect(() => {
		if (!isFragment(fragment)) {
			return;
		}

		const listener = async ({ fragment: changedFragment, content }) => {
			console.log(`fragment changed ${changedFragment.getIdString()}`);
			// TODO: batch all concurrent set_ calls https://medium.com/swlh/react-state-batch-update-b1b61bd28cd2
			if (content) { // If core update() emits the updated fragment content, use it
				setUpdatedContent(content);
				setUpdatedContentSrc(changedFragment);
			} else {
				// TODO: maybe we can save a db call if we just use combine the current
				//       updatedContent with changedValues (which is also provided to the
				//       listener, see core-pub-sub plugin). Though that might be unreliable.
				//       Also in order to use updatedContent inside this useEffect() block,
				//       we would have to use another ref so we don't have to re-subscribe
				//       every time updatedContent changes (like how we do with fragmentRef).
				setUpdatedContent(await changedFragment.$(read));
				setUpdatedContentSrc(changedFragment);
			}
		};

		const unsubscribeFn = scope.API.getAll().subscribeToFragment(fragment, listener);
		setSubscribedFragment(fragment);
		return unsubscribeFn;
	}, [fragment]);

	if (updatedContent && isFragment(fragment) && isFragment(updatedContentSrc) && updatedContentSrc.equals(fragment)) {
		// If updatedContent exists and isn't stale
		return { content: updatedContent, contentSrc: updatedContentSrc, loading: false, error: null };
	}

	if (!initialContentLoading) {
		// If initial content is fresh, or we don't care if initial content is fresh (aka non-responsive mode)
		return { content: initialContent, contentSrc: initialContentSrc, loading: false, error: initialContentError };
	}

	// Responsive mode and both updated content and initial content are stale
	return { content: null, contentSrc: null, loading: true, error: null };
}

export default useFragmentContent;
