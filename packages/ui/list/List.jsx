import { hooks as baseHooks } from '@interstices/base-ui';
const { useFragmentContent } = baseHooks;

// These are pulled from base-list-handler/index.js
// but normally these shouldn't be used directly, we should be using
// the functions ListPlugin.getNext() and ListPlugin.getHead().
// How do we reconcile ListPlugin's functional API with our client-side reactive hooks API?
const HEAD_KEY = 'head';
const NEXT_KEY = 'next';
const VALUE_KEY = 'value';

export const List = ({ container, ItemComponent, EmptyComponent = null, transformProps = props => props }) => {
	const { loading, content, error } = useFragmentContent(container);

	if (loading) {
		return (
			<div>loading...</div>
		);
	}

	if (error || !content) {
		return (
			<pre>{JSON.stringify(error, null, 2) || 'unable to render list'}</pre>
		);
	}

	if (!content[HEAD_KEY]) {
		return <EmptyComponent/>; // No items
	}

	return (
		<ListItem item={content[HEAD_KEY]} ItemComponent={ItemComponent} transformProps={transformProps}/>
	);
};

export const ListItem = ({ item, ItemComponent, transformProps = props => props }) => {
	const { loading, content, error } = useFragmentContent(item);

	if (loading) {
		return (
			<div>loading...</div>
		);
	}

	if (error || !content) {
		return (
			<pre>{JSON.stringify(error, null, 2) || 'unable to render list item'}</pre>
		);
	}

	if (content[NEXT_KEY]) {
		return (
			<>
				<ItemComponent {...transformProps({ value: content[VALUE_KEY], listNode: item })}/>
				<ListItem item={content[NEXT_KEY]} ItemComponent={ItemComponent} transformProps={transformProps}/>
			</>
		);
	}

	return <ItemComponent {...transformProps({ value: content[VALUE_KEY], listNode: item })}/>;
};

export default List;
