import { useEffect, useContext, useState } from 'react';
import { IntersticesContext } from '@interstices/base-ui';

function useList(fragment, formats) {
	const scope = useContext(IntersticesContext);
	const { loadList } = scope.API.getAll();

	const [listHandler, setListHandler] = useState(null);
	const [error, setError] = useState(null);
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		// `loading` will only be true during the first list load.
		// If fragment changes, stale output will still be returned until new output is computed.
		(async () => {
			if (!fragment) {
				return;
			}

			try {
				setListHandler(await loadList(fragment, formats));
			} catch (e) {
				setError(e);
			} finally {
				setLoading(false);
			}
		})();
	}, [fragment, formats, loadList]);

	return { listHandler, loading, error };
}

export default useList;
