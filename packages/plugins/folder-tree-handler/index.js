/**
 * TODO: add description
**/

// TODO: move these to the base-formats package after we have confirmed that folder-tree-handler works properly.
const FOLDER_TYPE = 'Folder';
const PARENT_FOLDER_METADATA = 'ParentFolderMetadata';
const PARENT_FOLDER_KEY = 'parentFolder';

class FolderTreeHandler {
	rootFolder;
	API;

	constructor(scope) {
		this.API = scope.API;
	}

	/**
	 * Make sure to call init() after construction!
	 * We recommend using FolderTreePlugin.create or FolderTreePlugin.load
	 * instead of constructing FolderTreeHandler instances directly.
	 */
	async init(rootFolder) {
		if (this.rootFolder) {
			throw Error('FolderTreeHandler.init() should not be called after FolderTreeHandler is already initialized!');
		}

		this.rootFolder = rootFolder;
	}

	/**
	 * If destFolder is null, will place item under root folder
	 */
	async moveItem(item, destFolder = this.rootFolder) {
		const { readMapping, writeMapping, readProperty, loadList } = this.API.getAll();

		if (item.equals(this.rootFolder)) {
			throw new Error('Cannot move root folder');
		}

		const metadata = await readMapping(PARENT_FOLDER_METADATA, item);
		if (metadata) {
			// Undefined behavior is item isn't actually under the folder listed in the item's metadata.
			const currFolder = await metadata.$(readProperty, PARENT_FOLDER_KEY);
			const currFolderChildren = await loadList(await currFolder.$(readProperty, 'children'));
			await currFolderChildren.remove(item);
		}

		const destFolderChildren = await loadList(await destFolder.$(readProperty, 'children'));
		await destFolderChildren.push(item);
		await writeMapping(PARENT_FOLDER_METADATA, item, { [PARENT_FOLDER_KEY]: destFolder });
	}

	/**
	 *
	 * @param {*} parentFolder if null, new folder will be placed under root folder
	 */
	async createFolder(parentFolder, name) {
		const { createList, createFragment } = this.API.getAll();

		const children = await createList();
		const folder = await createFragment({
			type: FOLDER_TYPE,
			timestamp: Date.now(),
			name,
			children: children.getListContainer(),
		});

		await this.moveItem(folder, parentFolder);
		return folder;
	}

	async renameFolder(folder, name) {
		const { update } = this.API.getAll();

		await update(folder, { name });
	}
}

/**
 * Creates a new folder tree and returns a new handler for it.
 */
async function createFolderTree(scope, name) {
	const { createList, createFragment } = scope.API.getAll();

	const children = await createList();
	const rootFolder = await createFragment({
		type: FOLDER_TYPE,
		timestamp: Date.now(),
		name,
		children: children.getListContainer(),
	});
	return await loadFolderTree(scope, rootFolder);
}

/**
 * @param {*} rootFolder
 * @returns A handler for manipulating and traversing the folder tree.
 */
async function loadFolderTree(scope, rootFolder) {
	const folderTree = new FolderTreeHandler(scope);
	await folderTree.init(rootFolder);
	return folderTree;
}

export default function installFolderTreeHandler(scope) {
	scope.API.addAll({
		createFolderTree: createFolderTree.bind(null, scope),
		loadFolderTree: loadFolderTree.bind(null, scope),
	});
}
