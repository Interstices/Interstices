/**
 * Forking is when we copy a "source" structure to create a "destination" structure.
 * This handler contains some low level APIs used to help with this process.
 */

import { ForkMapping, FORK_MAPPING_KEY } from '@interstices/base-formats';
import { FragmentMap } from '@interstices/fragment';

// Works like lodash mapValues but supports async
async function asyncMapValues(obj, asyncFn) {
	const entriesPromises = Object.entries(obj).map(
		async ([key, val]) => [key, await asyncFn(val)],
	);
	return Object.fromEntries(await Promise.all(entriesPromises));
}

// TODO: support in-memory option, which doesn't use any metadata mappings and instead uses in-memory FragmentMap.
class BaseForkHandler {
	scope;
	sourceRoot;
	targetRoot;
	inMemory = false;
	sourceToDestFragMap;
	forkMetadataFormat;
	forkMetadataKey;

	/**
	 * For now, we allow sourceRoot to be a function that replaces the isSourceFragment function.
	 * Likewise for destRoot.
	 */
	constructor(scope, sourceRoot, destRoot, inMemory, forkMetadataFormat, forkMetadataKey) {
		this.bindScope(scope);
		if (typeof sourceRoot === 'function') {
			this.isSourceFragment = fragment => sourceRoot(this.scope, fragment);
		} else {
			this.sourceRoot = sourceRoot;
		}

		if (typeof destRoot === 'function') {
			this.isForkFragment = fragment => destRoot(this.scope, fragment);
		} else {
			this.destRoot = destRoot;
		}

		if (inMemory) {
			this.inMemory = true;
			this.sourceToDestFragMap = new FragmentMap();
		} else {
			if (!forkMetadataFormat) {
				throw Error('Base Fork Handler constructed with persistent mappings enabled but no fork metadata format provided.');
			}

			this.forkMetadataFormat = forkMetadataFormat;
			this.forkMetadataKey = forkMetadataKey;
		}
	}

	bindScope(scope) {
		this.scope = scope;
	}

	async isSourceFragment(fragment) {
		throw Error('unimplemented');
	}

	async isForkFragment(fragment) {
		// TODO: there are two ways to do this. Either we can add dest -> source mappings.
		// Or we can query under the fork root.
		throw Error('unimplemented');
	}

	async getSourceFragment(forkFragment) {
		throw Error('unimplemented'); // TODO: figure out if it's worth adding more mappings for dest -> source.
	}

	async getForkFragment(sourceFragment) {
		if (this.inMemory) {
			return this.sourceToDestFragMap.get(sourceFragment);
		}

		const { read, readMapping } = this.scope.API.getAll();

		const existingMapping = await readMapping(this.forkMetadataFormat, sourceFragment);
		if (existingMapping) {
			const { [this.forkMetadataKey]: fork } = await existingMapping.$(read);
			return fork;
		}

		return null;
	}

	/**
	 * Does not check if the fork was already created!
	 * It's expected that the caller handles this themselves.
	 *
	 * @param {*} sourceFragment the fragment in the source collection that we want to fork
	 * @param {*} skipContent can be used if we are going to immediately rewrite the content afterwards
	 * @returns the new fragment forked from the source fragment
	 */
	async createFork(sourceFragment, skipContent) {
		const { create, read, writeMapping } = this.scope.API.getAll();

		const content = skipContent ? undefined : await read(sourceFragment);
		const forkFragment = await create(content);

		if (this.inMemory) {
			this.sourceToDestFragMap.set(sourceFragment, forkFragment);
		} else {
			await writeMapping(this.forkMetadataFormat, sourceFragment, { [this.forkMetadataKey]: forkFragment });
		}

		return forkFragment;
	}

	/**
	 * If any value references a source fragment that has a fork, we want to re-map that
	 * reference to point to the fork instead. Any references to fragments outside the source collection will be preserved.
	 * @returns an object containing `transformedValues` and `transformed`
	 */
	async transformValues(values) {
		const { isFragment } = this.scope.API.getAll();
		// Keep track of whether any values needed transformation. Used in rewriteValues().
		let transformed = false;

		// If any value references a source fragment that has a fork, we want to re-map that
		// reference to point to the fork instead.
		// Any references to non-source fragments will be preserved.
		const transformedValues = await asyncMapValues(values, async value => {
			if (!isFragment(value) || !this.isSourceFragment(value)) {
				return value;
			}

			const forkedValue = await this.getForkFragment(value);
			if (forkedValue) {
				transformed = true;
				return forkedValue;
			}

			return value;
		});

		return { transformedValues, transformed };
	}

	/**
	 * Updates the content of the fork fragment from the source fragment, re-writing any references
	 * to source fragments that have corresponding fork fragments.
	 *
	 * @param {*} changedValues if you know what values in the source changed since the fork content
	 *                          was last updated, you can provide it here as an optimization.
	 */
	async updateForkContent(sourceFragment, changedValues) {
		const { read, update, overwrite } = this.scope.API.getAll();
		const forkFragment = await this.getForkFragment(sourceFragment);

		if (changedValues) {
			const { transformedValues } = await this.transformValues(changedValues);
			return await update(forkFragment, transformedValues);
		}

		const sourceContent = await sourceFragment.$(read);
		const { transformedValues } = await this.transformValues(sourceContent);
		return await overwrite(forkFragment, transformedValues);
	}

	/**
	 * Re-writes any references to source fragments that have corresponding fork fragments.
	 * Only use this function if you trust that the fork fragment's content already matches the
	 * source fragment (aside from any references that were re-written).
	 */
	async rewriteContent(forkFragment) {
		const { read, update } = this.scope.API.getAll();
		const content = await forkFragment.$(read);
		const { transformedValues, transformed } = await this.transformValues(content);
		if (transformed) {
			await update(forkFragment, transformedValues);
		}
	}
}

/**
 * @param {*} root A fragment with the LIST_TYPE type.
 * @returns A handler for manipulating and traversing the list.
 */
function loadForkRootSync(scope, sourceRoot, destRoot, inMemory = false, forkMetadataFormat = ForkMapping, forkMetadataKey = FORK_MAPPING_KEY) {
	const forkHandler = new BaseForkHandler(scope, sourceRoot, destRoot, inMemory, forkMetadataFormat, forkMetadataKey);
	return forkHandler;
}

// TODO: add the concept of a "managed" fork, where only the fork-handler is allowed to write to fork files.
//       To do this, we extend write so that they throw an error if another plugin tries to write to a fork fragment.
//       This way we can ensure that the fork content is always represents a snapshot of the source content.
//       However plugins like the client auto-fork plugin, are un-managed forks because after the client forks a file
//       they are free to modify the fork however they want.
// TODO: declare dependencies. Currently we depend on API methods read(), update(), overwrite(), and writeMappng()
//       if not using in-memory mappings.
export default function installBaseForkHandler(scope) {
	return scope.extendAPI({
		loadForkRootSync,
	});
}
