/**
 * Utilities for writing and reading mappings.
 * Much faster than doing it yourself using raw read/write operations
 * (because using raw read/writes, you would have to read and iterate through the
 * entire collection of mappings instead of using a query like we do here).
 */
import { METADATA_SUBJECT_KEY } from '@interstices/base-formats';

async function getFirstAsyncItem(asyncIterator) {
	return (await asyncIterator.next())?.value;
}

/**
 * Mapping fragments look like any other fragment, just a set of key-value pairs.
 * However the core property of mapping fragments is that there is a special "subject fragment", and
 * there can only be one mapping fragment for every subject fragment (at least for a given mapping type).
 *
 * If a mapping already exists, any attributes that are not overwritten will be preserved.
 * Similar to how WritePlugin.update() preserves existing properties.
 *
 * Example usage: MappingPlugin.writeMapping('EXIF_METADATA', imageFragment, { gps: ..., camera_model: ...  });
 *
 * IMPORTANT! While there is usually only supposed to be one mapping per subject fragment,
 * we do not account for race conditions here.
 *
 * @param {object} mappingFormat fragment format for the mapping
 * @param {IsxFragment} subject the fragment we are attaching the attribute to
 * @param {*} attributes an object containing key-value pairs we want the subject fragment to map to
 */
async function writeMapping(scope, mappingFormat, subject, attributes) {
	// TODO: synchronize function so that if we have multiple writeMapping calls to same fragment,
	//       only the first one will create a new mapping
	const { queryRootSet, create, update } = scope.API.getAll();
	if (attributes[METADATA_SUBJECT_KEY]) {
		throw Error(`${METADATA_SUBJECT_KEY} is a reserved field for mapping fragments.`);
	}

	// TODO: make sure mappings don't get garbage-collected.
	const queryResult = await getFirstAsyncItem(
		await queryRootSet({ type: mappingFormat.type, [METADATA_SUBJECT_KEY]: subject }, null, 1));
	if (queryResult?.fragment) {
		// Modify existing mapping
		await update(queryResult.fragment, attributes);
	} else {
		// No existing mapping, write a new one.
		await create({ type: mappingFormat.type, [METADATA_SUBJECT_KEY]: subject, ...attributes });
	}
}

/**
 * A utility function to optimize fetching an individual mapping.
 * If you don't want to use this function, you can always fetch the entire mapping collection
 * and iterate through it yourself.
 *
 * Only returns a single mapping
 *
 * For example, if we wanted to find the exif metadata for some image fragment, we would use
 *     readMapping(EXIF_METADATA, imageFragment), and it would return a mapping fragment, which looks like:
 *     { type: EXIF_METADATA, subject: imageFragment, ...attributes }
 *
 * IMPORTANT: there can only be at most one mapping fragment for a given mapping type and subject fragment.
 *
 * TODO: should we also return metadata content as well, because we already have it anyways,
 * so we can save the caller from making extra calls? If so, we should only return
 * primitive values (strings, numbers), because we should never be exposing raw fragment pointers
 * or ids outside the core.
 *
 * // TODO: better handling if no mapping found
 * @param {object} mappingFormat fragment format for the mapping
 * @param {IsxFragment} subject the subject fragment to query for
 * @returns {IsxFragment} the matching mapping fragment (if found)
 */
async function readMapping(scope, mappingFormat, subject) {
	// TODO: query primary set, not root set.
	const { queryRootSet } = scope.API.getAll();

	const queryResult = await getFirstAsyncItem(
		await queryRootSet({ type: mappingFormat.type, [METADATA_SUBJECT_KEY]: subject }, null, 1));
	return queryResult?.fragment;
}

// TODO: extend write/read so that they throw an error if another plugin tries to call write/read on a mapping fragment.
//       All writes/reads to mapping fragments should use writeMapping and readMapping, so that we can enforce the rule
//       that there should only be a single mapping for every subject file + mapping format.
export default function installMappingPlugin(scope) {
	return scope.extendAPI({
		writeMapping,
		readMapping,
	});
}
