import { FragmentMap } from '@interstices/fragment';

// TODO: look into using Zen Observables

/**
 * Since this pub-sub is in-memory, if you are running multiple processes (maybe REST server and a GraphQL server),
 * they won't see the change events that the other published.
 * This should be fine, since we assume that every Interstices system "owns" their datastore,
 * and that no other system is reading/writing to it at the same time. So instead of running
 * separate process for a REST server and a GraphQL server, you should install both as plugins
 * to a single Interstices process.
 */
class InMemoryPubSub {
	eventListeners = {};
	fragmentListeners = new FragmentMap();

	/**
	 * @param {string} eventName
	 * @param {function} listener
	 * @returns the function to unsubscribe the listener
	 */
	subscribeToEvent(scope, eventName, listener) {
		if (!this.eventListeners[eventName]) {
			this.eventListeners[eventName] = new Set();
		}

		this.eventListeners[eventName].add(listener);

		// Return unsubscribe function
		return () => {
			if (!this.eventListeners[eventName]) {
				return false;
			}

			return this.eventListeners[eventName].delete(listener);
		};
	}

	publishEvent(scope, eventName, data) {
		if (this.eventListeners[eventName]) {
			for (const listener of this.eventListeners[eventName].values()) {
				listener(data);
			}
		}
	}

	subscribeToFragment(scope, fragment, listener) {
		if (!this.fragmentListeners.get(fragment)) {
			this.fragmentListeners.set(fragment, new Set());
		}

		this.fragmentListeners.get(fragment).add(listener);

		// Return unsubscribe function
		return () => {
			if (!this.fragmentListeners.get(fragment)) {
				return false;
			}

			const wasRemoved = this.fragmentListeners.get(fragment).delete(listener);
			if (this.fragmentListeners.get(fragment).size === 0) {
				this.fragmentListeners.delete(fragment);
			}

			return wasRemoved;
		};
	}

	publishFragmentChanged(scope, fragment, data) {
		if (this.fragmentListeners.has(fragment)) {
			for (const listener of this.fragmentListeners.get(fragment).values()) {
				listener(data);
			}
		}
	}
}

export default function installPubSubPlatform(scope) {
	const pubsub = new InMemoryPubSub(); // PubSub system should be shared across all scopes
	return scope.extendAPI({
		subscribeToEvent: pubsub.subscribeToEvent.bind(pubsub),
		publishEvent: pubsub.publishEvent.bind(pubsub),
		subscribeToFragment: pubsub.subscribeToFragment.bind(pubsub),
		publishFragmentChanged: pubsub.publishFragmentChanged.bind(pubsub),
	});
}
