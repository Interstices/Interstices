/**
 * IsxFragment provides a single $() method that can be used to chain
 * write/read calls in a fluent-API manner.
 * So instead of using `update(fragment, values)` or `read(fragment)`,
 * you would simply use `fragment.$(update, values)` or `fragment.$(read)`.
 *
 * If javascript proposals [pipeline operator](https://github.com/tc39/proposal-pipeline-operator)
 * or [extension methods](https://github.com/tc39/proposal-extensions) get accepted, we can leverage
 * those syntaxes as well.
 *
 * We keep fragments stateless so that we never have to worry about the
 * internal state becoming stale.
 *
 * Sidenote: Isx stands for Interstices
 */
class IsxFragment {
	/**
	 * @param {IsxId} id the environment id of the fragment
	 */
	constructor(id) {
		this._id = id;
	}

	/**
	 * Used to convert API methods into fluent-API methods for chaining.
	 * Eg instead of calling update(fragment, values) you can use fragment.$(update, value)
	 *
	 * If javascript proposals [pipeline operator](https://github.com/tc39/proposal-pipeline-operator)
	 * or [extension methods](https://github.com/tc39/proposal-extensions) get accepted, we can leverage
	 * those syntaxes as well.
	 */
	$(fn, ...args) {
		return fn(this, ...args);
	}

	equals(otherFragment) {
		return this.getIdObject().equals(otherFragment.getIdObject());
	}

	// We don't provide a getId() method because it can be confusing
	// whether it returns the id as a string or an object. And if we made it return
	// an object but people assumed it returned a string, they would be confused in
	// cases when fragment1.getId() !== fragment2.getId() even though the id strings were the same.

	/**
	 * @returns {string} A string id, should be unique for this fragment in the current system.
	 */
	getIdString() {
		return this._id.toString();
	}

	getIdObject() {
		return this._id;
	}
}

/**
 * Unlike Java, Javascript Maps don't support custom hash functions, which we need
 * if we want to use fragments as map keys.
 * So this class acts like a Map but takes in fragments as keys, and compares equality
 * using the fragment id.
 */
class FragmentMap extends Map {
	// TODO: for all keys we maintain a mapping of fragment-hashcode -> fragment, so that we can retrieve all keys
	//       as fragments within the keys() method and entries() method.
	//       However given that fragments are just wrappers around ids, we can maybe just construct fragments
	//       on the fly from the hashcodes instead.
	_keys = new Map();

	set = (key, val) => {
		this._keys.set(this.getHashCode(key), key);
		return super.set(this.getHashCode(key), val);
	};

	get = key => super.get(this.getHashCode(key));
	has = key => super.has(this.getHashCode(key));
	delete(key) { // ESM.js doesn't like it when you declare "delete" as a public class field, so we declare it as a method
		this._keys.delete(this.getHashCode(key));
		return super.delete(this.getHashCode(key));
	}

	keys = () => this._keys.values();
	values = () => super.values();

	* entries() {
		for (const [hashCode, value] of super.entries()) {
			yield [this._keys.get(hashCode), value];
		}
	}

	[Symbol.iterator] = this.entries;

	getHashCode = key => key.getIdString();
}

/**
 * Unlike Java, Javascript Sets don't support custom hash functions, which we need
 * if we want to use fragment ids for comparison.
 * So this class acts like a Set but takes in fragments as values, and compares equality
 * using the fragment id.
 */
class FragmentSet extends Set {
	// TODO: for all values we maintain a mapping of fragment-hashcode -> fragment, so that we can retrieve all values
	//       as fragments within the keys(), values(), and entries() methods.
	//       However given that fragments are just wrappers around ids, we can maybe just construct fragments
	//       on the fly from the hashcodes instead.
	_keys = new Map();

	add = val => {
		this._keys.set(this.getHashCode(val), val);
		return super.add(this.getHashCode(val));
	};

	has = key => super.has(this.getHashCode(key));
	delete(key) { // ESM.js doesn't like it when you declare "delete" as a public class field, so we declare it as a method
		this._keys.delete(this.getHashCode(key));
		return super.delete(this.getHashCode(key));
	}

	clear = () => super.clear();

	values = () => this._keys.values();
	keys = this.values;

	* entries() {
		for (const value of this.values()) {
			yield [value, value];
		}
	}

	[Symbol.iterator] = this.values;

	getHashCode = key => key.getIdString();
}

export {
	IsxFragment,
	FragmentMap,
	FragmentSet,
};

export default function installFragmentInterface(scope) {
	return scope.extendAPI({
		// If `api` is provided, we bind the new fragment to it.
		// This is because fragments are bound to APIs (see comment for IsxFragment class for more info).
		toFragment: (scope, id) => new IsxFragment(id),
		// `scope` is un-used here, but all API functions are required to take it as first argument.
		isFragment: (scope, value) => value instanceof IsxFragment,
	});
}
