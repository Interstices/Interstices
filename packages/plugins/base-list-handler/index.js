/**
 * Provides an extremely primitive api for working with singled linked lists,
 * the default structure for lists in Interstices.
 *
 * For a more comprehensive and user-friendly api, use ListHandler.
**/

import { ListFormat, ListItemFormat } from '@interstices/base-formats';

const DEFAULT_LIST_FORMATS = {
	list: ListFormat,
	item: ListItemFormat,
};

// TODO: look into https://github.com/ReactiveX/IxJS as an alternative
async function asyncIteratorToArray(iterable) {
	const array = [];
	for await (const item of iterable) {
		array.push(item);
	}

	return array;
}

class BaseListHandler {
	container;
	API;
	// `size` is only set and maintained after getSize() is called at least once,
	//  saving us a list traversal in cases where getSize() is never called.
	size;

	// TODO: since ListHandler needs access to the API, we currently
	// bind one to the object, but maybe we should consider using
	// the .$() apply method like we do for IsxFragment, so we don't
	// have to worry about passing the handler between different
	// scopes and "re-scoping" the handler.
	/**
	 * @param {*} formats required
	 * @param {*} formats.list required
	 * @param {*} formats.item required
	 */
	constructor(API, formats) {
		this.API = API;
		this.formats = formats;
	}

	/**
	 * Make sure to call init() after construction!
	 * We recommend using BaseListPlugin.createList or BaseListPlugin.loadList
	 * instead of constructing BaseListHandler instances directly.
	 */
	async init(container) {
		const { readProperty } = this.API.getAll();
		if ((await container.$(readProperty, 'type')) !== this.formats.list.type) {
			throw Error('Error: tried to create a ListHandler from a non-list structure.');
		}

		this.container = container;
	}

	getListContainer() {
		return this.container;
	}

	async getHead() {
		const { readProperty } = this.API.getAll();
		return await this.container.$(readProperty, 'head');
	}

	async getValue(item) {
		const { readProperty } = this.API.getAll();
		return await item.$(readProperty, 'value');
	}

	async getNext(item) {
		const { readProperty } = this.API.getAll();
		return await item.$(readProperty, 'next');
	}

	async * asIterable() {
		const { isFragment, readProperty } = this.API.getAll();

		let item = await this.getHead();
		while (isFragment(item)) {
			yield item;
			// eslint-disable-next-line no-await-in-loop
			item = await item.$(readProperty, 'next');
		}
	}

	async asArray() {
		return asyncIteratorToArray(this.asIterable());
	}

	async * values() {
		const { readProperty } = this.API.getAll();

		for await (const item of this.asIterable()) {
			yield await item.$(readProperty, 'value');
		}
	}

	async valueArray() {
		return asyncIteratorToArray(this.values());
	}

	/**
	 * This function only returns the item that was inserted, but `item` and `container` may
	 * have changed as well. Either listen for changes to `item` or `container`, or re-retrieve
	 * the fragments, to makes sure your local state up to date with the filesystem state.
	 *
	 * @param {*} item if null, will insert value at the head of the list.
	 * @param {*} value
	 * @returns item that was inserted
	 */
	async insertAfter(item, value) {
		const { isFragment, readProperty, create, update } = this.API.getAll();

		const nextItem = item ? await item.$(readProperty, 'next') : await this.getHead();
		const itemToInsert = await create({
			type: this.formats.item.type,
			value,
			next: isFragment(nextItem) ? nextItem : undefined,
		});

		if (item) {
			await update(item, { next: itemToInsert });
		} else {
			await update(this.container, { head: itemToInsert });
		}

		if (this.size !== undefined) {
			this.size++; // `size` is only maintained after it has been retrieved at least once.
		}

		return itemToInsert;
	}

	/**
	 * This function only returns the item that was removed, but `item` and `container` may
	 * have changed as well. Either listen for changes to `item` or `container`, or re-retrieve
	 * the fragments, to makes sure your local state up to date with the filesystem state.
	 *
	 * @param {*} item if null, will remove item at head of list.
	 * @returns item that was removed
	 */
	async removeItemAfter(item) {
		const { isFragment, readProperty, update } = this.API.getAll();

		const itemToRemove = item ? await item.$(readProperty, 'next') : await this.getHead();

		if (!isFragment(itemToRemove)) {
			throw Error('Error: nothing to remove!');
		}

		const nextItem = await itemToRemove.$(readProperty, 'next');
		if (item) {
			await update(item, { next: nextItem });
		} else {
			await update(this.container, { head: nextItem });
		}

		if (this.size !== undefined) {
			this.size--; // `size` is only maintained after it has been retrieved at least once.
		}

		return itemToRemove;
	}

	async getSize() {
		if (this.size === undefined) {
			this.size = (await this.asArray()).length;
		}

		return this.size;
	}
}

/**
 * Creates a new list and returns a handler for it.
 */
async function createList(scope, formats = DEFAULT_LIST_FORMATS) {
	const { create } = scope.API.getAll();

	const container = await create({ type: formats.list.type });
	return await loadList(scope, container, formats);
}

/**
 * @param {*} container A fragment with the LIST_TYPE type.
 * @returns A handler for manipulating and traversing the list.
 */
async function loadList(scope, container, formats = DEFAULT_LIST_FORMATS) {
	const list = new BaseListHandler(scope.API, formats);
	await list.init(container);
	return list;
}

// TODO: extend write/read so that they throw an error if another plugin tries to call write/read on a list/list-item fragment.
//       All writes/reads to lists and list items should go through list handler, to ensure that the list metadata is properly maintained.
export default function installBaseListHandler(scope) {
	return scope.extendAPI({
		createBaseList: createList,
		loadBaseList: loadList,
	});
}
