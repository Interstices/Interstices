/**
 * Creates a wrapper for creating/using sets via the common methods: has, add, remove, values(), getSize().
 *
 * Any given item will only appear in the set once even if added multiple times.
 *
 * This set handler is faster than base-set-handler by taking advantage of query operations.
**/

import { SetFormatGroup as DEFAULT_SET_FORMAT_GROUP, Any, required } from '@interstices/base-formats';
import { FragmentMap } from '@interstices/fragment';

// TODO: how do we enforce the "sub-class" restrictions mentioned in the comments below?
//       Currently we don't have a sub-classing system, but we do have format groups like the
//       DEFAULT_SET_FORMAT_GROUP that try to serve the same purpose.
//       Perhaps we need a system of registering format groups in our format system.
const MembershipMetadataFormat = {
	type: 'MembershipMetadata',
	set: required(Any), // Sub-class of SetFormat
	member: required(Any),
	item: required(Any), // Sub-class of SetItemFormat
};

/**
 * Job queues are keyed by fragment, so only when a function is called with the same fragment will it be serialized.
 * TODO: REPLACE THIS WITH AN INDUSTRY-STANDARD SOLUTION
 */
class KeyedJobQueues {
	fragmentQueues = new FragmentMap();
	rawValueQueues = new Map();
	scope;

	constructor(scope) {
		this.scope = scope;
	}

	getQueuesForVal = key => this.scope.API.getAll().isFragment(key) ? this.fragmentQueues : this.rawValueQueues;

	addJob(key, job) {
		return new Promise((resolve, reject) => {
			const queues = this.getQueuesForVal(key);
			// Notice how the block below is all synchronous. This is to prevent race conditions.
			if (queues.has(key)) {
				// Job queue for key is already running, so just push to job queue
				queues.get(key).push({ job, resolve, reject });
			} else {
				queues.set(key, []);
				queues.get(key).push({ job, resolve, reject });
				this.runJobs(key);
			}
		});
	}

	async runJobs(key) {
		const queues = this.getQueuesForVal(key);
		const queue = queues.get(key);
		// Notice that if queue.length reaches 0, this.queues.delete(fragment) will be called
		// synchronously right afterwards to prevent race conditions
		while (queue.length > 0) {
			const { job, resolve, reject } = queue.shift();
			try {
				// eslint-disable-next-line no-await-in-loop
				resolve(await job());
			} catch (e) {
				reject(e);
			}
		}

		queues.delete(key);
	}
}

/**
 * Serializes all calls to an asynchronous function.
 * CAUTION: if a synchronized function ever calls itself, it will deadlock!
 * Even if the call has different arguments, or is indirect recursion.
 * Consider the following:
 * factorial = (x) => x * g(x-1)
 * g = (x) => x === 0 ? 1 : factorial(x)
 * Trying to synchronize factorial() will cause a deadlock.
 * This is why it is best to use synchronize() within plugins that use Scoped API Platform,
 * since Scoped API Platform makes it so API calls will travel to outer scopes, and reduces
 * the chance that a function will call itself.
 */
class Synchronizer {
	constructor(scope) {
		this.jobQueues = new KeyedJobQueues(scope);
	}

	synchronize(asyncFn) {
		return async (scope, value, ...rest) => await this.jobQueues.addJob(value, () => asyncFn(scope, value, ...rest));
	}
}

async function getFirstAsyncItem(asyncIterator) {
	return (await asyncIterator.next())?.value;
}

class FastSetHandler {
	baseListHandler;
	API;
	dbCollection;

	// TODO: since BaseSetHandler is bound to an API like IsxFragment,
	// maybe we should consider using apply() pattern?
	/**
	 * @param {*} formats optional
	 */
	constructor(scope, formats = DEFAULT_SET_FORMAT_GROUP) {
		this.API = scope.API;
		this.formats = formats;

		// `add()` and `remove()` use a test-and-set pattern so they must be synchronized,
		// both with themselves and with each other.
		const synchronizer = new Synchronizer(scope);
		this.add = synchronizer.synchronize(this.addUnsynced.bind(this));
		this.remove = synchronizer.synchronize(this.removeUnsynced.bind(this));
	}

	/**
	 * Make sure to call init() after construction!
	 * We recommend using SetPlugin.createSet or SetPlugin.loadSet
	 * instead of constructing BaseSetHandler instances directly.
	 */
	async init(container) {
		// `initCalled` is used to prevent race conditions.
		if (this.initCalled) {
			throw Error('BaseSetHandler.init() should not be called after BaseSetHandler is already initialized!');
		}

		this.initCalled = true;

		const { loadBaseList } = this.API.getAll();
		this.baseListHandler = await loadBaseList(container, this.formats);
	}

	getSetContainer(...args) { return this.baseListHandler.getListContainer(...args); }

	async * asIterable(...args) {
		yield * this.baseListHandler.asIterable(...args);
	}

	async asArray(...args) {
		return await this.baseListHandler.asArray(...args);
	}

	async * values(...args) { yield * this.baseListHandler.values(...args); }
	async valueArray(...args) { return await this.baseListHandler.valueArray(...args); }

	async has(value) {
		// TODO: query primary set, not root set
		const { queryRootSet } = this.API.getAll();
		const metadataQuery = { set: this.getSetContainer(), member: value };
		const result = await getFirstAsyncItem(queryRootSet(metadataQuery));
		return Boolean(result);
	}

	async addUnsynced(value) {
		if (await this.has(value)) {
			return false;
		}

		const { create } = this.API.getAll();

		await this.baseListHandler.insertAfter(null, value); // Insert at head of list, an O(1) operation.
		const item = await this.baseListHandler.getHead(); // Get the item node for the value we just added

		await create({ type: MembershipMetadataFormat.type, set: this.getSetContainer(), member: value, item });
		return true;
	}

	// Unlike base-list-handler, here we leverage the Sets db to achieve O(1) for remove operations.
	async removeUnsynced(value) {
		const { queryRootSet, removeFromRootSet } = this.API.getAll();
		const metadataQuery = { set: this.getSetContainer(), member: value };
		const asyncIterator = queryRootSet(metadataQuery);
		const { value: queryResult } = await asyncIterator.next();

		if (!queryResult) {
			return false;
		}

		if (!(await asyncIterator.next()).done) {
			throw Error('Unexpected multiple results for set membership query');
		}

		await removeFromRootSet(queryResult.fragment);

		// Delete value from base list
		const itemNode = queryResult.content.item;
		const { fragment: prevItem } = await getFirstAsyncItem(queryRootSet({ next: itemNode })); // `prevItem` can be null
		await this.baseListHandler.removeItemAfter(prevItem);
	}

	async getSize() {
		return await this.baseListHandler.getSize();
	}
}

/**
 * Creates a new set and returns a handler for it.
 */
async function createSet(scope, formats = DEFAULT_SET_FORMAT_GROUP) {
	const { createBaseList } = scope.API.getAll();
	const baseListContainer = await createBaseList(formats);
	return await loadSet(scope, baseListContainer.getListContainer(), formats);
}

/**
 * @param {*} container A fragment with the SetFormat type.
 * @param {*} formats optional
 * @returns A handler for manipulating and traversing the set.
 */
async function loadSet(scope, container, formats = DEFAULT_SET_FORMAT_GROUP) {
	const set = new FastSetHandler(scope, formats);
	await set.init(container);
	return set;
}

// TODO: extend write/read so that they throw an error if another plugin tries to call write/read on a set/set-item fragment.
//       All writes/reads to sets and set items should go through set handler, to ensure that the set metadata is properly maintained.
export default function installFastSetHandler(scope) {
	return scope
		.extendAPI({
			createSet,
			loadSet,
		})
		.addFormats([MembershipMetadataFormat]);
}
