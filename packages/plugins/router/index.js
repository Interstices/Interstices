/**
 * Will automatically route the API function given by `fnName`.
 * Assumes that the specified API function taken in `scope` and `fragment` as their first arguments.
 * Functions that don't follow this condition will need custom routing.
 */
async function route(routingRules, fnName, scope, fragment, ...rest) {
	const childScope = routingRules(scope, fragment);
	const fn = childScope.API.getAll()[fnName];
	return fn(fragment, ...rest);
}

/**
 * @param {*} routerScope the base scope, must include scoped-api-platform.
 * @param {Function} routingRules a function that takes in a fragment and returns the scope that can handle that fragment.
 * @param {Iterable} functionsToRoute a set of functions to route
 * @returns a new scope, called the "parent scope"
 */
export default function installRouter(routerScope, routingRules, functionsToRoute) {
	const fns = [];
	for (const fnName of functionsToRoute) {
		fns[fnName] = (scope, fragment, ...rest) => route(routingRules, fnName, scope, fragment, ...rest);
	}

	return routerScope
		.extendAPI({ ...fns });
}
