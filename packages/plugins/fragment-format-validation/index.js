import baseScope from '@interstices/environment';
import installScopedAPIPlatform from '@interstices/scoped-api-platform';
import installFragmentInterface from '@interstices/fragment';

/**
 * If readProperty() is provided by the core api, we use it, otherwise we use read().
 * Important note: we use unsafe read because validating child fragments would, at best, impact performance,
 * and at worst would lead to infinite loops.
 */
async function readPropertyUnsafePolyfill(scope, fragment, propertyName) {
	const { read, readProperty } = scope.unsafeScope.API.getAll();

	return readProperty
		? await readProperty(fragment, propertyName)
		: (await read(fragment))[propertyName];
}

function emptyOrAllNull(arr) {
	return arr.filter(item => item).length === 0;
}

/**
 * Validates a given set of content fields and values. Used for validating writes/reads. For example, for something like
 * `update(someFragment, { foo: true, bar: null })`, we check if it is valid to add a Boolean value for `foo` and remove
 * the value for `bar`.
 *
 * Important note: we use unsafe read when retrieving the `type` property of child fragments because we do
 * NOT want to validate child fragments. This is because validating child fragments would, at best, impact performance,
 * and at worst would lead to infinite loops.
 *
 * TODO: optimize this function, since it is called for every read and write.
 *       Perhaps we can pre-process the formats beforehand to make them easier to parse.
 *       Or we can use an existing javascript framework for schema definition/validation.
 *
 * @param {*} scope
 * @param {*} content a set of key-value pairs, does _not_ have to be complete (contain every required property of the format).
 * @param {*} format optional, if not provided will use the `type` field of the content param to retrieve the format.
 */
async function validatePartialContentFormat(scope, content, format) {
	const { isFragment } = scope.unsafeScope.API.getAll();

	if (!format) {
		if (!content.type) {
			throw Error('validateContentFormat() expects either content.type or format to be provided');
		}

		format = scope.formats.get(content.type);
		if (!format) {
			throw Error(`Unable to retrieve format corresponding to format name ${content.type}.`);
		}
	}

	if (format.type === 'Any') {
		return; // No need to validate content with format 'Any'
	}

	for (const [key, childValue] of Object.entries(content)) {
		if (key === 'type') {
			if (childValue !== format.type) {
				throw Error(`Type mismatch, expected type ${format.type} but got ${childValue}`);
			}

			continue;
		}

		const childFormat = format[key];
		if (!childFormat) {
			throw Error(`Fragment content with format ${format.type} contains extra property ${key}.`);
		}

		const validFormats = Array.isArray(childFormat) ? childFormat : [childFormat];

		if (emptyOrAllNull(validFormats)) {
			throw Error(`Format ${format.type} is invalid, field ${key} does not specify at least one valid format`);
		}

		if (validFormats.some(format => format?.type === 'Any')) {
			continue;
		}

		if (childValue === undefined || childValue === null) {
			if (validFormats.includes(null)) {
				continue;
			} else {
				throw Error(`Fragment content does not conform to format ${format.type}. `
				+ `Property ${key} is required but was not provided.`);
			}
		}

		const validFormatNames = new Set(validFormats.filter(validFormat => validFormat).map(validFormat => (
			// If childFormat is an object, we assume it is a fragment format.
			// Otherwise we expect it to be a primitive constructor, namely `String`, `Boolean`, or `Number`.
			typeof validFormat === 'object' ? validFormat.type : validFormat.name
		)));

		// TODO: each property can be checked in parallel, instead of using await here and forcing serial execution.
		const childValueFormatName = isFragment(childValue)
			// eslint-disable-next-line no-await-in-loop
			? await readPropertyUnsafePolyfill(scope, childValue, 'type')
			: childValue.constructor.name;
		if (!childValueFormatName) {
			throw Error(`Unable to get format of property "${key}".`);
		}

		if (!validFormatNames.has(childValueFormatName)) {
			throw Error(`Fragment content does not conform to format "${format.type}". `
				+ `Property "${key}" should have one of formats [${[...validFormatNames.values()].join(', ')}] `
				+ `but instead has format "${childValueFormatName}"`);
		}
	}
}

/**
 * Validate content and assume it is the full content.
 */
async function validateContentFormat(scope, content) {
	if (!content.type) {
		throw Error('validateContentFormat() expects either content.type to be provided');
	}

	const format = scope.formats.get(content.type);
	if (!format) {
		throw Error(`Unable to retrieve format corresponding to format name ${content.type}.`);
	}

	const contentCopy = { ...content };
	for (const key of Object.keys(format)) {
		if (content[key] === undefined) {
			// Explicitly set to null, so that the call to validatePartialContentFormat() below can
			// check if its valid to leave that field unprovided.
			contentCopy[key] = null;
		}
	}

	return await validatePartialContentFormat(scope, contentCopy, format);
}

async function read(bypassChecks, scope, fragment) {
	const { read: readUnsafe } = scope.unsafeScope.API.getAll();
	const content = await readUnsafe(fragment);

	if (!bypassChecks) {
		try {
			await validateContentFormat(scope, content);
		} catch (e) {
			throw Error(`Fragment ${fragment.getIdString()} does not conform to format. ${e.message}`);
		}
	}

	return content;
}

async function checkWrite(scope, fragment, newValues) {
	if (!newValues.type && !fragment) {
		throw Error('Write error: Must specify a valid format name in the \'type\' field when creating a new fragment.');
	}

	try {
		if (newValues.type) {
			// If 'type' is provided in newValues we assume that the format is changing,
			// in which case we have to do a full validation.
			const { read: readUnsafe } = scope.unsafeScope.API.getAll();

			const existingContent = fragment ? await readUnsafe(fragment) : {};
			const fullContent = { ...existingContent, ...newValues };
			await validateContentFormat(scope, fullContent);
		} else {
			const type = await readPropertyUnsafePolyfill(scope, fragment, 'type');
			await validatePartialContentFormat(scope, { ...newValues, type });
		}
	} catch (e) {
		throw Error(`${fragment ? 'Fragment ' + fragment.getIdString() : 'New fragment'} does not conform to format. ${e.message}`);
	}
}

async function update(bypassChecks, scope, fragment, newValues) {
	const { update: updateUnsafe } = scope.unsafeScope.API.getAll();

	if (!bypassChecks) {
		await checkWrite(scope, fragment, newValues);
	}

	return await updateUnsafe(fragment, newValues);
}

// Adds validation to the readProperty method if it is provided by the core api.
async function readProperty({ bypassReadChecks }, scope, fragment, propertyName) {
	const value = await readPropertyUnsafePolyfill(scope, fragment, propertyName);
	const formatName = await readPropertyUnsafePolyfill(scope, fragment, 'type');

	if (!bypassReadChecks) {
		try {
			await validatePartialContentFormat(scope, { [propertyName]: value, type: formatName });
		} catch (e) {
			throw Error(`Fragment ${fragment.getIdString()} does not conform to format. ${e.message}`);
		}
	}

	return value;
}

// Adds validation to the overwrite method if it is provided by the core api.
// No values in the new content will be inherited from the old content **except** the format.
async function overwrite({ bypassWriteChecks }, scope, fragment, newContent) {
	const { overwrite: overwriteUnsafe } = scope.unsafeScope.API.getAll();

	// If 'type' is provided in newContent we always use it, in case we are trying to change the format of an existing fragment.
	const type = newContent.type || (fragment && await readPropertyUnsafePolyfill(scope, fragment, 'type'));

	if (!bypassWriteChecks) {
		const newContentCopy = { type, ...newContent };
		const format = scope.formats.get(type);

		for (const key of Object.keys(format)) {
			if (newContent[key] === undefined) {
				// Explicitly set to null, so that the call to validatePartialContentFormat() inside checkWrite() below can
				// check if its valid to leave that field unprovided.
				newContentCopy[key] = null;
			}
		}

		await checkWrite(scope, fragment, newContentCopy);
	}

	return await overwriteUnsafe(fragment, { type, ...newContent });
}

// Adds validation to the copyAndWrite method if it is provided by the core api.
async function copyAndWrite({ bypassWriteChecks }, scope, fragment, newValues) {
	const { copyAndWrite: copyAndWriteUnsafe } = scope.unsafeScope.API.getAll();

	if (!bypassWriteChecks) {
		await checkWrite(scope, fragment, newValues);
	}

	return await copyAndWriteUnsafe(fragment, newValues);
}

async function create(bypassWriteChecks, scope, newValues) {
	const { create: createUnsafe } = scope.unsafeScope.API.getAll();

	if (!bypassWriteChecks) {
		await checkWrite(scope, null, newValues);
	}

	return await createUnsafe(newValues);
}

const CORE_UTILS_SAFE = {
	overwrite,
	readProperty,
	copyAndWrite,
};

class FragmentFormatsRegistry {
	constructor(formats) {
		this.formats = formats;
		this.formatMap = new Map(formats.map(format => [format.type, format]));
	}

	/**
	 * @param {Array} formats
	 */
	addFormats(formats) {
		return new FragmentFormatsRegistry([...this.formats, ...formats]);
	}

	get(formatType) {
		if (!this.formatMap.has(formatType)) {
			throw Error(`Unable to retrieve format with name ${formatType}, format is not registered.`);
		}

		return this.formatMap.get(formatType);
	}
}

/**
 * Normally if an app/plugin wants to bypass format validation for read/write, they should explicitly call the unsafe API
 * via `scope.unsafeScope.API`. However, there are certain cases where we know the entire system doesn't need the validation.
 *
 * For example, if we know that we are the only one writing to a certain db, then as long as we validate all writes
 * then we don't need to validate reads. Bypassing checks can improve performance because validation requires calling
 * readPropertyUnsafePolyfill on every child fragment inside the fragment content.
 *
 * @param {{
 * 	bypassWriteChecks: boolean
 * 	bypassReadChecks: boolean
 * }} options optional options
 */
export default function installFragmentFormatValidation(unsafeScope, { bypassWriteChecks, bypassReadChecks } = {}) {
	const coreUtils = {};
	for (const functionName of Object.keys(CORE_UTILS_SAFE)) {
		// Only validate functions if they are provided by the core-api
		if (unsafeScope.API.get(functionName)) {
			coreUtils[functionName] = CORE_UTILS_SAFE[functionName].bind(null, { bypassWriteChecks, bypassReadChecks });
		}
	}

	const safeScope = baseScope
		.apply(installScopedAPIPlatform)
		.apply(installFragmentInterface)
		.extend({ NAMESPACE_ID: unsafeScope.NAMESPACE_ID }); // Carry over the namespace.

	return safeScope
		.extend({
			unsafeScope, // Add unsafe (unvalidated) scope as member, so that its methods are still accessible.
		})
		.metaExtend(ScopeClass => class extends ScopeClass {
			addFormats(formats) {
				return this.extend({ formats: this.formats.addFormats(formats) });
			}
		})
		.extend({
			formats: new FragmentFormatsRegistry([]),
		})
		.extendAPI({
			create: create.bind(null, bypassWriteChecks),
			update: update.bind(null, bypassWriteChecks),
			read: read.bind(null, bypassReadChecks),
			validateContentFormat,
			validatePartialContentFormat,
		})
		.extendAPI(coreUtils)
		.extendAPI({
			// Pub-sub pass-through methods.

			// TODO: do we need format validation for subscriptions?
			//       Right now content being passed to the subscribers isn't being validated.
			subscribeToFragment(scope, fragment, listener) {
				return scope.unsafeScope.API.getAll().subscribeToFragment(fragment, listener);
			},

			subscribeToEvent(scope, eventName, listener) {
				return scope.unsafeScope.API.getAll().subscribeToEvent(eventName, listener);
			},

			publishFragmentChanged(scope, ...args) {
				return scope.unsafeScope.API.getAll().publishFragmentChanged(...args);
			},

			// TODO: separate this to an optional plugin only installed by mongodb core
			closeMongoDB: scope => scope.unsafeScope.API.getAll().closeMongoDB(),
		});
}
