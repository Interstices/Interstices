
/**
 * @param {*} scope provided environment must already contains query plugin + format validation (in that order).
 */
export default function installValidatedSetEmulationPlugin(scope) {
	return scope.extendAPI({
		// No need for any special format validation logic since these functions act on fragments
		// themselves and not fragment content.
		getRootSetValues: (scope, ...args) => scope.unsafeScope.API.getAll().getRootSetValues(...args),
		getRootSetValueArray: (scope, ...args) => scope.unsafeScope.API.getAll().getRootSetValueArray(...args),
		getRootSetContainer: (scope, ...args) => scope.unsafeScope.API.getAll().getRootSetContainer(...args),

		// No need for any special format validation logic since root set values are
		// allowed to be any format.
		addToRootSet: (scope, ...args) => scope.unsafeScope.API.getAll().addToRootSet(...args),
		removeFromRootSet: (scope, ...args) => scope.unsafeScope.API.getAll().removeFromRootSet(...args),
	});
}
