
/**
 * Currently follows MongoDB query.js formats for all parameters
 * TODO: MongoDB query function and LokiJS query function should use the same parameters and parameter formats.
 *       For example, we should define a common format for the `sort` argument.
 *
 * @param {*} scope
 * @param {object} query supports fragments directly inside the query, eg `{ type: ListItemFormat.type, next: someFragment }`
 * @param {Array} sort see https://mongodb.github.io/node-mongodb-native/4.3/interfaces/FindOptions.html#sort
 * @param {number} limit default 100, see https://mongodb.github.io/node-mongodb-native/4.3/interfaces/FindOptions.html#limit
 * @param {Fragment} setToQuery
 * @returns an async generator for all the results, each result is an object containing { fragment, content }
 */
async function * queryWithinSet(scope, query, sort, limit = 100, setToQuery) {
	const { isFragment, getRootSetContainer, queryRootSet, loadSet, read } = scope.API.getAll();

	if (setToQuery !== undefined && !isFragment(setToQuery)) {
		throw Error('invalid `setToQuery` argument passsed to queryWithinSet()');
	}

	if (setToQuery === undefined || setToQuery.equals(getRootSetContainer())) {
		yield * queryRootSet(query, sort, limit);
		return;
	}

	// TODO: right now we manually traverse across set values, which is really inefficient.
	//       if possible we should leverage fast-sets + membership metadata + db join queries.
	const setHandler = await loadSet(setToQuery);
	for await (const fragment of setHandler.values()) {
		const content = await fragment.$(read);
		yield { fragment, content };
	}
}

export default function installQueryWithinSet(scope) {
	return scope.extendAPI({
		queryWithinSet,
	});
}
