/* eslint-disable object-shorthand */

const POLYFILLS = {
	/**
	 * Replaces all content in the old fragment with the new content. No values will be inherited from
	 * the old content, **except** for the `type` field.
	 */
	overwrite: async function (scope, fragment, content) {
		const { read, update } = scope.API.getAll();

		if (!fragment) {
			throw Error('No fragment provided to overwrite()');
		}

		const oldContent = await fragment.$(read);
		const newContent = { ...content };
		for (const key of Object.keys(oldContent)) {
			if (!Object.prototype.hasOwnProperty.call(newContent, key)) {
				newContent[key] = undefined; // Explicitly set to undefined so it gets deleted
			}
		}

		return await fragment.$(update, newContent);
	},

	readProperty: async function (scope, fragment, propertyName) {
		const { read } = scope.API.getAll();

		const content = await read(fragment);
		return content[propertyName];
	},

	/**
	 * Utility function. Like update(), but creates a new fragment with the changes.
	 *
	 * @param {IsxFragment} fragment an existing fragment (required)
	 * @param {*} newValues new properties that will override existing properties on oldContent
	 * @returns {IsxFragment} the new fragment
	 */
	copyAndWrite: async function (scope, fragment, newValues) {
		const { read, create } = scope.API.getAll();

		if (!fragment) {
			throw Error('No fragment provided to copyAndWrite()');
		}

		const oldContent = await fragment.$(read);
		const newFragment = await create({ ...oldContent, ...newValues });
		return newFragment;
	},
};

// TODO: polyfill readMapping() and writeMapping() (if they don't already exist) using query()
export default function installCoreUtilsPolyfill(scope) {
	const functions = {};
	for (const functionName of Object.keys(POLYFILLS)) {
		// Only polyfill functions if they aren't already in the scope
		if (!scope.API.get(functionName)) {
			functions[functionName] = POLYFILLS[functionName];
		}
	}

	return scope.extendAPI(functions);
}
