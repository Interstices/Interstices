import { APIPlatform } from '@interstices/api-platform';

class ScopedAPIPlatform extends APIPlatform {
	scopedAPI;
	boundAPI;
	upstreamAPI;

	constructor(globalAPI, scopedAPI = {}, upstreamAPI = null) {
		super(globalAPI);
		this.scopedAPI = scopedAPI;
		this.upstreamAPI = upstreamAPI;
	}

	/**
	 * Adds a function to the global API, doesn't create a new scoped API.
	 */
	add(functionName, fn) {
		const apiPlatform = super.add(functionName, fn);
		return new ScopedAPIPlatform(apiPlatform.globalAPI, this.scopedAPI, this.upstreamAPI);
	}

	/**
	 * Adds functions to the global API, doesn't create a new scoped API.
	 */
	addAll(functions) {
		const apiPlatform = super.addAll(functions);
		return new ScopedAPIPlatform(apiPlatform.globalAPI, this.scopedAPI, this.upstreamAPI);
	}

	/**
	 *
	 * @param {string} functionName
	 * @param {function} fn a function that takes in the old API function as an argument, and returns a new API function
	 */
	replace(functionName, fn) {
		const apiPlatform = super.replace(functionName, fn);
		return new ScopedAPIPlatform(apiPlatform.globalAPI, this.scopedAPI, this.upstreamAPI);
	}

	bindScope(scope) {
		// We auto-scope global API. In other words, global APis are available at every scope.
		const unboundAPI = { ...this.globalAPI, ...this.scopedAPI };
		this.boundAPI = Object.fromEntries(
			Object.entries(unboundAPI).map(([key, val]) => [key, val.bind(null, scope)]),
		);
	}

	get(functionName) {
		return this.boundAPI[functionName];
	}

	getAll() {
		return this.boundAPI;
	}

	/**
	 * Creates a new scoped API, doesn't modify the global API.
	 */
	extend(functions) {
		// For functions defined here, if they access scope, it returns the upstream scope.
		// we intentionally don't allow recursion, too dangerous.
		// Also, if a single plugin wants function A to access function B, you can't do
		// scope.API.extend({ functionA: ..., functionB: ... }).
		// Instead, declare function B as a global API, or declare function B separately,
		// scope.API.extend({ functionB: ... }) scope.API.extend({ functionA: scope => scope.API.functionB() })
		const outerAPI = { ...this.globalAPI, ...this.scopedAPI }; // We auto-scope global API. In other words, global APis are available at every scope.

		const innerAPI = Object.create(outerAPI);

		for (const [functionName, fn] of Object.entries(functions)) {
			if (Object.prototype.hasOwnProperty.call(innerAPI, functionName)) {
				throw Error(`Error: Multiple plugins trying to add the same api within the same API scope: ${functionName}`);
			}

			innerAPI[functionName] = (scope, ...args) => fn(scope.popAPILayer(), ...args);
		}

		return new ScopedAPIPlatform(this.globalAPI, innerAPI, this);
	}

	getUpstreamAPI() {
		return this.upstreamAPI;
	}
}

export default function installScopedAPIPlatform(parentScope) {
	return parentScope
		.metaExtend(ScopeClass => class extends ScopeClass {
			extend(props) {
				const newScope = super.extend(props);
				newScope.API.bindScope(newScope);
				return newScope;
			}

			extendAPI(methods) {
				return this.extend({ API: this.API.addAll(methods) });
			}

			replaceAPI(functionName, fn) {
				return this.extend({ API: this.API.replace(functionName, fn) });
			}

			pushAPILayer(methods) {
				return this.extend({ API: this.API.extend(methods) });
			}

			// Important! This extends the current scope, so if you keep pushing and popping
			// API layers the scope chain will continue to get deeper and deeper.
			popAPILayer() {
				return this.extend({ API: this.API.getUpstreamAPI() });
			}
		})
		.extend({
			API: new ScopedAPIPlatform(),
		});
}

export {
	ScopedAPIPlatform,
};
