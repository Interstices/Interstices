class APIPlatform {
	globalAPI = {};
	boundGlobalAPI = {};
	constructor(globalAPI = {}) {
		this.globalAPI = globalAPI;
	}

	bindScope(scope) {
		this.boundGlobalAPI = Object.fromEntries(
			Object.entries(this.globalAPI).map(([key, val]) => [key, val.bind(null, scope)]),
		);
	}

	add(functionName, fn) {
		if (this.globalAPI[functionName]) {
			throw Error(`Error: Multiple plugins trying to add the same api: ${functionName}`);
		}

		return new APIPlatform({ ...this.globalAPI, [functionName]: fn });
	}

	/**
	 * @param {*} functions a dictionary of <function name, function>
	 */
	addAll(functions) {
		for (const name of Object.keys(functions)) {
			if (this.globalAPI[name]) {
				throw Error(`Error: Multiple plugins trying to add the same api: ${name}`);
			}
		}

		return new APIPlatform({ ...this.globalAPI, ...functions });
	}

	// TODO: add a function loadClass(class) that instantiates the class and loads all methods of it as API functions,
	// aside from ones prefixed with an underscore eg _someFn()
	// How do we handle inherited methods? Methods on the prototype chain? Methods inherited from Object?

	/**
	 *
	 * @param {string} functionName
	 * @param {function} fn a function that takes in the old API function as an argument, and returns a new API function
	 */
	replace(functionName, fn) {
		const oldFn = this.globalAPI[functionName];
		if (!oldFn) {
			throw Error(`Error: Cannot replace function ${functionName} before it is defined at least once.`);
		}

		return new APIPlatform({ ...this.globalAPI, [functionName]: fn(oldFn) });
	}

	get(functionName) {
		return this.boundGlobalAPI[functionName];
	}

	getAll() {
		return this.boundGlobalAPI;
	}
}

export {
	APIPlatform,
};

export default function installAPIPlatform(parentScope) {
	return parentScope
		.metaExtend(ScopeClass => class extends ScopeClass {
			extend(props) {
				const newScope = super.extend(props);
				newScope.API.bindScope(newScope);
				return newScope;
			}

			extendAPI(methods) {
				return this.extend({ API: this.API.addAll(methods) });
			}

			replaceAPI(functionName, fn) {
				return this.extend({ API: this.API.replace(functionName, fn) });
			}
		})
		.extend({
			API: new APIPlatform(),
		});
}
