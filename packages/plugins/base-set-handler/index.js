/**
 * Creates a wrapper for creating/using sets via the common methods: has, add, remove, values(), getSize().
 * Internally uses a BaseListHandler. This is a simple but rather slow implementation,
 * since has() add() and remove() all have to traverse the entire list.
 *
 * Any given item will only appear in the set once even if added multiple times.
 *
 * The SetHandler is more performant by taking advantage of query operations, but it has the appearance
 * of creating the same data structure as the BaseSetHandler. Contrast this with the ListHandler, which
 * adds additional data structures on top of the BaseListHandler.
**/

import { SetFormatGroup as DEFAULT_SET_FORMAT_GROUP } from '@interstices/base-formats';
import { FragmentMap } from '@interstices/fragment';

/**
 * Job queues are keyed by fragment, so only when a function is called with the same fragment will it be serialized.
 * TODO: REPLACE THIS WITH AN INDUSTRY-STANDARD SOLUTION
 */
class KeyedJobQueues {
	fragmentQueues = new FragmentMap();
	rawValueQueues = new Map();
	scope;

	constructor(scope) {
		this.scope = scope;
	}

	getQueuesForVal = key => this.scope.API.getAll().isFragment(key) ? this.fragmentQueues : this.rawValueQueues;

	addJob(key, job) {
		return new Promise((resolve, reject) => {
			const queues = this.getQueuesForVal(key);
			// Notice how the block below is all synchronous. This is to prevent race conditions.
			if (queues.has(key)) {
				// Job queue for key is already running, so just push to job queue
				queues.get(key).push({ job, resolve, reject });
			} else {
				queues.set(key, []);
				queues.get(key).push({ job, resolve, reject });
				this.runJobs(key);
			}
		});
	}

	async runJobs(key) {
		const queues = this.getQueuesForVal(key);
		const queue = queues.get(key);
		// Notice that if queue.length reaches 0, this.queues.delete(fragment) will be called
		// synchronously right afterwards to prevent race conditions
		while (queue.length > 0) {
			const { job, resolve, reject } = queue.shift();
			try {
				// eslint-disable-next-line no-await-in-loop
				resolve(await job());
			} catch (e) {
				reject(e);
			}
		}

		queues.delete(key);
	}
}

/**
 * Serializes all calls to an asynchronous function.
 * CAUTION: if a synchronized function ever calls itself, it will deadlock!
 * Even if the call has different arguments, or is indirect recursion.
 * Consider the following:
 * factorial = (x) => x * g(x-1)
 * g = (x) => x === 0 ? 1 : factorial(x)
 * Trying to synchronize factorial() will cause a deadlock.
 * This is why it is best to use synchronize() within plugins that use Scoped API Platform,
 * since Scoped API Platform makes it so API calls will travel to outer scopes, and reduces
 * the chance that a function will call itself.
 */
class Synchronizer {
	constructor(scope) {
		this.jobQueues = new KeyedJobQueues(scope);
	}

	synchronize(asyncFn) {
		return async (scope, value, ...rest) => await this.jobQueues.addJob(value, () => asyncFn(scope, value, ...rest));
	}
}

/**
 * Sets internally uses a base list.
 * This is a simple but rather slow implementation, since has() add() and remove() all have to traverse
 * the list.
 */
class BaseSetHandler {
	baseListHandler;
	API;

	// TODO: since BaseSetHandler is bound to an API like IsxFragment,
	// maybe we should consider using apply() pattern?
	/**
	 * @param {*} formats optional
	 */
	constructor(scope, formats = DEFAULT_SET_FORMAT_GROUP) {
		this.API = scope.API;
		this.formats = formats;

		// `add()` and `remove()` use a test-and-set pattern so they must be synchronized,
		// both with themselves and with each other.
		const synchronizer = new Synchronizer(scope);
		this.add = synchronizer.synchronize(this.addUnsynced.bind(this));
		this.remove = synchronizer.synchronize(this.removeUnsynced.bind(this));
	}

	/**
	 * Make sure to call init() after construction!
	 * We recommend using SetPlugin.createSet or SetPlugin.loadSet
	 * instead of constructing BaseSetHandler instances directly.
	 */
	async init(container) {
		// `initCalled` is used to prevent race conditions.
		if (this.initCalled) {
			throw Error('BaseSetHandler.init() should not be called after BaseSetHandler is already initialized!');
		}

		this.initCalled = true;

		const { loadBaseList } = this.API.getAll();
		this.baseListHandler = await loadBaseList(container, this.formats);
	}

	getSetContainer(...args) { return this.baseListHandler.getListContainer(...args); }
	async * asIterable(...args) { yield * this.baseListHandler.asIterable(...args); }
	async asArray(...args) { return await this.baseListHandler.asArray(...args); }
	async * values(...args) { yield * this.baseListHandler.values(...args); }
	async valueArray(...args) { return await this.baseListHandler.valueArray(...args); }

	async has(item) {
		const { isFragment } = this.API.getAll();

		for await (const value of this.values()) {
			if (isFragment(item)) {
				if (isFragment(value) && value.equals(item)) {
					return true;
				}
			} else if (value === item) {
				return true;
			}
		}

		return false;
	}

	async addUnsynced(value) {
		if (await this.has(value)) {
			return false;
		}

		await this.baseListHandler.insertAfter(null, value); // Insert at head of list, an O(1) operation.
	}

	async removeUnsynced(item) {
		const { isFragment, readProperty } = this.API.getAll();
		let previousNode;

		for await (const listNode of this.asIterable()) {
			const value = await listNode.$(readProperty, 'value');
			if (isFragment(item)) {
				if (isFragment(value) && value.equals(item)) {
					await this.baseListHandler.removeItemAfter(previousNode);
					return true;
				}
			} else if (value === item) {
				await this.baseListHandler.removeItemAfter(previousNode);
				return true;
			}

			previousNode = listNode;
		}

		return false;
	}

	async getSize() {
		return await this.baseListHandler.getSize();
	}
}

/**
 * Creates a new set and returns a handler for it.
 */
async function createSet(scope, formats = DEFAULT_SET_FORMAT_GROUP) {
	const { createBaseList } = scope.API.getAll();
	const baseListContainer = await createBaseList(formats);
	return await loadSet(scope, baseListContainer.getListContainer(), formats);
}

/**
 * @param {*} container A fragment with the SetFormat type.
 * @param {*} formats optional
 * @returns A handler for manipulating and traversing the set.
 */
async function loadSet(scope, container, formats = DEFAULT_SET_FORMAT_GROUP) {
	const set = new BaseSetHandler(scope, formats);
	await set.init(container);
	return set;
}

// TODO: extend write/read so that they throw an error if another plugin tries to call write/read on a set/set-item fragment.
//       All writes/reads to sets and set items should go through set handler, to ensure that the set metadata is properly maintained.
export default function installBaseSetHandler(scope) {
	return scope.extendAPI({
		createSet,
		loadSet,
	});
}
