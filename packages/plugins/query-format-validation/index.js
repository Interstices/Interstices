
/**
 * @param {*} scope
 * @param {object} query
 * @param {Array} sort IMPORTANT!! MongoDB core and LokiJS core currently have different formats for the sort parameter!
 * @param {number} limit default 100
 * @returns an async generator for all the results, each result is an object containing { fragment, content }
 */
async function * queryRootSet(scope, query, sort, limit = 100) {
	const { validateContentFormat } = scope.API.getAll();
	const { queryRootSet: queryRootSetUnsafe } = scope.unsafeScope.API.getAll();

	for await (const { fragment, content } of queryRootSetUnsafe(query, sort, limit)) {
		validateContentFormat(content);
		yield { fragment, content };
	}
}

/**
 * @param {*} scope provided environment must already contains query plugin + format validation (in that order).
 */
export default function installValidatedQueryPlugin(scope) {
	return scope.extendAPI({
		queryRootSet,
	});
}
