import { PrefixedId } from '@interstices/identity';

function mapValues(array, fn) {
	return Object.fromEntries(
		Object.entries(array).map(([key, val]) => [key, fn(val)]),
	);
}

function deserializeValue(scope, value) {
	const { toFragment } = scope.API.getAll();

	if (typeof value === 'object' && value !== null) { // All objects are assumed to be fragment links
		const idString = value.id; // All fragment links assumed to be prefixed ids
		if (idString) {
			return toFragment(PrefixedId.fromIdString(idString));
		}

		throw Error('Unable to deserialize value: No `id` property.');
	}

	return value;
}

/**
 * For raw values, removes prefix and parses value. Resolves fragment links into server-side fragment objects.
 * @param {*} json A JSON string produced by apollo-client-lib's serializeFragmentContent()
 * @returns an object
 */
export function deserializeFragmentContent(scope, json) {
	return mapValues(JSON.parse(json), val => deserializeValue(scope, val));
}

export function fromQueryVariableId(scope, idJson) {
	const { toFragment } = scope.API.getAll();
	try {
		const idString = JSON.parse(idJson).id;
		if (!idString) {
			throw Error('No `id` property in resolved object.');
		}

		return toFragment(PrefixedId.fromIdString(idString));
	} catch (e) {
		throw Error(`Invalid id passed to fromQueryVariableId(). Error: ${e.message}`);
	}
}
