function serializeValue(scope, value) {
	const { isFragment } = scope.API.getAll();

	if (isFragment(value)) {
		return { idType: 'PrefixedId', id: value.getIdString() }; // Wrap fragment links in objects to distinguish them from regular string values
	}

	if (typeof value === 'object' && value !== null) {
		// TODO: if we do want to support nested objects, we might also want to detect
		//       circular references so the JSON.stringify replacer doesn't loop infinitely.
		throw Error('Error: nested objects are currently unsupported.');
	}

	return value; // For strings and numbers and booleans, return as-is
}

/**
 *
 * @param {*} scope
 * @param {*} fragment
 * @param {*} content (optional) if provided, saves a db read
 * @returns
 */
export async function serializeFragmentContent(scope, fragment, content) {
	const { read } = scope.API.getAll();

	content = content === undefined ? await fragment.$(read) : content;
	// Unfortunately we can't leverage JSON.stringify's replacer because serializeValue() is async.
	return JSON.stringify(content, (key, val) => key === '' ? val : serializeValue(scope, val));
}

export function toQueryResultId(fragment) {
	return JSON.stringify({ idType: 'PrefixedId', id: fragment.getIdString() });
}
