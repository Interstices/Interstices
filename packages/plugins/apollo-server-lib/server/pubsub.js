import { PubSub } from 'graphql-subscriptions';
import { EventEmitter } from 'events';

export const FRAGMENT_CHANGED_EVENT = 'FRAGMENT_CHANGED';

function createPubSub(scope) {
	console.log('starting apollo-server-lib subscriptions publisher...');

	const eventEmitter = new EventEmitter();
	eventEmitter.setMaxListeners(100);
	const pubsub = new PubSub({ eventEmitter });

	scope.API.getAll().subscribeToEvent('update', ({ fragment }) => {
		console.log(`publishing Apollo event for fragment ${fragment.getIdString()}`);
		pubsub.publish(FRAGMENT_CHANGED_EVENT, fragment);
	});

	return pubsub;
}

export default function installApolloServerPubSub(scope) {
	// Add graphql pub-sub to scope (and all descendant scopes).
	return scope.extend({
		apolloServerPubSub: createPubSub(scope),
	});
}
