import { ApolloServer } from 'apollo-server-micro';
import defaultTypeDefs from './graphql/typeDefs';
import createDefaultResolvers from './graphql/resolvers';
import { URL } from '@interstices/apollo-const';
import { WebSocketServer } from 'ws';
import installApolloServerPubSub from './server/pubsub';
import { deserializeFragmentContent, fromQueryVariableId } from './utils/deserialize';

export const { SUBSCRIPTIONS_PATH } = URL;
export const { GRAPHQL_PATH } = URL;

const wss = new WebSocketServer({ port: URL.WEBSOCKETS_PORT });

const createApolloServer = (scope, typeDefs, resolvers) => new ApolloServer({
	typeDefs,
	resolvers,
	subscriptions: {
		path: URL.WEBSOCKETS_HOSTNAME + SUBSCRIPTIONS_PATH, // Not sure what this does actually
		keepAlive: 9000,
		onConnect: () => console.log('Subscriptions client connected'),
		onDisconnect: () => console.log('Subscriptions client disconnected'),
	},
	playground: {
		subscriptionEndpoint: URL.WEBSOCKETS_HOSTNAME + SUBSCRIPTIONS_PATH,
		settings: {
			'request.credentials': 'same-origin',
		},
	},
});

async function startServer(scope, typeDefs, resolvers) {
	const apolloServer = createApolloServer(scope, typeDefs, resolvers);
	await apolloServer.start();
	apolloServer.installSubscriptionHandlers(wss);
	return apolloServer.createHandler({ path: GRAPHQL_PATH });
}

export default function installApolloServer(scope) {
	let apolloHandler;

	return scope
		.apply(installApolloServerPubSub)
		.extendAPI({
			async startApolloServer(scope, typeDefs = defaultTypeDefs, resolvers) {
				resolvers = resolvers || createDefaultResolvers(scope);
				apolloHandler = await startServer(scope, typeDefs, resolvers);
			},

			/**
			 * Returns a middleware function that you can use in your server.
			 * Eg if you are using Express, you can do:
			 *
			 *     app.use(await getApolloHandler);
			 *
			 * Make sure startServer() is called before this.
			 * @returns {Promise<Function>} a middleware function
			 */
			getApolloHandler(scope) {
				if (!apolloHandler) {
					throw Error('Please start Apollo Server before getting handler');
				}

				return apolloHandler;
			},
		});
}

export {
	defaultTypeDefs,
	createDefaultResolvers,
	fromQueryVariableId,
	deserializeFragmentContent,
};
