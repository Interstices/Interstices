import { gql } from 'apollo-server-micro';

export default gql`

# entity definitions

type Fragment {
	idJson: String!,
	contentJSON: String!,
}

type FragmentContent {
	json: String!
}

input FragmentContentInput {
	json: String!
}

# queries, mutations, subscriptions

type Query {
	fragmentContent(idJson: String!): FragmentContent
	queryRootSet(queryJSON: String!, sortJSON: String, limit: Int): [Fragment]
}
type Subscription {
	fragmentChanged(idJson: String!): FragmentContent
}
type Mutation {
	createFragment(fragmentContent: FragmentContentInput): Boolean!
	updateFragmentContent(idJson: String!, fragmentContent: FragmentContentInput!): Boolean!
}

`;
