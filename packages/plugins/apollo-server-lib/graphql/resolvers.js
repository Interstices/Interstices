import { withFilter } from 'graphql-subscriptions';
import { FRAGMENT_CHANGED_EVENT } from '../server/pubsub';
import { serializeFragmentContent, toQueryResultId } from '../utils/serialize';
import { deserializeFragmentContent, fromQueryVariableId } from '../utils/deserialize';

function mapValues(obj, fn) {
	return Object.fromEntries(Object.entries(obj), ([key, val]) => [key, fn(val)]);
}

async function mapAsync(arr, asyncFn) {
	return await Promise.all(arr.map(asyncFn));
}

// TODO: look into https://github.com/ReactiveX/IxJS as an alternative
async function asyncIteratorToArray(iterable) {
	const array = [];
	for await (const item of iterable) {
		array.push(item);
	}

	return array;
}

function convertFragmentIdIfExists(scope, value) {
	const prelimRegex = /^\{.*prefix.*tail.*\}$/;
	// Reduce calls to JSON.parse using a preliminary regex. Also since we use a try/catch to check for valid conversion,
	// it can be annoying for debugging if you have the "pause on caught exceptions" option on, so the preliminary regex
	// also reduces the number of these caught errors.
	if (!prelimRegex.test(value)) {
		return false;
	}

	try {
		return fromQueryVariableId(scope, value);
	} catch {
		return value; // If fail to resolve to an id, we just assume it's not an id
	}
}

// Scope should have apolloServerPubSub installed before being passed in.
const createResolvers = scope => ({
	Query: {
		async fragmentContent(_, { idJson }) {
			const fragment = fromQueryVariableId(scope, idJson);
			console.log(`fetching fragment content for fragment ${fragment.getIdString()}`);
			const json = await serializeFragmentContent(scope, fragment);
			return { json };
		},
		async queryRootSet(_, { queryJSON, sortJSON, limit = 100 }) {
			const { queryRootSet } = scope.API.getAll();
			const query = mapValues(JSON.parse(queryJSON), value => convertFragmentIdIfExists(scope, value));
			const sort = sortJSON && JSON.parse(sortJSON);
			const results = await asyncIteratorToArray(queryRootSet(query, sort, limit));
			console.log(`direct server query ${queryJSON} with limit ${limit}`);
			return await mapAsync(results, async ({ fragment, content }) => ({
				idJson: toQueryResultId(fragment),
				contentJSON: await serializeFragmentContent(scope, null, content),
			}));
		},
	},
	Subscription: {
		fragmentChanged: {
			subscribe: withFilter(
				() => scope.apolloServerPubSub.asyncIterator(FRAGMENT_CHANGED_EVENT),
				async (updatedFragment, { idJson }) => {
					const requestedFragment = fromQueryVariableId(scope, idJson);
					if (updatedFragment.equals(requestedFragment)) {
						console.log(`updating subscription for fragment ${requestedFragment.getIdString()}`);
					}

					return updatedFragment.equals(requestedFragment);
				},
			),
			resolve: async fragment => ({ json: await serializeFragmentContent(scope, fragment) }),
		},
	},
	Mutation: {
		async createFragment(_, { fragmentContent }) {
			// TODO: should we use createUnsafe here and assume that client has already validated it?
			const { create } = scope.API.getAll();
			const content = fragmentContent && await deserializeFragmentContent(scope, fragmentContent.json);
			const fragment = await create(content);
			console.log(`created new fragment ${fragment.getIdString()}`);
			return true;
		},
		async updateFragmentContent(_, { idJson, fragmentContent }) {
			// TODO: should we use updateUnsafe here and assume that client has already validated it?
			const { update } = scope.API.getAll();
			const existingFragment = fromQueryVariableId(scope, idJson);
			const { json } = fragmentContent;
			const content = await deserializeFragmentContent(scope, json);
			await update(existingFragment, content);
			console.log(`wrote to fragment ${existingFragment.getIdString()}`);
			return true;
		},
	},
});

export default createResolvers;
