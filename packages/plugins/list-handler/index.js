/**
 * Adds a more intuitive api around BaseListHandler.
 * We do this by creating back-pointers from every list item to it's previous list item, and then
 * storing these back-pointers in metadata so as to not interfere with the base-list format.
 * The new API functions introduced here utilize these back-pointers so that the operations are still efficient.
**/

import { ListMetadataFormat, ListItemMetadataFormat } from '@interstices/base-formats';

const DEFAULT_LIST_METADATA_FORMATS = {
	listMetadata: ListMetadataFormat,
	itemMetadata: ListItemMetadataFormat,
};

/**
 * We add wrappers around all BaseListHandler functions that modify the list,
 * to ensure that back-pointers are updated after any modifications.
 * The BaseListHandler maintains all forward pointers (the head pointer and any next pointers),
 * and the ListHandler maintains all backward pointers (the tail pointer and any prev pointers).
 *
 * Perhaps alternatively we could use the pub-sub mechanism of WritePlugin.update(),
 * to trigger back-pointer updates whenever a list item changes. However that would cause
 * back-pointer changes to be asynchronous, which can cause race conditions.
 *
 * TODO: extend BaseListHandler? If we want to do this, we will have to retrieve it from
 * the scope/environment somehow (perhaps we can put it under scope.classes.BaseListHandler).
 */
class ListHandler {
	baseList;
	API;

	// TODO: since ListHandler is bound to an API like IsxFragment,
	// maybe we should consider using bind operator?
	/**
	 * @param {*} formats required
	 * @param {*} formats.list optional (only used when calling loadBaseList)
	 * @param {*} formats.item optional (only used when calling loadBaseList)
	 * @param {*} formats.listMetadata required
	 * @param {*} formats.itemMetadata required
	 */
	constructor(scope, formats) {
		this.API = scope.API;
		this.formats = formats;
	}

	/**
	 * Make sure to call init() after construction!
	 * We recommend using ListPlugin.createList or ListPlugin.loadList
	 * instead of constructing ListHandler instances directly.
	 */
	async init(container) {
		if (this.baseList) {
			throw Error('ListHandler.init() should not be called after ListHandler is already initialized!');
		}

		const { loadBaseList, readMapping, writeMapping } = this.API.getAll();
		const baseListFormats = (this.formats.list || this.formats.item) ? this.formats : undefined;
		this.baseList = await loadBaseList(container, baseListFormats);
		const list = this.getListContainer();

		const listMetadata = await readMapping(this.formats.listMetadata, list);

		if (listMetadata) {
			return; // List metadata was already initialized by a previous ListHandler
		}

		// Create back-pointers
		let prevItem;
		for await (const item of this.asIterable()) {
			await writeMapping(this.formats.itemMetadata, item, { previous: prevItem });
			prevItem = item;
		}

		await writeMapping(this.formats.listMetadata, list, { tail: prevItem });
	}

	getListContainer(...args) { return this.baseList.getListContainer(...args); }
	async getHead(...args) { return await this.baseList.getHead(...args); }
	async getValue(...args) { return await this.baseList.getValue(...args); }
	async getNext(...args) { return await this.baseList.getNext(...args); }
	async * asIterable(...args) { yield * this.baseList.asIterable(...args); }
	async asArray(...args) { return await this.baseList.asArray(...args); }
	async * values(...args) { yield * this.baseList.values(...args); }
	async valueArray(...args) { return await this.baseList.valueArray(...args); }

	async getTail() {
		const { readProperty, readMapping } = this.API.getAll();
		const list = this.getListContainer();
		const listMetadata = await readMapping(this.formats.listMetadata, list);
		return listMetadata?.$(readProperty, 'tail');
	}

	/**
	 * If item is head (first item in list), returns null.
	 */
	async getPrev(item) {
		const { readProperty, readMapping } = this.API.getAll();
		const itemMetadata = await readMapping(this.formats.itemMetadata, item);
		return itemMetadata?.$(readProperty, 'previous');
	}

	/**
	 * Whenever an item changes its next-pointer, we need to update the new next item's back-pointer.
	 * Will also update the tail pointer if necessary.
	 * CAUTION! undefined behavior if the user passes an item that isn't in the current list.
	 *
	 * @param {*} item (required) the list item that changed
	 */
	async _onChanged(item) {
		const { isFragment, writeMapping } = this.API.getAll();
		const nextItem = await this.getNext(item);
		if (isFragment(nextItem)) {
			await writeMapping(this.formats.itemMetadata, nextItem, { previous: item });
		} else {
			// If no next item, we must be at the tail. Update the list's tail pointer.
			const list = this.getListContainer();
			await writeMapping(this.formats.listMetadata, list, { tail: item });
		}
	}

	/**
	 * @param {*} item if null, will insert value at the head of the list.
	 * @param {*} value
	 * @returns item that was inserted
	 */
	async insertAfter(item, value) {
		const { writeMapping } = this.API.getAll();
		const insertedItem = await this.baseList.insertAfter(item, value);
		// Initialize metadata just in case none of the _onChanged calls write any metadata to the new item
		await writeMapping(this.formats.itemMetadata, insertedItem, {});

		if (item) {
			await this._onChanged(item); // Update back-pointers for inserted item
		}

		await this._onChanged(insertedItem); // Update back-pointers for item after inserted item
		return insertedItem;
	}

	/**
	 * @param {*} item if null, will remove item at head of list.
	 * @returns item that was removed
	 */
	async removeItemAfter(item) {
		const removedItem = await this.baseList.removeItemAfter(item);
		if (!removedItem) {
			return null;
		}

		if (item) {
			await this._onChanged(item);
		}

		return removedItem;
	}

	async push(value) {
		const tail = await this.getTail();
		if (!tail) {
			return await this.insertAfter(null, value);
		}

		return await this.insertAfter(tail, value);
	}

	async pop() {
		const tail = await this.getTail();
		return tail ? await this.remove(tail) : null;
	}

	// CAUTION! undefined behavior if the user passes an item that isn't in the current list.
	async remove(item) {
		const prev = await this.getPrev(item);
		if (prev) {
			return await this.removeItemAfter(prev);
		}

		return await this.removeItemAfter(null);
	}

	async shift() {
		return await this.removeItemAfter(null);
	}

	async unshift(value) {
		return await this.insertAfter(null, value);
	}

	async getSize() {
		return await this.baseList.getSize();
	}
}

/**
 * Creates a new list and returns a handler for it.
 */
async function createList(scope, formats = DEFAULT_LIST_METADATA_FORMATS) {
	const { createBaseList } = scope.API.getAll();
	const baseListFormats = (formats.list || formats.item) ? formats : undefined;
	const baseList = await createBaseList(baseListFormats);
	return await loadList(scope, baseList.getListContainer(), formats);
}

/**
 * @param {*} container A fragment with the LIST_TYPE type.
 * @param {*} formats optional
 * @returns A handler for manipulating and traversing the list.
 */
async function loadList(scope, container, formats = DEFAULT_LIST_METADATA_FORMATS) {
	const list = new ListHandler(scope, formats);
	await list.init(container);
	return list;
}

// TODO: extend write/read so that they throw an error if another plugin tries to call write/read on a list/list-item fragment.
//       All writes/reads to lists and list items should go through list handler, to ensure that the list metadata is properly maintained.
//       We should also throw an error if createBaseList or loadBaseList is called, since mixing usage of base-list-handler
//       and list-handler can also corrupt metadata. Unless we figure out a way to prevent it.
export default function installListHandler(scope) {
	return scope.extendAPI({
		createList,
		loadList,
	});
}
