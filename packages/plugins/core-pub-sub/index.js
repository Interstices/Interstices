/**
 * This plugin should probably be installed before the fragment-format-validation plugin,
 * because we want the "update" event to be close to the database update, and not affected by
 * unrelated operations like format validation. For example if format validation fails,
 * we still want to emit an event if the database was changed.
 */
export default function installCorePubSubPlugin(scope) {
	return scope.replaceAPI('update', oldUpdateFn => (
		/**
		 * ExtraArgs: in case anybody else wrapped update() and added extra arguments before CorePubSub could wrap update(),
		 *            we need to forward those extra arguments.
		 */
		async (scope, fragment, newValues, ...extraArgs) => {
			// TODO: is this dangerous, retrieving other apis during api replacement? Can this lead to infinite loops?
			const { publishEvent, publishFragmentChanged } = scope.API.getAll();

			await oldUpdateFn(scope, fragment, newValues, ...extraArgs);
			publishEvent('update', { fragment, changedValues: newValues });
			publishFragmentChanged(fragment, { fragment, changedValues: newValues });
		}
	));

	// We don't publish events for read or resolve() because those would cause incompatibilities
	// with common plugins like query or readMapping, since query() is technically traversing across all fragments
	// in the filesystem, and we would have to publish a "read" event for every single fragment whenever query() is called.
}
