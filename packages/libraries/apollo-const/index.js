/**
 * Shared constants for apollo-server-lib and apollo-server-client
 */

export const URL = {
	WEBSOCKETS_PORT: '3001',
	WEBSOCKETS_HOSTNAME: 'localhost:3001',
	SUBSCRIPTIONS_PATH: '/api/subscriptions',
	GRAPHQL_PATH: '/api/graphql',
};
