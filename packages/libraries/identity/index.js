/**
 * Abstract parent class for all Interstices Id types.
 *
 * We use separate classes for ids vs fragments because
 * ids should never be exposed above the network layer, while fragments can be.
 * Also, there can be many types of ids, so you might have one fragment
 * that uses one id type, and another fragment that uses another id type,
 * even if both fragments are of the same fragment class.
 */
class IsxId {
	/**
	 * Every Id type in Interstices must have a toString() method, used for equality checking
	 * and used inside FragmentMap.
	 */
	toString() {
		throw Error('unimplemented'); // Abstract function, override in subclass implementations.
	}

	equals(otherId) {
		return this.toString() === otherId.toString();
	}
}

/**
 * The default id format in Interstices, where ids use a prefix+tail format.
 * But other classes of ids can also be defined.
 */
class PrefixedId extends IsxId {
	constructor(prefix, tail) {
		super();
		if (prefix.includes(':')) {
			throw Error(`PrefixedIds cannot contain a ":" inside the prefix. Got: ${prefix}`);
		}

		if (typeof prefix !== 'string' || typeof tail !== 'string') {
			throw Error(`PrefixedId prefix and tail must be strings. Got prefix ${prefix} and tail ${tail}.`);
		}

		this.prefix = prefix;
		this.tail = tail;
	}

	toString() {
		return this.prefix + ':' + this.tail;
	}

	asObject() {
		return { prefix: this.prefix, tail: this.tail };
	}

	static fromIdString(str) {
		if (!str.includes(':')) {
			throw Error(`PrefixedId.fromIdString() could not find a ":" in input string ${str}.`);
		}

		const firstColon = str.indexOf(':');
		const prefix = str.slice(0, firstColon);
		const tail = str.slice(firstColon + 1);
		return new PrefixedId(prefix, tail);
	}

	static fromObject({ prefix, tail }) {
		return new PrefixedId(prefix, tail);
	}
}

export {
	IsxId,
	PrefixedId,
};
