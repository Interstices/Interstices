
module.exports = {
	// We are NOT using any polyfills (like @babel/preset-env).
	// This is because I want to leverage the browser's native async/await
	// instead of dealing with all the extra regenerator-runtime stuff
	// See: https://stackoverflow.com/questions/53558916/babel-7-referenceerror-regeneratorruntime-is-not-defined
	presets: [
		// Use automatic runtime so we don't need to import "React" everywhere
		// (https://reactjs.org/blog/2020/09/22/introducing-the-new-jsx-transform.html)
		['@babel/preset-react', { runtime: 'automatic' }],
	],
	exclude: 'node_modules/**', // Don't transform node_modules, we expect them to already be transformed
};
