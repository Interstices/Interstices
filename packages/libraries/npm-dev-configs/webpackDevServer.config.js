const webpackBaseConfig = require('./webpack.config.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	...webpackBaseConfig,
	devServer: {
		static: './public',
		port: 3000,
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: './templates/index.html',
		}),
	],
};
