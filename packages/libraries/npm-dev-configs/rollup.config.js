/**
 * Adapted from https://adostes.medium.com/authoring-a-javascript-library-that-works-everywhere-using-rollup-f1b4b527b2a9
 * and https://medium.com/@tomaszmularczyk89/guide-to-building-a-react-components-library-with-rollup-and-styled-jsx-694ec66bd2
 */

const { nodeResolve } = require('@rollup/plugin-node-resolve');
// eslint-disable-next-line capitalized-comments
// const { terser } = require('rollup-plugin-terser');
const { babel } = require('@rollup/plugin-babel');
const commonjs = require('@rollup/plugin-commonjs');
const babelOptions = require('./babel-rollup.config.js');
const peerDepsExternal = require('rollup-plugin-peer-deps-external');

// TODO: support UMD output? see https://adostes.medium.com/authoring-a-javascript-library-that-works-everywhere-using-rollup-f1b4b527b2a9
module.exports = {
	input: 'index.js',
	preserveSymlinks: true, // Needed so that lerna symlinked dependencies act like regular dependencies.
	plugins: [
		peerDepsExternal(),
		nodeResolve({ extensions: ['.js', '.jsx'] }),
		babel(babelOptions),
		// Commonjs is needed because lokiJS uses CommonJS syntax. If we want ES6 we should
		// consider moving to LokiDB, see https://github.com/techfort/LokiJS/issues/824
		commonjs(),
		// eslint-disable-next-line capitalized-comments
		// terser(), // for minification
	],
	output: [
		{
			dir: 'dist/esm',
			format: 'esm',
			exports: 'named',
			sourcemap: true,
		},
		{
			dir: 'dist/cjs',
			format: 'cjs',
			exports: 'named',
			sourcemap: true,
		},
	],
};
