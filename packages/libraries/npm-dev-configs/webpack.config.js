/**
 * Adapted from https://medium.com/swlh/a-complete-webpack-setup-for-react-e56a2edf78ae
 */

const path = require('path');
const babelOptions = require('./babel.config.js');

module.exports = {
	mode: 'development',
	entry: './index.js',
	output: {
		path: path.resolve('./public/dist'),
	},
	resolve: {
		extensions: ['.js', '.jsx'],
		// To deal with lokijs trying to import 'fs'
		// See https://github.com/techfort/LokiJS/issues/449
		// and https://webpack.js.org/configuration/resolve/#resolvefallback
		// and https://webpack.js.org/migrate/5/#clean-up-configuration
		fallback: { fs: false },
		// Unfortunately lerna does not hoist local packages (eg @interstices/environment).
		// So for the following dependency tree:
		//     @interstices/folder-tree-text-editor-server
		//     +-- @interstices/environment
		//     `-- @interstices/folder-tree-text-editor
		//         `-- @interstices/environment (peer dependency)
		// If we followed symlinks, the webpack build from folder-tree-text-editor-server would try
		// to find environment under folder-tree-text-editor. So we need to set `symlinks` to false
		// so that webpack looks for environment under folder-tree-text-editor-server, where it can be found.
		// This can be removed after lerna supports hoisting local deps (see https://github.com/lerna/lerna/issues/2734).
		symlinks: false,
		// Prevent "Module not found: Error: Can't resolve 'react/jsx-runtime'" error when building
		// from a package with type "module".
		// https://github.com/facebook/react/issues/20235#issuecomment-750911623
		alias: {
			'react/jsx-dev-runtime': 'react/jsx-dev-runtime.js',
			'react/jsx-runtime': 'react/jsx-runtime.js',
		},
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: babelOptions,
				},
			},
		],
	},
	devtool: 'eval-source-map', // https://webpack.js.org/configuration/devtool/
};
