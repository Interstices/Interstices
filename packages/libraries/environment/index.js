/**
 * TODO: look into using the :: bind-operator instead of using classes.
 */

// Environment properties and functions are added by leveraging class extension.
const baseScope = new (class IsxRootEnvironment {
	/**
	 * Creates a new Environment adding on the properties provided.
	 *
	 * @param {*} props a set of properties to add to the scope.
	 * @returns {Environment} a new Environment scope
	 */
	extend(props) {
		// Notice that extend() not only creates a new instance but creates a new subclass of the current environment.
		const CurrentClass = this.constructor;
		class ChildClass extends CurrentClass {}
		for (const [key, val] of Object.entries(props)) {
			ChildClass.prototype[key] = val;
		}

		return new ChildClass();
	}

	/**
	 * This function is to provide a fluent-api for installing plugins.
	 * So instead of doing
	 *     newScope = installAnotherPlugin(installSomePlugin(scope));
	 * You can do
	 *     newScope = scope.apply(installSomePlugin).apply(installAnotherPlugin);
	 *
	 * @param {Function} fn a function that takes in a scope and returns a new scope
	 * @returns {Environment} a new environment/scope
	 */
	apply(installer, ...extraOptions) {
		return installer(this, ...extraOptions);
	}

	/**
	 * Used to extend methods and even override the extend() property
	 *
	 * @param {Function} createChildClass a function of (class) => (child class)
	 * @returns a new scope
	 */
	metaExtend(createChildClass) {
		const CurrentClass = this.constructor;
		const ChildClass = createChildClass(CurrentClass);
		return new ChildClass();
	}
})();

export default baseScope;
