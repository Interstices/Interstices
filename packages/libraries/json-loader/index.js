import { Any } from '@interstices/base-formats';
import baseScope from '@interstices/environment';
import installLokiJSCore from '@interstices/lokijs-core';
import installMappingPlugin from '@interstices/fast-mapping';
import installBaseListPlugin from '@interstices/base-list-handler';
import installListPlugin from '@interstices/list-handler';
import installFragmentFormatValidation from '@interstices/fragment-format-validation';
import installScopedAPIPlatform from '@interstices/scoped-api-platform';
import installFragmentInterface from '@interstices/fragment';
import installRouter from '@interstices/router';
import installBaseForkHandler from '@interstices/base-fork-handler';
import { FragmentSet } from '@interstices/fragment';

async function defaultFieldResolver(scope, value) {
	if (typeof value === 'object' && value !== null) {
		if (value.ref !== undefined) {
			const resolved = scope.keyToFragmentMap.get(String(value.ref));
			if (!resolved) {
				throw Error(`JSON loader: Unable to resolve reference to JSON key ${value.ref}.`);
			}

			return resolved;
		}

		throw Error(`JSON loader: Unable to parse object ${JSON.stringify(value)} inside JSON data`);
	}

	return value;
}

/**
 *
 * @param {*} arr an array of items
 * @param {*} itemResolver a function that takes in an array item and returns a value
 * @returns a fragment list
 */
async function resolveArray(scope, arr, formats, itemResolver) {
	const { createList } = scope.API.getAll();
	const listHandler = await createList(formats);
	for (const item of arr) {
		// eslint-disable-next-line no-await-in-loop
		const fragment = await itemResolver(scope, item);
		// eslint-disable-next-line no-await-in-loop
		await listHandler.push(fragment);
	}

	return listHandler.getListContainer();
}

/**
 * Works similarly to Apollo GraphQL, where you provide field resolvers
 * for every field of a fragment type. The fields of `initialContent`
 * are first passed through those resolvers before being written to the fragment.
 * Uses the default field resolver if none is provided.
 *
 * For example, if you provide
 *
 *     resolvers = {
 *         [TEXT_FRAGMENT_TYPE]: {
 *             content: (str) => str + "stuff"
 *         }
 *     }
 *
 * Then when resolving text fragments, "stuff" will be appended to the `content` field
 * before being written to the fragment. The `timestamp` field will be written as-is
 * since no resolver is provided for that field.
 *
 * @param {*} customResolvers (optional) a dictionary of <fragment type, field resolvers>
 * @param {*} fragment
 * @param {*} initialContent initial fragment content
 */
async function resolveFragment(scope, customResolvers, fragment, initialContent) {
	const { update } = scope.API.getAll();

	// If no custom resolver is provided, we use default field resolvers for all fields.
	const fieldResolvers = customResolvers[initialContent.type] || {};

	const resolvedContent = {};
	for (const [field, value] of Object.entries(initialContent)) {
		if (fieldResolvers[field]) {
			// eslint-disable-next-line no-await-in-loop
			resolvedContent[field] = await fieldResolvers[field](scope, value);
		} else {
			// eslint-disable-next-line no-await-in-loop
			resolvedContent[field] = await defaultFieldResolver(scope, value);
		}
	}

	await update(fragment, resolvedContent);
}

async function loadJSONData(scope, jsonData, customResolvers = {}, rootKey) {
	const { create } = scope.API.getAll();

	// A mapping from the key in the javascript fragment collection, to the corresponding loki document id.
	const keyToFragmentMap = new Map();

	// First add all fragments and folders to LokiDB, and keep track of which keys correspond to which fragments
	for (const key of Object.keys(jsonData)) {
		// eslint-disable-next-line no-await-in-loop
		const fragment = await create({ type: Any.type }); // Create empty file.
		keyToFragmentMap.set(key, fragment);
	}

	const newScope = scope.extend({ keyToFragmentMap });

	// Resolve any references to other fragments
	for (const [key, fragmentData] of Object.entries(jsonData)) {
		const fragment = keyToFragmentMap.get(key);
		// eslint-disable-next-line no-await-in-loop
		await resolveFragment(newScope, customResolvers, fragment, fragmentData);
	}

	if (rootKey === undefined) {
		// We assume the first fragment in the mock data to be the root.
		rootKey = Object.keys(jsonData)[0];
	}

	const root = keyToFragmentMap.get(String(rootKey));
	return root;
}

async function loadJSONDataWithValidation(scope, jsonData, customResolvers, rootKey) {
	// We create new scope to prevent the validation issues we could run into when loading the JSON data.
	//
	// For example, notice how ListMetadata format has a required METADATA_SUBJECT_KEY field.
	// But what if when loading the json data, we load a list metadata fragment before
	// we load it's subject fragment, then the initial content of that list metadata fragment
	// would not point to a subject fragment, and would be invalid.
	//
	// One method to solve this would be to try and do a graph analysis of the JSON data
	// to figure out the best order to load the data.
	// But another method is to simply load the data without validation into a separate filesystem.
	// Then validate all fragments in that filesystem, and if valid, migrate them into the user's filesystem.
	//
	// TODO: there is still one danger with this approach, in that it's possible
	// that during the final migration, the fragments are in an invalid state
	// until the end, but the user might make a query that accesses one of these
	// fragments while they are invalid. We can solve this with atomic operations.
	const createdFragments = new FragmentSet();
	const jsonLoaderScope = baseScope
		.apply(installLokiJSCore)
		.pushAPILayer({
			async create(scope, newValues) {
				const { create: upstreamCreate } = scope.API.getAll();
				const resultFragment = await upstreamCreate(newValues);
				createdFragments.add(resultFragment); // Keep track of all newly created fragments.
				return resultFragment;
			},
		})
		.apply(installMappingPlugin)
		.apply(installBaseListPlugin)
		.apply(installListPlugin);

	const tempRoot = await loadJSONData(jsonLoaderScope, jsonData, customResolvers, rootKey);

	try {
		const validationScope = jsonLoaderScope
			.apply(installFragmentFormatValidation)
			// Parent scope provides the formats.
			.extend({ formats: scope.formats });

		// We use `read()` to validate the full content of the fragment.
		const { read } = validationScope.API.getAll();
		await Promise.all([...createdFragments].map(fragment => read(fragment)));
	} catch (e) {
		console.error('Mock data was invalid');
		throw e;
	}

	// Use mux core for forkScope since it needs to be able to interact
	// with json loader core (for reading source fragment content) and mongodb core (for writing fork fragments).
	const routingRule = (scope, fragment) =>
		(fragment && createdFragments.has(fragment)) ? jsonLoaderScope : scope.unsafeScope;
	const forkScope = scope
		.apply(installScopedAPIPlatform)
		.apply(installFragmentInterface)
		.apply(installRouter, routingRule, ['update', 'read', 'overwrite'])
		.extendAPI({
			create: (scope, ...args) => scope.unsafeScope.API.getAll().create(...args),
		})
		.apply(installBaseForkHandler);

	// Async function to match what ForkHandler constructor expects.
	const isSourceFragment = async (scope, fragment) => createdFragments.has(fragment);

	const { loadForkRootSync } = forkScope.API.getAll();
	// TODO: store mappings in jsonLoaderScope data-store using mapping fragments.
	//       This will be tricky since we want the fork handler to write forks in the mongodb store,
	//       but store mappings in the jsonLoaderScope store.
	const forkHandler = loadForkRootSync(isSourceFragment, null, true);

	// First pass: create forks.
	await Promise.all([...createdFragments].map(async fragment => {
		// We can skip writing any content since we are re-writing immediately afterwards.
		await forkHandler.createFork(fragment, true);
	}));

	// Second pass: Re-write content.
	await Promise.all([...createdFragments].map(async srcFragment => {
		await forkHandler.updateForkContent(srcFragment);
	}));

	const root = await forkHandler.getForkFragment(tempRoot);
	return root;
}

/**
 * Converts JSON arrays into lists when appropriate by generating custom resolvers.
 * For example, in json-loader-test we pass in the todo list format group.
 * This allows us to specify the todo items as an array instead of manually writing out a linked list
 * inside the json data (see @interstices/mock-data/todo-list for an example).
 *
 * @param {*} scope an environment (must include any formats used in the json data, must have format validation installed).
 * @param {*} jsonData
 * @param {*} listFormatGroups an array of list format groups, see json-loader-test/index.js example usage.
 */
async function loadJSONDataWithArrayResolvers(scope, jsonData, listFormatGroups, rootKey) {
	const allFormatTypes = new Set();
	for (const content of Object.values(jsonData)) {
		allFormatTypes.add(content.type);
	}

	const allListFormatTypes = new Map();
	for (const group of listFormatGroups) {
		allListFormatTypes.set(group.list.type, group);
	}

	const customResolvers = {};
	for (const formatType of allFormatTypes) {
		const format = scope.formats.get(formatType);
		const listFields = Object.entries(format)
			.filter(([_, fieldFormat]) => allListFormatTypes.has(fieldFormat?.type));
		if (listFields.length > 0) {
			customResolvers[formatType] = {};
			for (const [key, fieldFormat] of listFields) {
				const formats = allListFormatTypes.get(fieldFormat.type);
				const itemResolver = defaultFieldResolver;
				customResolvers[formatType][key] = (scope, arr) => resolveArray(scope, arr, formats, itemResolver);
			}
		}
	}

	return await loadJSONDataWithValidation(scope, jsonData, customResolvers, rootKey);
}

export default loadJSONDataWithArrayResolvers;
