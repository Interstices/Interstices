import { ClientForkMetadata } from '@interstices/base-formats';
import { FragmentMap } from '@interstices/fragment';

// Works like lodash mapValues but supports async
async function asyncMapValues(obj, asyncFn) {
	const entriesPromises = Object.entries(obj).map(
		async ([key, val]) => [key, await asyncFn(val)],
	);
	return Object.fromEntries(await Promise.all(entriesPromises));
}

/**
 * Job queues are keyed by fragment, so only when a function is called with the same fragment will it be serialized.
 * TODO: REPLACE THIS WITH AN INDUSTRY-STANDARD SOLUTION
 */
class FragmentJobQueues {
	queues = new FragmentMap();
	addJob(fragment, job) {
		return new Promise((resolve, reject) => {
			// Notice how the block below is all synchronous. This is to prevent race conditions.
			if (this.queues.has(fragment)) {
				// Job queue for fragment is already running, so just push to job queue
				this.queues.get(fragment).push({ job, resolve, reject });
			} else {
				this.queues.set(fragment, []);
				this.queues.get(fragment).push({ job, resolve, reject });
				this.runJobs(fragment);
			}
		});
	}

	async runJobs(fragment) {
		const queue = this.queues.get(fragment);
		// Notice that if queue.length reaches 0, this.queues.delete(fragment) will be called
		// synchronously right afterwards to prevent race conditions
		while (queue.length > 0) {
			const { job, resolve, reject } = queue.shift();
			try {
				// eslint-disable-next-line no-await-in-loop
				resolve(await job());
			} catch (e) {
				reject(e);
			}
		}

		this.queues.delete(fragment);
	}
}

/**
 * Serializes all calls to an asynchronous function.
 * CAUTION: if a synchronized function ever calls itself, it will deadlock!
 * Even if the call has different arguments, or is indirect recursion.
 * Consider the following:
 * factorial = (x) => x * g(x-1)
 * g = (x) => x === 0 ? 1 : factorial(x)
 * Trying to synchronize factorial() will cause a deadlock.
 * This is why it is best to use synchronize() within plugins that use Scoped API Platform,
 * since Scoped API Platform makes it so API calls will travel to outer scopes, and reduces
 * the chance that a function will call itself.
 */
function synchronize(asyncFn) {
	const jobQueues = new FragmentJobQueues();
	return async (fragment, ...rest) => await jobQueues.addJob(fragment, () => asyncFn(fragment, ...rest));
}

/**
 * Just like the fork plugin, this one will create a fork of the source collection. However, it will only fork fragments
 * that have been read or referenced, giving the illusion that the entire source collection has been forked.
 *
 * In other words, any time we read a fork fragment and it points to a source fragment, we fork the source fragment and rewrite
 * the content of the initial fork fragment to now point to the newly created fork fragment.
 */
class AutoForkPlugin {
	scope;
	forkHandler;

	constructor(scope, forkHandler) {
		this.scope = scope;
		this.forkHandler = forkHandler;
	}

	bindMembers(scope, forkHandler) {
		this.scope = scope;
		this.forkHandler = forkHandler;
	}

	// Un-used first argument since API platform expects all functions to accept scope as first argument.
	async getExternalFragForLocalFrag(localFragment) {
		throw Error('TODO');
		// TODO: right now getSourceFragment() doesn't work because we don't maintain back-pointers inside
		// base-fork-handler. See base-fork-handler for more details. How do we maintain back-pointers for
		// this case, without adding overhead for all cases?
		// Perhaps we can have a plugin on top of base-fork-handler that maintains back-pointers and
		// provides functions like getSourceFragment().

		// return this.forkHandler.getSourceFragment(localFragment);
	}

	// Do not confuse this with base-fork-handler's getForkFragment()! The difference is this will automatically
	// create a fork if one doesn't exist yet. This is because lazy fork plugin needs to give the illusion that
	// all forks are already created.
	//
	// Synchronization needed because only the first call for a given external fragment should fork the fragment,
	// subsequent calls should just return the existing fork.
	getLocalFragForExternalFrag = synchronize(async sourceFragment => {
		if (!await this.forkHandler.isSourceFragment(sourceFragment)) {
			throw Error('getLocalFragForExternalFrag() should only be called with external fragments');
		}

		const existingForkFragment = await this.forkHandler.getForkFragment(sourceFragment);
		if (existingForkFragment) {
			return existingForkFragment;
		}

		const forkFragment = await this.forkHandler.createFork(sourceFragment);
		return forkFragment;
	});

	// Synchronization needed because only the first call (for a given local fragment) needs to rewrite
	// the external value to a local value
	// We intentionally do NOT synchronize getLocalFragForExternalFrag and readPropertyAndFork within the
	// same queue, as that would lead to a deadlock, see the comment for synchronize() for details.
	// On the other hand we could synchronize readPropertyAndFork and readAndForkValues on the same queue,
	// but there's not much benefit and we lose out on the async benefits of putting them on different queues.
	readPropertyAndFork = synchronize(async (fragment, propertyName) => {
		const { readProperty, isFragment, update } = this.scope.API.getAll();

		if (!(await this.forkHandler.isForkFragment(fragment))) {
			// For source fragments, call outer api's read(), and router should take care of the rest.
			return await fragment.$(readProperty);
		}

		const value = await fragment.$(readProperty, propertyName);
		if (!isFragment(value)) {
			return value;
		}

		if (await this.forkHandler.isForkFragment(value)) {
			return value; // Already a local fragment
		}

		const localValue = await this.getLocalFragForExternalFrag(value);
		await fragment.$(update, { propertyName, localValue }); // Update fragment to point to local value instead of external value
		return localValue;
	});

	// Synchronization needed because only the first call (for a given local fragment) needs to rewrite
	// external values to local values.
	// We intentionally do NOT synchronize getLocalFragForExternalFrag and readAndForkValues within the
	// same queue, as that would lead to a deadlock, see the comment for synchronize() for details.
	// On the other hand we could synchronize readPropertyAndFork and readAndForkValues on the same queue,
	// but there's not much benefit and we lose out on the async benefits of putting them on different queues.
	readAndForkValues = synchronize(async fragment => {
		const { read, isFragment, update } = this.scope.API.getAll();

		if (!(await this.forkHandler.isForkFragment(fragment))) {
			// For source fragments, call outer api's read(), and router should take care of the rest.
			return await fragment.$(read);
		}

		const content = await fragment.$(read);
		// If we rewrite every time, it can lead to an infinite loop, because React components might be subscribed
		// to the fragment and might call read() every time the fragment changes, leading to
		// read => rewrite => fragment changed => read => rewrite => etc etc.
		let rewriteNeeded = false;
		// Convert any pointers to external fragments into pointers to local fragments
		const offlineContent = await asyncMapValues(content, async value => {
			if (!isFragment(value) || await this.forkHandler.isForkFragment(value)) {
				return value;
			}

			rewriteNeeded = true;
			return await this.getLocalFragForExternalFrag(value); // If value points to a source fragment, return a fork
		});

		if (rewriteNeeded) {
			await fragment.$(update, offlineContent); // Update fragment content to only reference forks
		}

		return offlineContent;
	});
}

export default function installAutoForkPlugin(parentScope) {
	// TODO: the Scoped API Platform should support a single function to create
	//       objects that are bound to the upstream API like clientAutoFork.
	return parentScope
		.metaExtend(ScopeClass => class extends ScopeClass {
			extend(props) {
				const newScope = super.extend(props);

				// Async function to match what ForkHandler constructor expects.
				const isSourceFragment = async (scope, fragment) => !scope.API.getAll().isLocalFragment(fragment);
				const isForkFragment = async (scope, fragment) => scope.API.getAll().isLocalFragment(fragment);

				// Create new forkHandler and autoForkPlugin, bound to the current scope
				// Scoped API Platform handles the rest, ensuring that when one of the methods defined inside
				// pushAPILayer() below is called, the api platform creates a new scope with the upstream
				// API and binds the clientAutoFork to that new scope, before calling the api method.
				const { loadForkRootSync } = newScope.API.getAll();
				const forkHandler = loadForkRootSync(isSourceFragment, isForkFragment, false, ClientForkMetadata, 'fork');
				newScope.clientAutoFork.bindMembers(newScope, forkHandler);

				return newScope;
			}
		})
		.extend({
			clientAutoFork: new AutoForkPlugin(null, null),
		})
		.pushAPILayer({
			readProperty: (scope, ...args) => scope.clientAutoFork.readPropertyAndFork(...args),
			read: (scope, ...args) => scope.clientAutoFork.readAndForkValues(...args),
			getLocalFragForExternalFrag: (scope, ...args) => scope.clientAutoFork.getLocalFragForExternalFrag(...args),
			getExternalFragForLocalFrag: (scope, ...args) => scope.clientAutoFork.getExternalFragForLocalFrag(...args),
		});
}
