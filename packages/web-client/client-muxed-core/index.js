/* eslint-disable object-shorthand */
import baseScope from '@interstices/environment';
import installApolloClientCore from '@interstices/apollo-client-core';
import installLokiJSCore from '@interstices/lokijs-core';
import installScopedAPIPlatform from '@interstices/scoped-api-platform';
import installFragmentInterface from '@interstices/fragment';
import installFragmentFormatValidation from '@interstices/fragment-format-validation';
import installValidatedQueryPlugin from '@interstices/query-format-validation';
import installMappingPlugin from '@interstices/fast-mapping';
import installCoreUtilsPolyfill from '@interstices/core-utils-polyfill';
import installRouter from '@interstices/router';

// TODO: should we support passing in the local and external cores? Or would that add too much abstraction?
export default function installMuxCore(scope) {
	const apolloCoreScope = baseScope
		.apply(installApolloClientCore);
	const lokiScope = baseScope
		.apply(installLokiJSCore);

	const isLocalFragment = (scope, fragment) => fragment?.getIdObject().prefix === lokiScope.NAMESPACE_ID;
	const routingRule = (scope, fragment) => (!fragment || isLocalFragment(scope, fragment)) ? lokiScope : apolloCoreScope;

	const muxScope = scope
		.apply(installScopedAPIPlatform)
		.apply(installFragmentInterface)
		.apply(installRouter, routingRule, ['update', 'read', 'subscribeToFragment'])
		.extend({
			// Add child cores to scope so downstream plugins can access them if needed.
			localScope: lokiScope,
			externalScope: apolloCoreScope,
		})
		.extendAPI({
			queryServer: (scope, ...args) => scope.externalScope.API.getAll().queryRootSet(...args),
			// Queries route to local core by default (which includes all read/write mapping calls).
			queryRootSet: (scope, ...args) => scope.localScope.API.getAll().queryRootSet(...args),
			// Creation routes to local core by default
			create: (scope, ...args) => scope.localScope.API.getAll().create(...args),
		})
		.apply(installFragmentFormatValidation)
		.extendAPI({
			// Do we need to validate server queries? They might already be validated on server side.
			queryServer: async function * (scope, query, ...args) {
				const { queryServer: queryServerUnsafe } = scope.unsafeScope.API.getAll();
				const { validateContentFormat } = scope.API.getAll();

				for await (const { fragment, content } of queryServerUnsafe(query, ...args)) {
					validateContentFormat(content);
					yield { fragment, content };
				}
			},
		})
		// We need to validate local queries.
		.apply(installValidatedQueryPlugin)
		.apply(installMappingPlugin)
		.apply(installCoreUtilsPolyfill);

	return muxScope.extendAPI({
		// Un-used first argument since API platform expects all functions to accept scope as first argument.
		isLocalFragment: (scope, fragment) => isLocalFragment(scope, fragment),

		// This allows us to create fragments on the server, because normally calling create() on the client will
		// create a local fragment. This is also good for safety, and allows us to explicitly create external fragments
		// without going through complex mechanisms like autofork and sync and merge and such.
		createExternal: async (scope, fragment, newValues, ...rest) =>
			await scope.externalScope.API.getAll().create(fragment, newValues, ...rest),

		// TODO: if any of the child cores provide optimized core util functions, we should have
		//       them bypass core-utils polyfill (though they should still go through format validation).
		//       For example see overwriteUnsafePolyfill() above

		areFragmentsEqual: (scope, fragment1, fragment2) => fragment1.equals(fragment2), // Add this to CoreUtils Polyfill
	});
}
