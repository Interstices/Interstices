/**
 * Call webpack and webpack dev server programmatically (following https://webpack.js.org/api/webpack-dev-server/).
 * We need to do this for two reasons:
 *
 * Our webpack config imports mongodb-core using ES6 imports, which will produce error:
 *     The requested module 'mongodb' is a CommonJS module, which may not support all module.exports as named exports.
 *
 * So we use esm to transpile the config on the fly, instead of refactoring mongodb-core entirely.
 *
 * Also, our webpack config uses symlinks (bc of lerna), and if we don't tell node to preserve symlinks we
 * run into dependency resolution issues.
 * So we have to call webpack programmatically and then pass in --preserve-symlinks from our npm scripts,
 * eg `"dev": "node --preserve-symlinks dev.js"`
 */
const esm = require('esm-wallaby')(module);
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const { default: webpackConfig } = esm('./webpack.config.js');

const compiler = webpack(webpackConfig);
const devServerOptions = { ...webpackConfig.devServer, open: true };
const server = new WebpackDevServer(devServerOptions, compiler);

const runServer = async () => {
	console.log('Starting server...');
	await server.start();
};

runServer();
