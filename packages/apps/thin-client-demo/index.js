import React from 'react';
import ReactDOM from 'react-dom';
import baseScope from '@interstices/environment';
import { installApolloClientCoreExtended } from '@interstices/apollo-client-core';
import installBaseFormats from '@interstices/base-formats';
import installTestFormats from '@interstices/test-formats';
import installBaseListPlugin from '@interstices/base-list-handler';
import installListPlugin from '@interstices/list-handler';
import AppContainer from './components/AppContainer.jsx';
import App from './components/App.jsx';

const scope = baseScope
	.apply(installApolloClientCoreExtended)
	.apply(installBaseFormats)
	.apply(installTestFormats)
	.apply(installBaseListPlugin)
	.apply(installListPlugin);

ReactDOM.render(<AppContainer scope={scope}><App/></AppContainer>, document.querySelector('#root'));
