import { useContext, useEffect, useState } from 'react';
import FolderTreeTextEditor from '@interstices/folder-tree-text-editor';
import { IntersticesContext } from '@interstices/base-ui';
import { TestFolderTreeFormat } from '@interstices/test-formats';

async function getFirstAsyncItem(asyncIterator) {
	return (await asyncIterator.next())?.value;
}

const App = () => {
	const [workspace, setWorkspace] = useState(null);
	const scope = useContext(IntersticesContext);

	useEffect(() => {
		(async () => {
			const { queryRootSet: queryServer } = scope.API.getAll();

			// Use query to get latest folder tree (since one is generated every time thin-client-demo is run).
			const { content: folderTreeContent } = await getFirstAsyncItem(
				queryServer({ type: TestFolderTreeFormat.type }, [['createdTS', 'descending']], 1),
			);
			const { rootFolder } = folderTreeContent;
			console.log(`Found root folder: ${rootFolder.getIdString()}`);
			setWorkspace(rootFolder);
		})();
	}, [scope]);

	return (
		workspace
			? <FolderTreeTextEditor root={workspace}/>
			: <p>Loading...</p>
	);
};

export default App;
