import { CustomApolloProvider } from '@interstices/apollo-client-core';
import { IntersticesContext } from '@interstices/base-ui';

const AppContainer = ({ scope, children }) => (
	<IntersticesContext.Provider value={scope}>
		<CustomApolloProvider>
			{children}
		</CustomApolloProvider>
	</IntersticesContext.Provider>
);

export default AppContainer;
