### Development

run `npm run dev` and go to http://localhost:3000 to see the app.
Or go to http://localhost:3000/api/graphql to see graphql playground.

1. run `npm run dev`
2. go to http://localhost:3000 and click "Fetch" button once (leave input box empty for now)
3. go back to server-side console, and you should see `Root folder id: <some fragment id>` printed
4. use that fragment id, and paste it into the input box on the client-side page
5. click "Fetch" and you should see a tree of folders and fragments
6. click on a fragment to view and edit it
