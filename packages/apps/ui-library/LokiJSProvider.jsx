import React, { useState, useEffect } from 'react';
import baseScope from '@interstices/environment';
import { installLokiJSCoreExtended } from '@interstices/lokijs-core';
import installBaseFormats from '@interstices/base-formats';
import installTestFormats from '@interstices/test-formats';
import { IntersticesContext } from '@interstices/base-ui';

const lokiScope = baseScope
	.apply(installLokiJSCoreExtended)
	.apply(installBaseFormats)
	.apply(installTestFormats);

/**
 * The setupEnvironment function can be used to install any extra plugins that your UI component might need.
 * In addition, because it is async (unlike most plugin installer functions), it can also be used for initializing
 * mock data into the local filesystem.
 *
 * @param {*} param0
 * @returns A Provider component.
 */
const LokiJSProvider = ({ children, setupEnvironment }) => {
	const [scope, setScope] = useState(null);

	useEffect(() => {
		(async () => {
			setScope(await setupEnvironment(lokiScope));
		})();
	}, [setupEnvironment]);

	if (!scope) {
		return 'loading...';
	}

	return (
		<IntersticesContext.Provider value={scope}>
			{children}
		</IntersticesContext.Provider>
	);
};

export default LokiJSProvider;
