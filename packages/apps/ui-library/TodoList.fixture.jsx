import { useContext } from 'react';
import TodoList from '@interstices/todo-list';
import installListPlugin from '@interstices/list-handler';
import installBaseListPlugin from '@interstices/base-list-handler';
import LokiJSProvider from './LokiJSProvider';
import { IntersticesContext } from '@interstices/base-ui';
import loadJSONData from '@interstices/json-loader';
import mockData from '@interstices/mock-data/todo-list.js';
import { TodoListFormatGroup } from '@interstices/base-formats';

const MOCK_DATA_ROOT_KEY = 1;

async function setupEnvironment(lokiScope) {
	const scope = lokiScope
		.apply(installBaseListPlugin)
		.apply(installListPlugin);

	const root = await loadJSONData(scope, mockData, [TodoListFormatGroup], MOCK_DATA_ROOT_KEY);
	return scope.extend({ root });
}

const Wrapper = () => {
	const scope = useContext(IntersticesContext);
	return <TodoList root={scope.root}/>;
};

export default (
	<LokiJSProvider setupEnvironment={setupEnvironment}>
		<Wrapper/>
	</LokiJSProvider>
);
