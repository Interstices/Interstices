import { useContext, useEffect, useState } from 'react';
import FolderTreeTextEditor from '@interstices/folder-tree-text-editor';
import installListPlugin from '@interstices/list-handler';
import installBaseListPlugin from '@interstices/base-list-handler';
import LokiJSProvider from './LokiJSProvider';
import { IntersticesContext } from '@interstices/base-ui';
import loadJSONData from '@interstices/json-loader';
import mockData from '@interstices/mock-data/folder-tree-text-editor';
import { ListFormatGroup } from '@interstices/base-formats';
import { TestFolderTreeFormat } from '@interstices/test-formats';

async function setupEnvironment(lokiScope) {
	const scope = lokiScope
		.apply(installBaseListPlugin)
		.apply(installListPlugin);

	const ROOT_KEY = 0;
	// App fetches root by query and looks for the latest createdTS, so set createdTS to current time.
	mockData[ROOT_KEY].createdTS = Date.now();
	await loadJSONData(scope, mockData, [ListFormatGroup]);

	return scope;
}

async function getFirstAsyncItem(asyncIterator) {
	return (await asyncIterator.next())?.value;
}

const Wrapper = () => {
	const scope = useContext(IntersticesContext);
	const [rootFolder, setRootFolder] = useState(null);

	useEffect(() => {
		(async () => {
			const { queryRootSet, readProperty } = scope.API.getAll();
			const { fragment: root } = await getFirstAsyncItem(
				queryRootSet({ type: TestFolderTreeFormat.type }, ['createdTS', 'descending'], 100));
			setRootFolder(await root.$(readProperty, 'rootFolder'));
		})();
	}, [scope]);

	if (!rootFolder) {
		return <p>Loading...</p>;
	}

	return <FolderTreeTextEditor root={rootFolder}/>;
};

export default (
	<LokiJSProvider setupEnvironment={setupEnvironment}>
		<Wrapper/>
	</LokiJSProvider>
);
