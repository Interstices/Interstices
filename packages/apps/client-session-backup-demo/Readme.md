### Development

run `npm run dev` and go to http://localhost:3000 to see the app

1. run `npm run dev`
2. click on a fragment to view and edit it
	* try making some offline changes! Use chrome dev tools > Network and change "No Throttling" to "Offline". Make some changes. Then turn it back to "No Throttling" before pushing your changes.
3. then click "Push all fragments" to push all changes to server
4. if you refresh the page, and re-fetch all fragments using the root folder id from earlier, your edits should be there
