import { FragmentMap } from '@interstices/fragment';

/**
 * This is a rough approximation of subscribing to all fragments in the client's root collection, by using the client's
 * namespace/prefix to identify the fragments.
 * There are a few edge cases that this implementation does not handle:
 * 	1. if the client's root collection contains fragments from other peers (and thus would not use the client's namespace)
 * 	2. if the client doesn't use the prefixed id format
 *
 * A more general plugin for subscribing to all fragments in a collection, should be able to replace this as well as
 * handle all the edge cases
 */
export default function installSubscribeToRootCollectionPlugin(scope) {
	return scope
		.extend({
			namespaceListeners: new Map(),
			backupRootHandlers: new FragmentMap(), // Used in apollo resolvers.
			backupForkHandlers: new FragmentMap(), // Used in apollo resolvers.
		})
		.extendAPI({
			subscribeToCollection(scope, collection, listener) {
				// TODO: support subscribing to any kind of collection.
				if (collection.getIdObject().tail !== 'root') {
					throw Error('subscribeToCollection() currently only supports subscribing to root collections');
				}

				// TODO: more robust handling of this case. For example if the server tried to do a session backup of
				//       itself, it would create a new file every time a file was created, leading to an infinite loop.
				if (collection.getIdObject().prefix === scope.NAMESPACE_ID) {
					throw Error('Dangerous! Do not subscribe to your own collection, may lead to infinite loops.');
				}

				const namespace = collection.getIdObject().prefix;

				if (!scope.namespaceListeners.has(namespace)) {
					scope.namespaceListeners.set(namespace, new Set());
				}

				scope.namespaceListeners.get(namespace).add(listener);

				// Return unsubscribe function
				return () => {
					if (!scope.namespaceListeners.get(namespace)) {
						return false;
					}

					const wasRemoved = scope.namespaceListeners.get(namespace).delete(listener);
					if (scope.namespaceListeners.get(namespace).size === 0) {
						scope.namespaceListeners.delete(namespace);
					}

					return wasRemoved;
				};
			},
		})
		.replaceAPI('publishFragmentChanged', oldPublishFragmentChangedFn => (
			/**
			 * ExtraArgs: in case anybody else wrapped publishFragmentChanged() and added extra arguments before
			 *            this plugin, we need to forward those extra arguments.
			 */
			(scope, fragment, data, ...extraArgs) => {
				const namespace = fragment.getIdObject().prefix;
				if (scope.namespaceListeners.has(namespace)) {
					for (const listener of scope.namespaceListeners.get(namespace)) {
						listener(data);
					}
				}

				return oldPublishFragmentChangedFn(scope, fragment, data, ...extraArgs);
			}
		));
}
