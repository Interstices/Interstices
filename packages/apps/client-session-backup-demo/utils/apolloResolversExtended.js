import { gql } from 'apollo-server-micro';
import { defaultTypeDefs, createDefaultResolvers, fromQueryVariableId, deserializeFragmentContent } from '@interstices/apollo-server-lib';
import { FragmentMap } from '@interstices/fragment';
import { ClientSessionBackupMapping } from '@interstices/base-formats';

/**
 * Job queues are keyed by fragment, so only when a function is called with the same fragment will it be serialized.
 * TODO: REPLACE THIS WITH AN INDUSTRY-STANDARD SOLUTION
 */
class KeyedJobQueues {
	fragmentQueues = new FragmentMap();
	rawValueQueues = new Map();

	getQueuesForVal = (scope, key) => scope.API.getAll().isFragment(key) ? this.fragmentQueues : this.rawValueQueues;

	addJob(scope, key, job) {
		return new Promise((resolve, reject) => {
			const queues = this.getQueuesForVal(scope, key);
			// Notice how the block below is all synchronous. This is to prevent race conditions.
			if (queues.has(key)) {
				// Job queue for key is already running, so just push to job queue
				queues.get(key).push({ job, resolve, reject });
			} else {
				queues.set(key, []);
				queues.get(key).push({ job, resolve, reject });
				this.runJobs(scope, key);
			}
		});
	}

	async runJobs(scope, key) {
		const queues = this.getQueuesForVal(scope, key);
		const queue = queues.get(key);
		// Notice that if queue.length reaches 0, this.queues.delete(fragment) will be called
		// synchronously right afterwards to prevent race conditions
		while (queue.length > 0) {
			const { job, resolve, reject } = queue.shift();
			try {
				// eslint-disable-next-line no-await-in-loop
				resolve(await job());
			} catch (e) {
				reject(e);
			}
		}

		queues.delete(key);
	}
}

// A shared job queue per fragment for all backup-related operations on that fragment,
// to ensure that we don't create multiple backups for a single fragment.
const jobQueues = new KeyedJobQueues();

/**
 * Serializes all calls to an asynchronous function.
 * CAUTION: if a synchronized function ever calls itself, it will deadlock!
 * Even if the call has different arguments, or is indirect recursion.
 * Consider the following:
 * factorial = (x) => x * g(x-1)
 * g = (x) => x === 0 ? 1 : factorial(x)
 * Trying to synchronize factorial() will cause a deadlock.
 * This is why it is best to use synchronize() within plugins that use Scoped API Platform,
 * since Scoped API Platform makes it so API calls will travel to outer scopes, and reduces
 * the chance that a function will call itself.
 */
function synchronize(asyncFn) {
	return async (scope, value, ...rest) => await jobQueues.addJob(scope, value, () => asyncFn(scope, value, ...rest));
}

const clientBackupTypeDefs = gql`
enum CollectionChange {
	ADDED,
	REMOVED,
}

extend type Mutation {
	publishFragmentChanged(idJson: String!, changedFragmentContent: FragmentContentInput!): Boolean!
	publishRootCollectionChanged(rootIdJson: String!, idJson: String!, change: CollectionChange!): Boolean!
	shareRootCollection(rootIdJson: String!): Boolean!
}
`;

export const typeDefs = [defaultTypeDefs, clientBackupTypeDefs];

const HANDLER_PLACEHOLDER = 'placeholder';

export const createResolvers = scope => {
	const defaultResolvers = createDefaultResolvers(scope);

	return {
		...defaultResolvers,
		Mutation: {
			...defaultResolvers.Mutation,
			async publishFragmentChanged(_, { idJson, changedFragmentContent }) {
				const { publishFragmentChanged } = scope.API.getAll();

				const fragment = fromQueryVariableId(scope, idJson);
				const { json } = changedFragmentContent;
				const changedValues = await deserializeFragmentContent(scope, json);
				publishFragmentChanged(fragment, { fragment, changedValues });
				console.log(`Client fragment ${fragment.getIdString()} changed.`);
				return true;
			},
			async publishRootCollectionChanged(_, { rootIdJson, idJson, change }) {
				const { backupRootHandlers, backupForkHandlers } = scope;

				const clientRootFragment = fromQueryVariableId(scope, rootIdJson);
				console.log(`Received root collection change for root ${clientRootFragment.getIdString()}`);

				if (!backupRootHandlers.has(clientRootFragment)
						|| backupRootHandlers.get(clientRootFragment) === HANDLER_PLACEHOLDER
						|| !backupForkHandlers.has(clientRootFragment)) {
					throw Error('Tried to use client root collection handler before it was initialized '
						+ `for client ${clientRootFragment.getIdObject().prefix}`);
				}

				const backupRootHandler = backupRootHandlers.get(clientRootFragment);
				const forkHandler = backupForkHandlers.get(clientRootFragment);
				const fragment = fromQueryVariableId(scope, idJson);

				return await jobQueues.addJob(scope, fragment, async () => {
					let backupFragment = await forkHandler.getForkFragment(fragment);

					if (backupFragment) {
						// TODO: currently we don't handle out-of-order events.
						throw Error(`Client backup: a change event was received before an add-to-root event was received for fragment ${fragment.getIdString()}`);
					} else {
						// Pass skipContent as true since fork handler won't be able to call read() on the client fragment anyways.
						backupFragment = await forkHandler.createFork(fragment, true);
					}

					switch (change) {
						case 'ADDED':
							await backupRootHandler.add(backupFragment);
							break;
						case 'REMOVED':
							await backupRootHandler.remove(backupFragment);
							break;
						default: throw Error('Unknown collection change');
					}

					return true;
				});
			},
			async shareRootCollection(_, { rootIdJson }) {
				const { backupRootHandlers, backupForkHandlers } = scope;
				const { read, createSet, loadSet, readMapping, writeMapping, subscribeToCollection } = scope.API.getAll();

				// We keep backup-root handlers in memory so we don't have to re-create them every time we want to change the backup-root.
				// TODO: if a client hasn't been connected in a long time, remove it's backup-root handler from memory.
				//       Otherwise we will just accumulate handlers without bound (though individually they are rather cheap).
				//       If a client re-connects after a long time, we can just re-create the backup-root handler.
				if (!backupRootHandlers) {
					throw Error('Please install SubscribeToRootCollection plugin first');
				}

				const clientRootFragment = fromQueryVariableId(scope, rootIdJson);
				console.log(`Received new root collection ${clientRootFragment.getIdString()}`);

				if (backupRootHandlers.has(clientRootFragment)) {
					return true; // Prevent duplicate subscriptions.
				}

				// Synchronously add a placeholder to prevent race conditions with the check above.
				backupRootHandlers.set(clientRootFragment, HANDLER_PLACEHOLDER);

				let backupRootHandler;

				const existingBackupRootMapping = await readMapping(ClientSessionBackupMapping, clientRootFragment);
				if (existingBackupRootMapping) {
					const { backup } = await existingBackupRootMapping.$(read);
					backupRootHandler = await loadSet(backup);
				} else {
					backupRootHandler = await createSet();
					const backup = backupRootHandler.getSetContainer();
					await writeMapping(ClientSessionBackupMapping, clientRootFragment, { backup });
				}

				backupRootHandlers.set(clientRootFragment, backupRootHandler);

				// IMPORTANT! We use "unsafe" scope because we don't validate session backup files since we want
				// them to be perfect copies of the client files, even if the format is invalid.
				// The validation only matters when the server tries to merge the client file contents into the server files.
				// TODO: using the unsafe scope means that we are also not validating the mapping files written by the fork handler.
				//       One way to get around this is to create a special scope where writeMapping() is routed to the safe/validated
				//       scope, or even better, use safe scope by default and somehow only allow forkHandler.updateForkContent to
				//       make unsafe writes.
				const { loadForkRootSync } = scope.unsafeScope.API.getAll();
				const rootNamespace = clientRootFragment.getIdObject().prefix;
				const isClientFragment = async (scope, fragment) => fragment.getIdObject().prefix === rootNamespace;

				const forkHandler = loadForkRootSync(isClientFragment, null, false, ClientSessionBackupMapping, 'backup');

				backupForkHandlers.set(clientRootFragment, forkHandler);

				const synchronizedListener = synchronize(async (scope, fragment, changedValues) => {
					console.log(`Received change for client fragment ${fragment.getIdString()}`);

					const backup = await forkHandler.getForkFragment(fragment);

					if (!backup) {
						throw Error(`Client backup: a change event was received before an add-to-root event was received for fragment ${fragment.getIdString()}`);
					}

					// There is an extremely unlikely edge case where a fragment on one client points to
					// a fragment on another client. In which case, we will have a backup for the referenced
					// fragment, and we currently will remap the reference. However, if we want to preserve
					// those references, we need to scope this `readMapping` call to only query within
					// the local backups for this client session.
					await forkHandler.updateForkContent(fragment, changedValues);
				});

				subscribeToCollection(clientRootFragment, ({ fragment, changedValues }) => synchronizedListener(scope, fragment, changedValues));
				return true;
			},
		},
	};
};
