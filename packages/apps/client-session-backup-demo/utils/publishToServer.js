import { apolloClient, toQueryVariableId, serializeFragmentContent } from '@interstices/apollo-client-core';
import { gql } from '@apollo/client';

export const PUBLISH_FRAGMENT_CHANGED = gql`
	mutation PublishFragmentChanged($idJson: String!, $changedFragmentContent: FragmentContentInput!) {
		publishFragmentChanged(idJson: $idJson, changedFragmentContent: $changedFragmentContent)
	}
`;

export const PUBLISH_ROOT_COLLECTION_CHANGED = gql`
	mutation PublishRootCollectionChanged($rootIdJson: String!, $idJson: String!, $change: CollectionChange!) {
		publishRootCollectionChanged(rootIdJson: $rootIdJson, idJson: $idJson, change: $change)
	}
`;

export const SHARE_ROOT_COLLECTION = gql`
	mutation ShareRootCollection($rootIdJson: String!) {
		shareRootCollection(rootIdJson: $rootIdJson)
	}
`;

async function publishFragmentAddedToRoot(scope, fragment) {
	const { getRootSetContainer } = scope.unsafeScope.localScope.API.getAll();
	const rootIdJson = toQueryVariableId(getRootSetContainer());
	const idJson = toQueryVariableId(fragment);
	console.log(`Publish: local fragment ${idJson} added to root collection.`);
	const { data } = await apolloClient.mutate({
		mutation: PUBLISH_ROOT_COLLECTION_CHANGED,
		variables: {
			rootIdJson,
			idJson,
			change: 'ADDED',
		},
	});
	return data?.publishRootCollectionChanged;
}

async function publishFragmentChangedToServer(scope, fragment, changedValues) {
	// Note that we only publish the change in content, not the entire content.
	// This is because if we have multiple atomic change groups that change the same fragment, we still want to
	// publish each change to separate events (also once we start reading from the local db to retrieve the full content,
	// we have to start taking into account race conditions).

	const idJson = toQueryVariableId(fragment);
	console.log(`Publish: change for local fragment ${idJson}`);
	const { data } = await apolloClient.mutate({
		mutation: PUBLISH_FRAGMENT_CHANGED,
		variables: {
			idJson,
			changedFragmentContent: { json: await serializeFragmentContent(scope, changedValues) },
		},
	});
	return data?.publishFragmentChanged;
}

export default function installPublishToServerPlugin(scope) {
	const { getRootSetContainer } = scope.unsafeScope.localScope.API.getAll();
	const rootIdJson = toQueryVariableId(getRootSetContainer());
	console.log(`Sharing root ${getRootSetContainer().getIdString()}`);
	apolloClient.mutate({
		mutation: SHARE_ROOT_COLLECTION,
		variables: { rootIdJson },
	});

	// Right now we manually hook into create() and update() and publish to server.
	// This means we have two pub-sub systems running on the client:
	//   1. the local pub-sub created by the lokijs-core and core-pub-sub, and used by the client UI
	//   2. the external pub-sub created here and used by the server.
	// We should combine both systems into a single system, that can publish and route events to both local
	// and external subscribers.
	// We should also probably look into distributed pub-sub systems like kafka or rabbitmq.
	return scope
		.replaceAPI('create', oldCreateFn => (
			/**
			 * ExtraArgs: in case anybody else wrapped create() and added extra arguments before PublishToServer could wrap create(),
			 *            we need to forward those extra arguments.
			 */
			async (scope, newValues, ...extraArgs) => {
				const fragment = await oldCreateFn(scope, newValues, ...extraArgs);
				await publishFragmentAddedToRoot(scope, fragment); // We don't actually need to await here, but it's an easy way to propagate async errors.
				await publishFragmentChangedToServer(scope, fragment, newValues);
				return fragment;
			}
		))
		.replaceAPI('update', oldUpdateFn => (
			/**
			 * ExtraArgs: in case anybody else wrapped update() and added extra arguments before PublishToServer could wrap update(),
			 *            we need to forward those extra arguments.
			 */
			async (scope, fragment, newValues, ...extraArgs) => {
				if (Object.keys(newValues).length === 0) {
					return true; // No update or publish needed
				}

				const result = await oldUpdateFn(scope, fragment, newValues, ...extraArgs);
				await publishFragmentChangedToServer(scope, fragment, newValues); // We don't actually need to await here, but it's an easy way to propagate async errors.
				return result;
			}
		));
}
