Apollo Server Sandbox
======================

Example usage of apollo-server-lib

## Run

run `npm start`, then go to http://localhost:3000/api/graphql, and then try the following query using a fragment you already have in your mongodb database (you can create it using something like list-handler-test to generate a few fragments).

```gql
{
	fragmentContent(idJson: "{\"id\":\"<full fragment id>\"}") {
		json
	}
}
```

or a mutation like

```gql
mutation {
  updateFragmentContent(idJson: "{\"id\":\"<full fragment id>\"}", fragmentContent: {
    json: "{\"someKey\":\"hello world\"}"
  })
}
```
