import baseScope from '@interstices/environment';
import { installMongoDBCoreExtended } from '@interstices/mongodb-core';
import installApolloServer from '@interstices/apollo-server-lib';

const scope = baseScope
	.apply(installMongoDBCoreExtended, { bypassWriteChecks: true, bypassReadChecks: true })
	.apply(installApolloServer);

const { startApolloServer, getApolloHandler } = scope.API.getAll();

// Returns a handler (promise) for Micro server to use
export default startApolloServer().then(() => getApolloHandler());
