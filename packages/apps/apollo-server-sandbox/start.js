/**
 * Ideally we would just call 'micro' directly from our npm script commands, and it would
 * automatically pull the handler from index.js.
 * But we run into issues since we use ES6 imports, but Micro uses require().
 * So we call micro programmatically and then use esm to import it using ES6 imports.
 * In the future we might use typescript and have the typescript compiler handle
 * transpiling all dependencies.
 */
import micro from 'micro';
import { GRAPHQL_PATH } from '@interstices/apollo-server-lib';
import apolloHandlerPromise from './index.js';

apolloHandlerPromise.then(async apolloHandler => {
	// https://www.npmjs.com/package/micro#programmatic-use
	// Don't follow github.com/vercel/micro#programmatic-use, this is for the canary branch of micro
	const server = micro(apolloHandler);

	await new Promise(resolve => { server.listen(3000, resolve); });

	console.log(`🚀  Server ready! go to localhost://3000${GRAPHQL_PATH} to see the graphql playground`);
});
