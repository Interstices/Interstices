/**
 * For functions that should only be called on the server-side.
**/
import { FRAGMENT_LINK_PREFIX, RAW_VALUE_PREFIX } from './fragmentUtils';
import GlobalScope from '../server/scope';

function addPrefix(value) {
	const { isFragment } = GlobalScope.API.getAll();

	if (isFragment(value)) {
		return FRAGMENT_LINK_PREFIX + value.getIdString();
	}

	if (typeof value === 'string') {
		return RAW_VALUE_PREFIX + `"${value}"`;
	}

	if (typeof value === 'object' && value !== null) {
		// TODO: if we do want to support nested objects, we might also want to detect
		//       circular references so the JSON.stringify replacer doesn't loop infinitely.
		throw Error('Error: nested objects are currently unsupported.');
	}

	return RAW_VALUE_PREFIX + String(value);
}

export async function serializeFragmentContent(fragment) {
	const { read } = GlobalScope.API.getAll();

	const fragmentContent = {
		// Normally we don't expose fragment ids outside the core,
		// but mongodb-explorer is an exception
		_id: fragment.getIdString(),
		...await fragment.$(read),
	};
	return JSON.stringify(fragmentContent, (key, val) => key === '' ? val : addPrefix(val));
}
