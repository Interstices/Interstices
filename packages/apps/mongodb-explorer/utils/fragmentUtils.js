/**
 * Can be called on server or client
**/

export const FRAGMENT_LINK_PREFIX = 'fragment://';
export const RAW_VALUE_PREFIX = 'raw://'; // For primitives like strings, booleans, or numbers

export const FRAGMENT_LINK_PREFIX_REGEX = new RegExp('^' + FRAGMENT_LINK_PREFIX);
export const RAW_VALUE_PREFIX_REGEX = new RegExp('^' + RAW_VALUE_PREFIX);

export const isFragmentString = str => FRAGMENT_LINK_PREFIX_REGEX.test(str);

/**
 * For raw values, removes prefix and parses value. Leaves fragment links as-is.
 * @param {*} json A JSON string produced by serializeFragmentContent()
 * @returns an object
 */
export function deserializeFragmentContent(json) {
	return JSON.parse(json, (key, value) => {
		if (RAW_VALUE_PREFIX_REGEX.test(value)) {
			const withoutPrefix = value.replace(RAW_VALUE_PREFIX_REGEX, '');
			return JSON.parse(withoutPrefix);
		}

		return value;
	});
}
