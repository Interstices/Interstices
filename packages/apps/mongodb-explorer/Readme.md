### Development

run `npm run dev` and go to http://localhost:3000 to see the app.

Note that mongodb-explorer does not depend on apollo-server-lib because it is meant to be a stand-alone example that only depends on mongodb-core and base-ui.
