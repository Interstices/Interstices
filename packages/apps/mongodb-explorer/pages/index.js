import { useRef, useState } from 'react';
import styled from '@emotion/styled';
import CustomApolloProvider from '../components/CustomApolloProvider';
import FragmentFetcher from '../components/FragmentFetcher';
import HelpButton from '../components/HelpButton';
import TreeFragmentViewer from '../components/TreeFragmentViewer';
import { Box, Page } from '@interstices/base-ui/components';

const HELP_TEXT = 'Enter a fragment id to begin. In the fragment viewer, if a fragment property points to another fragment, '
	+ 'it will be in purple, and you can click to open the fragment in a nested view. You can also hover over '
	+ 'these fragment links and if the fragment is open elsewhere in the tree, it will be highlighted with a purple border.';

const FixedOverlayContainer = styled(Box)`
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	pointer-events: none;
`;

const Home = () => {
	const [idString, setIdString] = useState('');
	const fixedOverlayRef = useRef(null);

	return (
		<CustomApolloProvider>
			<Page>
				<FixedOverlayContainer ref={fixedOverlayRef}>
					<HelpButton
						styling="top: 32px; right: 32px; pointer-events: auto;"
						helpText={HELP_TEXT}
						overlayContainerRef={fixedOverlayRef}/>
				</FixedOverlayContainer>
				<FragmentFetcher setIdString={setIdString}/>
				<TreeFragmentViewer idString={idString}/>
			</Page>
		</CustomApolloProvider>
	);
};

export default Home;
