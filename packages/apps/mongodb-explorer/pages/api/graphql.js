import { ApolloServer } from 'apollo-server-micro';
import typeDefs from '../../graphql/typeDefs';
import resolvers from '../../graphql/resolvers';

console.log('starting - api/graphql.js');

const apolloServer = new ApolloServer({
	typeDefs,
	resolvers,
	subscriptions: {
		path: '/api/subscriptions',
		keepAlive: 9000,
		onConnect: () => console.log('Subscriptions client connected'),
		onDisconnect: () => console.log('Subscriptions client disconnected'),
	},
	playground: {
		subscriptionEndpoint: '/api/subscriptions',
		settings: {
			'request.credentials': 'same-origin',
		},
	},
});

export const config = {
	api: {
		bodyParser: false,
	},
};

// Dynamically inject subscription handlers on first request. See https://stackoverflow.com/a/66268643
const graphqlWithSubscriptionHandler = (req, res, next) => {
	const boundApolloServer = res.socket.server.apolloServer;
	// During a hot-reload, this file will executed again, and `apolloServer` will be re-instantiated.
	// We need to install subscription handlers on the new instance.
	if (boundApolloServer && boundApolloServer !== apolloServer) {
		delete res.socket.server.apolloServer;
		// Break old client connections, forcing clients to re-connect
		boundApolloServer?.stop();
	}

	if (!res.socket.server.apolloServer) {
		console.log('* ApolloServer: (re)install subscription handlers *');

		apolloServer.installSubscriptionHandlers(res.socket.server);
		res.socket.server.apolloServer = apolloServer;
		const handler = apolloServer.createHandler({ path: '/api/graphql' });
		res.socket.server.apolloServerHandler = handler;
	}

	return res.socket.server.apolloServerHandler(req, res, next);
};

export default graphqlWithSubscriptionHandler;
