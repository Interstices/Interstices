import { gql } from '@apollo/client';
import styled from '@emotion/styled';
import CustomApolloProvider from '../components/CustomApolloProvider';
import FragmentFetcher from '../components/FragmentFetcher';
import RawFragmentViewer from '../components/RawFragmentViewer';
import useMutation from '../hooks/useMutationWithNetworkErrorHandling';
import { Box, Button, Page, Row } from '@interstices/base-ui/components';
import { FRAGMENT_LINK_PREFIX_REGEX } from '../utils/fragmentUtils';
import { useState } from 'react';

const stripFragmentLinkPrefix = str => str.replace(FRAGMENT_LINK_PREFIX_REGEX, '');

export const UPDATE_TIMESTAMP = gql`
	mutation UpdateTimestamp($idString: String) {
		updateTimestamp(idString: $idString)
	}
`;

const Wrapper = styled(Box)`
	margin: 16px 0px;
`;

const UpdateTimestampButton = ({ idString }) => {
	const [updateTimestamp] = useMutation(UPDATE_TIMESTAMP, {
		variables: { idString: idString && stripFragmentLinkPrefix(idString) },
		ignoreResults: true,
		onCompleted({ updateTimestamp }) {
			// eslint-disable-next-line no-alert, no-undef
			alert(updateTimestamp ? 'success!' : 'failed...');
		},
	});

	return (
		<Wrapper>
			<Button onClick={updateTimestamp}>Update Timestamp</Button>
		</Wrapper>
	);
};

const Home = () => {
	const [idString, setIdString] = useState('');

	return (
		<CustomApolloProvider>
			<Page>
				<p>{
					'This page is mainly for testing apollo subscriptions. First generate a timestamp fragment by'
					+ 'clicking "Update Timestamp" while leaving the text box empty. The server console should'
					+ 'print out "updated timestamp for fragment XXXX". Copy that fragment id into the text box, and'
					+ 'hit "update Timestamp" a few more times. You should see a timestamp reported under the'
					+ 'textbox, and it should change every time you hit "Update Timestamp".'
				}
				</p>
				<Row>
					<FragmentFetcher setIdString={setIdString}/>
					<UpdateTimestampButton idString={idString}/>
				</Row>
				<RawFragmentViewer idString={idString}/>
			</Page>
		</CustomApolloProvider>
	);
};

export default Home;
