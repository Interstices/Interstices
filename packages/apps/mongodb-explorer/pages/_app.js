/**
 * https://github.com/vercel/next.js/blob/canary/examples/with-emotion/pages/_app.js
 */
import { Global, css, ThemeProvider } from '@emotion/react';
import { light as lightTheme } from '@interstices/base-ui/themes';

const globalStyle = css`
	body {
		margin: 0;
		padding: 0;
		box-sizing: border-box;
	}

	* {
		margin: 0;
		padding: 0;
	}
`;

const App = ({ Component, pageProps }) => (
	<>
		<Global styles={globalStyle}/>
		<ThemeProvider theme={lightTheme}>
			<Component {...pageProps}/>
		</ThemeProvider>
	</>
);

export default App;
