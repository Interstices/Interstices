import { useMemo, useEffect } from 'react';
import { useQuery, gql } from '@apollo/client';
import { FRAGMENT_LINK_PREFIX_REGEX, deserializeFragmentContent } from '../utils/fragmentUtils';

const stripFragmentLinkPrefix = str => str.replace(FRAGMENT_LINK_PREFIX_REGEX, '');

export const GET_FRAGMENT = gql`
query FragmentContent($idString: String!) {
	fragmentContent(idString: $idString) {
		json
	}
}
`;

export const FRAGMENT_SUBSCRIPTION = gql`
subscription OnFragmentChanged($idString: String!) {
	fragmentChanged(idString: $idString) {
		json
	}
}
`;

export default function useFragmentContent(prefixedIdString) {
	const idString = stripFragmentLinkPrefix(prefixedIdString);
	const { data, loading, error, subscribeToMore } = useQuery(GET_FRAGMENT, {
		variables: { idString },
		skip: !idString,
	});

	const fragmentContent = useMemo(() => {
		if (loading || error) {
			return { loading, error };
		}

		if (!idString) {
			return { error: 'fragment not fetched because no fragment id provided' };
		}

		if (!data) {
			return { error: 'no data' };
		}

		const json = data?.fragmentContent?.json;
		try {
			return deserializeFragmentContent(json);
		} catch (err) {
			return { error: err.message, data };
		}
	}, [loading, error, data]);

	useEffect(() => {
		if (!idString) {
			return;
		}

		try {
			const cleanupFn = subscribeToMore({
				document: FRAGMENT_SUBSCRIPTION,
				variables: { idString },
				updateQuery(prev, { subscriptionData }) {
					return subscriptionData.data ? { fragmentContent: subscriptionData.data.fragmentChanged } : prev;
				},
			});
			return cleanupFn;
		} catch (err) {
			// See https://github.com/apollographql/react-apollo/issues/3931
			// and https://github.com/apollographql/apollo-client/issues/6437
			console.warn(`subscribeToMore() threw an error: "${err.message}".`
				+ ' This most likely happened due to a hot-reload/fast-refresh.'
				+ ' It should be fine to ignore this, but if you run into issues later'
				+ ' try refreshing the page or triggering another fast-refresh.');
		}
	}, [idString]);

	return fragmentContent;
}
