import { useMutation as originalUseMutation, ApolloError } from '@apollo/client';

export default function useMutation(mutation, options) {
	return originalUseMutation(mutation, {
		...options,
		onError(err) {
			let fixedError = err;

			// When you send a mutation with malformed variables, it will throw a network error and
			// a graphql error. However it seems like for these kinds of errors, the graphql error gets
			// omitted when constructing the ApolloError object, resulting in unhelpful error messages.
			// However, we can actually extract the graphql errors from the networkError object,
			// and reconstruct the ApolloError with the correct arguments.
			// ApolloError source: https://github.com/apollographql/apollo-client/blob/v3.3.20/src/errors/index.ts#L49
			if (err?.networkError?.result?.errors?.length > 0) {
				fixedError = new ApolloError({
					graphQLErrors: err.networkError.result.errors,
					networkError: err.networkError,
				});
			}

			if (options.onError) {
				options.onError(fixedError);
			} else {
				throw fixedError;
			}
		},
	});
}
