module.exports = {
	webpack(config) {
		if (!config.resolve) {
			config.resolve = {};
		}

		// Unfortunately lerna does not hoist local packages (eg @interstices/environment).
		// So for the following dependency tree:
		//     @interstices/folder-tree-text-editor-server
		//     +-- @interstices/environment
		//     `-- @interstices/folder-tree-text-editor
		//         `-- @interstices/environment (peer dependency)
		// If we followed symlinks, the webpack build from folder-tree-text-editor-server would try
		// to find environment under folder-tree-text-editor. So we need to set `symlinks` to false
		// so that webpack looks for environment under folder-tree-text-editor-server, where it can be found.
		// This can be removed after lerna supports hoisting local deps (see https://github.com/lerna/lerna/issues/2734).
		config.resolve.symlinks = false;
		return config;
	},
};
