import { gql } from 'apollo-server-micro';

export default gql`

# entity definitions

type FragmentContent {
	json: String!
}

# queries, mutations, subscriptions

type Query {
	fragmentContent(idString: String!): FragmentContent
}
type Subscription {
	fragmentChanged(idString: String!): FragmentContent
}
type Mutation {
	updateTimestamp(idString: String): Boolean!
}

`;
