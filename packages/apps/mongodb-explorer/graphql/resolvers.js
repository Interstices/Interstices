import { withFilter } from 'graphql-subscriptions';
import GlobalScope from '../server/scope';
import pubsub, { FRAGMENT_CHANGED_EVENT } from '../server/pubsub';
import { serializeFragmentContent } from '../utils/serverUtils';
import { PrefixedId } from '@interstices/identity';

console.log('starting - resolvers.js');

function fromQueryVariableId(idString) {
	const { toFragment } = GlobalScope.API.getAll();
	return toFragment(PrefixedId.fromIdString(idString));
}

export default {
	Query: {
		async fragmentContent(_, { idString }) {
			console.log(idString);
			const fragment = fromQueryVariableId(idString);
			console.log(`fetching fragment content for fragment ${fragment.getIdString()}`);
			const json = await serializeFragmentContent(fragment);
			return { json };
		},
	},
	Subscription: {
		fragmentChanged: {
			subscribe: withFilter(
				() => pubsub.asyncIterator(FRAGMENT_CHANGED_EVENT),
				async (updatedFragment, variables) => {
					const { idString } = variables;
					const requestedFragment = fromQueryVariableId(idString);
					if (updatedFragment.equals(requestedFragment)) {
						console.log(`updating subscription for fragment ${requestedFragment.getIdString()}`);
					}

					return updatedFragment.equals(requestedFragment);
				},
			),
			resolve: async fragment => ({ json: await serializeFragmentContent(fragment) }),
		},
	},
	Mutation: {
		// MongoDB Explorer skips format validation.
		async updateTimestamp(_, { idString }) {
			const { create: createUnsafe, update: updateUnsafe } = GlobalScope.unsafeScope.API.getAll();
			if (idString) {
				const existingFragment = fromQueryVariableId(idString);
				await updateUnsafe(existingFragment, { timestamp: Date.now() });
				console.log(`updated timestamp for fragment ${existingFragment.getIdString()}`);
			} else {
				const fragment = await createUnsafe({ timestamp: Date.now() });
				console.log(`new timestamp for new fragment ${fragment.getIdString()}`);
			}

			return true;
		},
	},
};
