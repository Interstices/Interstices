import { PubSub } from 'graphql-subscriptions';
import { EventEmitter } from 'events';
import GlobalScope from './scope';

console.log('starting - pubsub.js');

const eventEmitter = new EventEmitter();
eventEmitter.setMaxListeners(100);
const pubsub = new PubSub({ eventEmitter });

export const FRAGMENT_CHANGED_EVENT = 'FRAGMENT_CHANGED';

GlobalScope.API.getAll().subscribeToEvent('update', ({ fragment }) => {
	console.log(`publishing Apollo event for fragment ${fragment.getIdString()}`);
	pubsub.publish(FRAGMENT_CHANGED_EVENT, fragment);
});

export default pubsub;
