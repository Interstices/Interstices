import baseScope from '@interstices/environment';
import { installMongoDBCoreExtended } from '@interstices/mongodb-core';

// Singleton pattern to create a global Interstices scope for the entire mongodb-explorer server.
const scope = baseScope
	.apply(installMongoDBCoreExtended, { bypassWriteChecks: true, bypassReadChecks: true });

export default scope;
