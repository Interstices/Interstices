import { useState } from 'react';
import styled from '@emotion/styled';
import useFragmentContent from '../hooks/useFragment';
import { TableCell, Paragraph, TableRow, Table, TableBody } from '@interstices/base-ui/components';
import { boxShadowBorder } from '@interstices/base-ui/mixins';
import { FRAGMENT_LINK_PREFIX_REGEX } from '../utils/fragmentUtils';

const isFragmentLink = str => FRAGMENT_LINK_PREFIX_REGEX.test(str);

const fontStyle = `
	font-size: 14px;
	line-height: 1.2;
`;

const PropertyCell = styled(TableCell)`
	${fontStyle}
`;

const PropertyKey = styled(PropertyCell)`
	font-weight: bold;
	text-align: right;
	padding-left: 2px;
	padding-right: 2px;
`;

const PropertyValue = styled(PropertyCell)`
	padding-right: 2px;
`;

const FragmentLinkValue = styled(Paragraph)`
	${fontStyle}
	color: ${({ theme }) => theme.colors.primary};
`;

const FragmentLinkRow = styled(TableRow)`
	&:hover {
		background-color: #f4f4f4;
		cursor: pointer;
	}
`;

const FragmentPropertyTable = styled(Table)`
	${({ isActiveFragment, theme }) => isActiveFragment ? boxShadowBorder(1, theme.colors.primary) : ''}
`;

/**
 * Strips the fragment id prefix.
 * @param {*} prefixedIdString something that looks like fragment://<full id string>
 * @returns the fragment id of the fragment corresponding to the given string
 */
function getServerFragmentId(prefixedIdString) {
	const idString = prefixedIdString.replace(FRAGMENT_LINK_PREFIX_REGEX, '');
	return idString;
}

const PropertyViewer = ({ context, propKey, propValue }) => {
	const [isExpanded, setIsExpanded] = useState(false);
	const { setActiveFragmentId } = context;

	if (!isFragmentLink(propValue)) {
		return (
			<TableRow>
				<PropertyKey>{propKey}</PropertyKey>
				<PropertyValue>
					{typeof propValue === 'string' ? propValue : JSON.stringify(propValue)}
				</PropertyValue>
			</TableRow>
		);
	}

	const toggle = () => setIsExpanded(!isExpanded);
	const idString = propValue;
	const hoverHandlers = {
		onMouseEnter: () => isFragmentLink(propValue) && setActiveFragmentId(getServerFragmentId(propValue)),
		onMouseLeave: () => setActiveFragmentId(),
	};

	return (
		<>
			<FragmentLinkRow onClick={toggle} {...hoverHandlers}>
				<PropertyKey>{propKey}</PropertyKey>
				<PropertyValue>
					{ isExpanded
						? <FragmentLinkValue>▼</FragmentLinkValue>
						: <FragmentLinkValue>▶ {idString}</FragmentLinkValue>}
				</PropertyValue>
			</FragmentLinkRow>
			{ isExpanded && (
				<TableRow>
					<PropertyKey/>
					<PropertyValue>
						<TreeFragmentViewer context={context} idString={idString}/>
					</PropertyValue>
				</TableRow>)}
		</>
	);
};

export const TreeFragmentViewer = ({ context, idString }) => {
	const fragmentContent = useFragmentContent(idString);
	const { activeFragmentId } = context;

	return (
		<FragmentPropertyTable isActiveFragment={idString && getServerFragmentId(idString) === activeFragmentId}>
			<TableBody>
				{ /* Use `propKey` since the `key` prop is reserved in React */ }
				{Object.entries(fragmentContent).map(([propKey, propValue]) =>
					<PropertyViewer key={propKey} {...{ context, propKey, propValue }}/>,
				)}
			</TableBody>
		</FragmentPropertyTable>
	);
};

const TreeFragmentViewerContext = ({ idString }) => {
	const [activeFragmentId, setActiveFragmentId] = useState('');
	const context = { activeFragmentId, setActiveFragmentId };

	return <TreeFragmentViewer context={context} idString={idString}/>;
};

export default TreeFragmentViewerContext;
