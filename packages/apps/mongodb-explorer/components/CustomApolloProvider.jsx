import { split, HttpLink, ApolloClient, InMemoryCache,
	ApolloProvider } from '@apollo/client';
import { getMainDefinition } from '@apollo/client/utilities';
import { WebSocketLink } from '@apollo/client/link/ws';

// https://www.apollographql.com/docs/react/data/subscriptions/#3-split-communication-by-operation-recommended
// https://github.com/apollographql/subscriptions-transport-ws/issues/333#issuecomment-359261024
const httpLink = new HttpLink({
	uri: 'api/graphql',
	credentials: 'same-origin',
});

// Process.browser is provided by a NextJS polyfill, see https://stackoverflow.com/a/65018686
// and https://github.com/vercel/next.js/blob/v12.0.7/packages/next/build/webpack-config.ts#L1314
const wsLink = process.browser && new WebSocketLink({
	uri: `ws://${window.location.host}/api/subscriptions`,
	options: { reconnect: true },
});
const link = process.browser ? split(
	({ query }) => {
		const definition = getMainDefinition(query);
		return (definition.kind === 'OperationDefinition' && definition.operation === 'subscription');
	},
	wsLink,
	httpLink,
) : httpLink;

const client = new ApolloClient({
	link,
	cache: new InMemoryCache(),
});

const CustomApolloProvider = ({ children }) => (
	<ApolloProvider client={client}>
		{children}
	</ApolloProvider>
);

export default CustomApolloProvider;
