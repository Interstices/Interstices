import useFragmentContent from '../hooks/useFragment';

const RawFragmentViewer = ({ idString }) => {
	const fragmentContent = useFragmentContent(idString);

	return (
		<pre>{JSON.stringify(fragmentContent, null, 3)}</pre>
	);
};

export default RawFragmentViewer;
