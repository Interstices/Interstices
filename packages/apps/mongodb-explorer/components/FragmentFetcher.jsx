import { useState } from 'react';
import styled from '@emotion/styled';
import { PrimaryButton, Input, Box } from '@interstices/base-ui/components';
import { FRAGMENT_LINK_PREFIX } from '../utils/fragmentUtils';

const Wrapper = styled(Box)`
	margin: 16px 0px;
`;

/**
 * Normally server side fragment ids should never be exposed to the client.
 * However MongoDB Explorer is a special case where the user directly enters a server-side
 * fragment id to explore the contents.
 * In order for this to work, we need to convert the document id to a fragment id.
 * Luckily, the way the mongodb-core generates fragment ids is replicable on the client side
 * @param {string} _id an _id of a mongodb document
 * @returns a valid fragment id
 */
function convertToIdString(_id) {
	return `MongoDB:${_id}`;
}

const FragmentFetcher = ({ setIdString }) => {
	const [mongoId, setFragmentId] = useState('');

	// Every time the button is pressed, we "snapshot" the current input value `mongoId`
	// and save it to `idString`. You can see how this is used in pages/raw-fragment-viewer.js
	// We also attach a prefix to specify that it is a reference to a fragment.
	const onButtonClick = () => setIdString(FRAGMENT_LINK_PREFIX + convertToIdString(mongoId));

	return (
		<Wrapper>
			<Input type="text" value={mongoId} onChange={e => setFragmentId(e.target.value)}/>
			<PrimaryButton onClick={() => onButtonClick()}>Fetch</PrimaryButton>
		</Wrapper>
	);
};

export default FragmentFetcher;
