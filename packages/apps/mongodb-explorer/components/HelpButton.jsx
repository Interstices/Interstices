import { useState } from 'react';
import { createPortal } from 'react-dom';
import styled from '@emotion/styled';
import { Box, CircularButton, Paragraph } from '@interstices/base-ui/components';
import { boxShadowBorder } from '@interstices/base-ui/mixins';

const HelpButtonElem = styled(CircularButton)`
	position: absolute;
	background-color: transparent;
	color: ${({ theme }) => theme.colors.primary};
	${({ theme }) => boxShadowBorder(1, theme.colors.primary)}
	${props => props.styling} // parent passes in additional styling for positioning

	&:hover {
		background-color: ${({ theme }) => theme.colors.primary};
		color: white;
	}
`;

const Overlay = styled(Box)`
	position: absolute;
	background-color: white;
	box-shadow: 2px 3px 1px 5px ${({ theme }) => theme.colors.primary + '30'}; // add 10% transparency

	/* centered */
	left: 50%;
	top: 40%;
	transform: translate(-50%, -50%);

	/* for small screens, fills all the way to 16px away from all sides */
	/* for big screens, max-width 300px */
	width: calc(100% - 2*16px);
	max-width: 600px;
	max-height: 600px;

	border-radius: 8px;
	overflow: auto;
	pointer-events: auto; // in case the container has pointer-events: none
`;

const CloseButton = styled(CircularButton)`
	position: absolute;
	width: 24px;
	height: 24px;
	font-size: 14px;
	line-height: 24px;
	top: 8px;
	right: 8px;
	background-color: white;
	color: ${({ theme }) => theme.colors.primary};
	${({ theme }) => boxShadowBorder(1, theme.colors.primary)}
	font-family: sans-serif;

	&:hover {
		background-color: ${({ theme }) => theme.colors.primary};
		color: white;
	}
`;

const RelativePos = styled(Box)`
	height: 100%;
	position: relative;
`;

const OverlayContent = styled(Box)`
	padding: 16px 28px 16px 16px;
`;

const Popup = ({ children, onClose }) => (
	<Overlay>
		<RelativePos>
			<CloseButton onClick={onClose}>X</CloseButton>
			<OverlayContent>
				{children}
			</OverlayContent>
		</RelativePos>
	</Overlay>
);

const HelpButton = ({ helpText, styling, overlayContainerRef }) => {
	const [showOverlay, setShowOverlay] = useState(false);
	const toggle = () => setShowOverlay(!showOverlay);

	const popup = showOverlay && (
		<Popup onClose={() => setShowOverlay(false)}>
			<Paragraph>{helpText}</Paragraph>
		</Popup>
	);

	return (
		<>
			<HelpButtonElem styling={styling} onClick={toggle}>?</HelpButtonElem>
			{ showOverlay && createPortal(popup, overlayContainerRef?.current || document.body)}
		</>
	);
};

export default HelpButton;
