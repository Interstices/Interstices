
import webpackConfig from '@interstices/npm-dev-configs/webpackDevServer.config.js';

import baseScope from '@interstices/environment';
import { installMongoDBCore, installMongoDBCoreExtensions } from '@interstices/mongodb-core';
import installCoreUtilsPolyfill from '@interstices/core-utils-polyfill';
import installBaseFormats, { ListFormatGroup } from '@interstices/base-formats';
import installTestFormats from '@interstices/test-formats';
import installBaseListPlugin from '@interstices/base-list-handler';
import installListPlugin from '@interstices/list-handler';
import installApolloServer from '@interstices/apollo-server-lib';
import mockData from '@interstices/mock-data/folder-tree-text-editor.js';
import loadJSONData from '@interstices/json-loader';

const scope = baseScope
	.apply(installMongoDBCore)
	.apply(installCoreUtilsPolyfill) // JSON-loader needs overwrite() provided by the unvalidated core API.
	.apply(installMongoDBCoreExtensions)
	.apply(installBaseFormats)
	.apply(installTestFormats)
	.apply(installBaseListPlugin)
	.apply(installListPlugin)
	.apply(installApolloServer);

const setupPromise = (async () => {
	const { startApolloServer } = scope.API.getAll();
	await startApolloServer();
	const ROOT_KEY = 0;
	// App fetches root by query and looks for the latest createdTS, so set createdTS to current time.
	mockData[ROOT_KEY].createdTS = Date.now();
	const rootFolder = await loadJSONData(scope, mockData, [ListFormatGroup], ROOT_KEY);
	console.log(`Root folder id: ${rootFolder._id}`);
})();

// We don't want webpack to serve the page before the apollo server has started,
// so use a middleware before all other middlewares to defer everything until after setup is complete.
async function awaitSetupMiddleware(req, res, next) {
	await setupPromise;
	next();
}

// Caution: that getApolloHandler() can only be called after setup is complete.
async function apolloMiddleware(req, res) {
	const { getApolloHandler } = scope.API.getAll();
	return getApolloHandler()(req, res);
}

export default {
	...webpackConfig,
	devServer: {
		...webpackConfig.devServer,
		setupMiddlewares(middlewares) {
			return [awaitSetupMiddleware, ...middlewares, apolloMiddleware];
		},
	},
};
