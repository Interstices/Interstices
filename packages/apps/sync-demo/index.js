import React from 'react';
import ReactDOM from 'react-dom';
import baseScope from '@interstices/environment';
import installMuxCore from '@interstices/client-muxed-core';
import installBaseFormats from '@interstices/base-formats';
import installTestFormats from '@interstices/test-formats';
import installBaseListPlugin from '@interstices/base-list-handler';
import installListPlugin from '@interstices/list-handler';
import installBaseForkHandler from '@interstices/base-fork-handler';
import installAutoForkPlugin from '@interstices/client-auto-fork';
import installSubmitAllFragmentsPlugin from './utils/PushAllFragmentsPlugin.js';
import AppContainer from './components/AppContainer.jsx';
import App from './components/App.jsx';

const scope = baseScope
	.apply(installMuxCore)
	.apply(installBaseFormats)
	.apply(installTestFormats)
	.apply(installBaseListPlugin)
	.apply(installListPlugin)
	.apply(installBaseForkHandler)
	.apply(installAutoForkPlugin)
	.apply(installSubmitAllFragmentsPlugin);

ReactDOM.render(<AppContainer scope={scope}><App/></AppContainer>, document.querySelector('#root'));
