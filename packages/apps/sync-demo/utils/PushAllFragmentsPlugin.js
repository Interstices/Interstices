import { FragmentMap } from '@interstices/fragment';

async function getChanges(scope, workspace) {
	const { read, isFragment, getExternalFragForLocalFrag } = scope.API.getAll();

	const allFragments = new FragmentMap(); // A map of local fragment => { externalFragment, content }
	const queue = [workspace];
	while (queue.length > 0) {
		const localFragment = queue.shift();
		if (allFragments.has(localFragment)) {
			continue;
		}

		// TODO: parallelize
		// eslint-disable-next-line no-await-in-loop
		const fragmentContent = await localFragment.$(read);
		// eslint-disable-next-line no-await-in-loop
		const externalFragment = await getExternalFragForLocalFrag(localFragment);
		const externalContent = {};
		for (const [key, value] of Object.entries(fragmentContent)) {
			// TODO: check if value is already an external fragment
			if (isFragment(value)) {
				// eslint-disable-next-line no-await-in-loop
				externalContent[key] = await getExternalFragForLocalFrag(value);
				queue.push(value);
			} else {
				externalContent[key] = value;
			}
		}

		allFragments.set(localFragment, { externalFragment, content: externalContent });
	}

	return allFragments.values();
}

async function submitAllFragments(scope, workspace) {
	const { overwrite } = scope.API.getAll();

	const externalFragmentsAndContents = await getChanges(scope, workspace);
	for (const { externalFragment, content } of externalFragmentsAndContents) {
		try {
			// TODO: parallelize
			// eslint-disable-next-line no-await-in-loop
			const isSuccessful = await overwrite(externalFragment, content);
			if (!isSuccessful) {
				throw Error(`failed to push fragment ${externalFragment.getIdString()}`);
			}
		} catch (e) {
			const message = `failed to push fragment ${externalFragment.getIdString()}. ${e?.message || 'Unknown error'}`;
			// eslint-disable-next-line no-alert, no-undef
			alert(message);
			console.error(new Error(message));
			throw e;
		}
	}
}

export default function installSubmitAllFragmentsPlugin(scope) {
	return scope.extendAPI({
		submitAllFragments,
	});
}
