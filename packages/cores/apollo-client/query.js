import { apolloClient } from './components/CustomApolloProvider.jsx';
import { gql } from '@apollo/client';
import { deserializeFragmentContent, fromResultFragmentId } from './utils/deserialize.js';
import { toQueryVariableId } from './utils/serialize.js';

export const QUERY_ROOT_SET = gql`
query QueryRootSet($queryJSON: String!, $sortJSON: String, $limit: Int) {
	queryRootSet(queryJSON: $queryJSON, sortJSON: $sortJSON, limit: $limit) {
		idJson,
		contentJSON
	}
}
`;

function mapValues(obj, fn) {
	return Object.fromEntries(Object.entries(obj).map(([key, val]) => [key, fn(val)]));
}

/**
 * @param {*} scope
 * @param {object} query
 * @param {Array} sort see https://mongodb.github.io/node-mongodb-native/4.3/interfaces/FindOptions.html#sort
 * @param {number} limit default 100, see https://mongodb.github.io/node-mongodb-native/4.3/interfaces/FindOptions.html#limit
 * @returns an async generator for all the results, each result is an object containing { fragment, content }
 */
async function * queryRootSetUnsafe(scope, query, sort, limit = 100) {
	const { isFragment } = scope.API.getAll();

	const queryJSON = JSON.stringify(mapValues(query, value => isFragment(value) ? toQueryVariableId(value) : value));
	const sortJSON = sort && JSON.stringify(sort);

	console.log(`direct query ${queryJSON} with limit ${limit}`);
	const { data } = await apolloClient.query({ query: QUERY_ROOT_SET, variables: { queryJSON, sortJSON, limit } });
	const results = data?.queryRootSet;
	for (const result of results) {
		yield {
			fragment: fromResultFragmentId(scope, result.idJson),
			content: deserializeFragmentContent(scope, result.contentJSON),
		};
	}
}

export function installApolloClientQueryUnsafePlugin(scope) {
	return scope.extendAPI({
		queryRootSet: queryRootSetUnsafe,
	});
}
