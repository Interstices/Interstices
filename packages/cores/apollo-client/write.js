import { apolloClient } from './components/CustomApolloProvider.jsx';
import { gql } from '@apollo/client';
import { serializeFragmentContent, toQueryVariableId } from './utils/serialize';

export const CREATE_FRAGMENT = gql`
	mutation CreateExternalFragment($fragmentContent: FragmentContentInput) {
		createFragment(fragmentContent: $fragmentContent)
	}
`;

export const UPDATE_FRAGMENT = gql`
	mutation UpdateExternalFragment($idJson: String!, $fragmentContent: FragmentContentInput!) {
		updateFragmentContent(idJson: $idJson, fragmentContent: $fragmentContent)
	}
`;

/**
 * @param {IsxFragment} fragment
 * @param {object} newValues (optional) initial content for the fragment
 */
export async function create(scope, newValues) {
	console.log('manual create external fragment');
	const { data } = await apolloClient.mutate({
		mutation: CREATE_FRAGMENT,
		variables: {
			fragmentContent: newValues && { json: await serializeFragmentContent(scope, newValues) },
		},
	});
	return data?.updateFragmentContent;
}

/**
 * @param {IsxFragment} fragment
 * @param {object} newValues new properties that will override existing properties of the fragment
 */
export async function update(scope, fragment, newValues) {
	if (!fragment) {
		throw Error('No fragment provided to update()');
	}

	if (Object.keys(newValues).length === 0) {
		return true; // No update needed
	}

	const idJson = toQueryVariableId(fragment);
	console.log(`manual update to external fragment ${idJson}`);
	const { data } = await apolloClient.mutate({
		mutation: UPDATE_FRAGMENT,
		variables: {
			idJson,
			fragmentContent: { json: await serializeFragmentContent(scope, newValues) },
		},
	});
	return data?.updateFragmentContent;
}
