import { apolloClient } from './components/CustomApolloProvider.jsx';
import { gql } from '@apollo/client';
import { deserializeFragmentContent } from './utils/deserialize.js';
import { toQueryVariableId } from './utils/serialize';

export const GET_FRAGMENT = gql`
query FragmentContent($idJson: String!) {
	fragmentContent(idJson: $idJson) {
		json
	}
}
`;

/**
 * @param {IsxFragment} fragment
 */
export async function read(scope, fragment) {
	const idJson = toQueryVariableId(fragment);
	console.log(`manual fetch ${idJson}`);
	const { data } = await apolloClient.query({ query: GET_FRAGMENT, variables: { idJson } });
	const json = data?.fragmentContent?.json;
	return deserializeFragmentContent(scope, json);
}
