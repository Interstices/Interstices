import CustomApolloProvider, { apolloClient } from './components/CustomApolloProvider';
import { read } from './read';
import { create, update } from './write';
import installClientPubSubPlatform from './client-pub-sub';

import installScopedAPIPlatform from '@interstices/scoped-api-platform';
import installCoreUtilsPolyfill from '@interstices/core-utils-polyfill';
import installFragmentInterface from '@interstices/fragment';
import { installApolloClientQueryUnsafePlugin } from './query';
import installFragmentFormatValidation from '@interstices/fragment-format-validation';
import installValidatedQueryPlugin from '@interstices/query-format-validation';
import installMappingPlugin from '@interstices/fast-mapping';
import { serializeFragmentContent, toQueryVariableId } from './utils/serialize';

/**
 * Apollo Client Core is actually a collection of plugins that are all installed at once to make it easier
 * for developers (so they don't have to install them one by one).
 * These plugins include:
 * 	API Platform
 *    Pub Sub Platform
 *    Apollo Client Write
 *    Apollo Client Read
 *    Apollo Client Resolver
 *    Core Pub Sub
 * 	Query
 */
export default function installApolloClientCore(scope) {
	return scope
		.extend({
			NAMESPACE_ID: 'ApolloClient', // TODO: remove hard-coded namespace id
		})
		.apply(installScopedAPIPlatform)
		.apply(installFragmentInterface)
		.extendAPI({
			// These should be defined in their own plugins like WritePlugin and ReadPlugin. See mongodb-core/index.js for an example.
			create,
			update,
			read,

			// This should belong to a plugin, not ApolloClientCore (which is just supposed to be a group of plugins)
			// Maybe @interstices/fragment should be a plugin, and then we can put it in there?
			areFragmentsEqual: (fragment1, fragment2) => fragment1.equals(fragment2),
		})
		.apply(installClientPubSubPlatform)
		.apply(installApolloClientQueryUnsafePlugin);
}

/**
 * Adds format validation, mapping, and core utils polyfills
 *
 * TODO: The scope passed in should already have apollo-client-core installed, instead of installing it inside here.
 *       This way, the caller can customize the apollo-client-core however they like before passing it in.
 */
export function installApolloClientCoreExtended(scope) {
	return scope
		.apply(installApolloClientCore)
		.apply(installFragmentFormatValidation) // TODO: can we use bypassWriteChecks or bypassReadChecks in this case?
		.apply(installValidatedQueryPlugin)
		.apply(installMappingPlugin)
		.apply(installCoreUtilsPolyfill);
}

export {
	// TODO: we should separate this out from apollo-client-core, since CustomApolloProvider and apolloClient don't use scope,
	//       it's really just a utility component un-related to Interstices.
	//       Though I guess the only time you'd need CustomApolloProvider or apolloClient is if you were using apollo-client-core?
	//       What if somebody wanted to create an OptimizedWritePlugin? Then they would need to import this apollo-client-core package
	//       to access apolloClient (since if you import read() from apollo-client-core and create() and update() from optimized-apollo-client,
	//       you don't want duplicate apolloClients so you have to make sure to share the same instance).
	//       And now the optimized apollo write plugin is importing all this extra bloat.
	//       So maybe we should split apollo-client-core into smaller packages, like apollo-client-write, apollo-client-read,
	//       each of which depend on apollo-client-base-lib (which provides CustomApolloProvider and apolloClient)
	CustomApolloProvider,
	apolloClient,
	toQueryVariableId,
	serializeFragmentContent,
};
