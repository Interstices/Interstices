import { apolloClient, apolloCache } from './components/CustomApolloProvider.jsx';
import { gql } from '@apollo/client';
import { GET_FRAGMENT } from './read';
import { deserializeFragmentContent } from './utils/deserialize';
import { toQueryVariableId } from './utils/serialize';
import { FragmentMap } from '@interstices/fragment';

export const FRAGMENT_SUBSCRIPTION = gql`
subscription OnFragmentChanged($idJson: String!) {
	fragmentChanged(idJson: $idJson) {
		json
	}
}
`;

/**
 * In contrast to InMemoryPubSub (see @interstices/pub-sub-platform),
 * this pub-sub platform hooks into events from the server.
 */
class ClientPubSub {
	fragmentListeners = new FragmentMap();

	subscribeToFragment(scope, fragment, listener) {
		const idJson = toQueryVariableId(fragment);

		// https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.subscribe
		// https://github.com/zenparsing/zen-observable
		const observer = apolloClient.subscribe({
			query: FRAGMENT_SUBSCRIPTION,
			variables: { idJson },
		});
		observer.subscribe(async ({ data }) => {
			const json = data?.fragmentChanged?.json;
			const content = deserializeFragmentContent(scope, json);
			// Update cache: https://www.apollographql.com/docs/react/api/cache/InMemoryCache/#writequery
			apolloCache.writeQuery({
				query: GET_FRAGMENT,
				data: {
					fragmentContent: { json },
				},
				variables: { idJson },
			});
			return await listener({ fragment, content });
		});
		return observer.unsubscribe;
	}
}

export default function installClientPubSubPlatform(scope) {
	const pubsub = new ClientPubSub(); // PubSub system should be shared across all scopes
	return scope.extendAPI({
		subscribeToFragment: pubsub.subscribeToFragment.bind(pubsub),
	});
}
