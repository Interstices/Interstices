import { PrefixedId } from '@interstices/identity';

// Works like lodash mapValues
export function mapValues(obj, fn) {
	return Object.fromEntries(Object.entries(obj).map(([key, val]) => [key, fn(val)]));
}

function resolve(scope, value) {
	const { toFragment } = scope.API.getAll();
	const isWrappedIdString = typeof value === 'object' && value !== null; // All objects are assumed to be fragment links
	return isWrappedIdString ? toFragment(PrefixedId.fromIdString(value.id)) : value;
}

/**
 * See apollo-server-lib/utils/serialize.js to see how serialization works.
 *
 * For raw values, removes prefix and parses value. Converts fragment ids into fragments
 * @param {*} json A JSON string produced by serializeFragmentContent()
 * @returns an object
 */
export function deserializeFragmentContent(scope, json) {
	const unresolved = JSON.parse(json);
	return mapValues(unresolved, resolve.bind(null, scope));
}

export function fromResultFragmentId(scope, idJson) {
	const { toFragment } = scope.API.getAll();
	try {
		const idString = JSON.parse(idJson).id;
		if (!idString) {
			throw Error('No `id` property in resolved object.');
		}

		return toFragment(PrefixedId.fromIdString(idString));
	} catch (e) {
		throw Error(`Invalid id passed to fromQueryVariableId(). Error: ${e.message}`);
	}
}
