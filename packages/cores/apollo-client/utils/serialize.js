/**
 * See apollo-server-lib/utils/deserialize.js to see how serialization should work.
 */
function unresolve(scope, value) {
	const { isFragment } = scope.API.getAll();

	if (isFragment(value)) {
		return { idType: 'PrefixedId', id: value.getIdString() }; // Wrap ids in objects to distinguish them from regular string values
	}

	if (typeof value === 'object' && value !== null) {
		// TODO: if we do want to support nested objects, we might also want to detect
		//       circular references so the JSON.stringify replacer doesn't loop infinitely.
		throw Error('Error: nested objects are currently unsupported.');
	}

	return value; // For strings and numbers and booleans, return as-is
}

/**
 * @param {object} fragmentContent the fragment content
 */
export async function serializeFragmentContent(scope, fragmentContent) {
	return JSON.stringify(fragmentContent, (key, val) => key === '' ? val : unresolve(scope, val));
}

/**
 * Converts fragment to an id that we can pass to a graphql query/mutation/subscription.
 * Similar to toDocumentId() in mongodb-core/resolve.js
 */
export function toQueryVariableId(fragment) {
	return JSON.stringify({ idType: 'PrefixedId', id: fragment.getIdString() });
}
