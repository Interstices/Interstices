import { split, HttpLink, ApolloClient, InMemoryCache,
	ApolloProvider } from '@apollo/client';
import { getMainDefinition } from '@apollo/client/utilities';
import { WebSocketLink } from '@apollo/client/link/ws';
import { URL } from '@interstices/apollo-const';

const { WEBSOCKETS_HOSTNAME, GRAPHQL_PATH, SUBSCRIPTIONS_PATH } = URL;
const IS_BROWSER = typeof window !== 'undefined'; // Alternatively we can use process.browser polyfill, see https://stackoverflow.com/a/65018686

// https://www.apollographql.com/docs/react/data/subscriptions/#3-split-communication-by-operation-recommended
// https://github.com/apollographql/subscriptions-transport-ws/issues/333#issuecomment-359261024
const httpLink = new HttpLink({
	uri: GRAPHQL_PATH,
});

const wsLink = IS_BROWSER && new WebSocketLink({
	uri: `ws://${WEBSOCKETS_HOSTNAME}${SUBSCRIPTIONS_PATH}`,
	options: { reconnect: true },
});
const link = IS_BROWSER ? split( // Only create the split in the browser
	({ query }) => {
		const definition = getMainDefinition(query);
		return (definition.kind === 'OperationDefinition' && definition.operation === 'subscription');
	},
	wsLink,
	httpLink,
) : httpLink;

export const apolloCache = new InMemoryCache();

const client = new ApolloClient({
	link,
	cache: apolloCache,
});

export const apolloClient = client; // For executing manual queries and mutations

const CustomApolloProvider = ({ children }) => (
	<ApolloProvider client={client}>
		{children}
	</ApolloProvider>
);

export default CustomApolloProvider;
