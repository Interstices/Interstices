import { mapValues } from './utils.js';

const MAX_INSERTIONS = 1000; // A safeguard in case the system has an infinite loop. TODO: remove this
let totalInsertions = 0;

class WritePlugin {
	/**
	 * @param {*} newValues (optional) initial content for the fragment
	 * @returns {IsxFragment} the new fragment
	 */
	async create(scope, newValues = {}) {
		const { getFragmentsDB, unresolve, fromDocumentId, addToRootSet } = scope.API.getAll();

		// $loki is not allowed as a property name. However we do allow `meta` (even though it is
		// also a reserved property inside loki documents).
		if (newValues.$loki) {
			throw Error('$loki is a reserved property, writes to it are not allowed');
		}

		newValues = mapValues(newValues, unresolve);

		if (totalInsertions++ > MAX_INSERTIONS) {
			throw Error(`Reached ${MAX_INSERTIONS} insertions! Is there an infinite loop?`);
		}

		const fragmentsCollection = await getFragmentsDB();

		// Currently all fragment content is stored inside document.content property, so that
		// we don't have to worry about collision with Loki's `meta` property.
		const documentId = fragmentsCollection.insert({ content: newValues }).$loki;
		console.log(`created: ${JSON.stringify({ $loki: documentId, newValues })}`);

		const fragment = fromDocumentId(documentId);
		await addToRootSet(fragment);

		return fragment;
	}

	/**
	 * Updates the content of a fragment.
	 *
	 * @param {IsxFragment} fragment an existing fragment (optional)
	 * @param {*} newValues new properties that will override existing properties of the fragment. Other existing values will be preserved.
	 * @returns {IsxFragment} the new fragment
	 */
	async update(scope, fragment, newValues) {
		const { getFragmentsDB, unresolve, toDocumentId, read } = scope.API.getAll();

		if (!fragment) {
			throw Error('No fragment provided to update()');
		}

		// $loki is not allowed as a property name. However we do allow `meta` (even though it is
		// also a reserved property inside loki documents).
		if (newValues.$loki) {
			throw Error('$loki is a reserved property, writes to it are not allowed');
		}

		if (Object.keys(newValues).length === 0) {
			return; // No update needed
		}

		newValues = mapValues(newValues, unresolve);
		const documentId = toDocumentId(fragment);

		if (totalInsertions++ > MAX_INSERTIONS) {
			throw Error(`Reached ${MAX_INSERTIONS} insertions! Is there an infinite loop?`);
		}

		const fragmentsCollection = await getFragmentsDB();

		// Normally when updating a document in MongoDB, you only need to call findAndUpdate with
		// the properties you want to update, and existing properties will be preserved.
		//     console.log(someDocument) ==> { hello: "world" }
		//     findAndUpdate(someDocument, { goodbye: "planet" })
		//     console.log(someDocument) ==> { hello: "world", goodbye: "planet" }
		// However, in loki, because "meta" is a reserved property, we put all our fragment content
		// inside a "content" property to prevent collisions. But this also means that in order
		// to do an update, we can't just pass the new content into findAndUpdate, because all old
		// values will be clobbered.
		//     console.log(someDocument) ==> { content: { hello: "world" } }
		//     findAndUpdate(someDocument, { content: { goodbye: "planet" } })
		//     console.log(someDocument) ==> { content: { hello: "world" } }
		// Thus, we are forced to do an extra db read to get the previous content, manually combine
		// the old and new content, and then call findAndUpdate using the combined content.
		// TODO: we can save a read() operation if we instead prefix all property names so they can
		//       never collide with the `meta` property.
		const oldContent = mapValues(await read(fragment), unresolve);
		const newContent = { ...oldContent, ...newValues };

		fragmentsCollection.findAndUpdate(
			{ $loki: documentId },
			document => { document.content = newContent; },
		);

		console.log(`updated: ${JSON.stringify({ $loki: documentId, newValues })}`);
	}

	/**
	 * Overwrites all content, doesn't preserve properties like update().
	 *
	 * While normally the overwrite() function is provided by Core Utils Polyfill plugin,
	 * here we can save a read() operation if we define it ourselves.
	 *
	 * @param {IsxFragment} fragment an existing fragment (optional)
	 * @param {*} content new content to replace old content. All existing values will be deleted.
	 * @returns {IsxFragment} the new fragment
	 */
	async overwrite(scope, fragment, content) {
		const { getFragmentsDB, unresolve, toDocumentId } = scope.API.getAll();

		if (!fragment) {
			throw Error('No fragment provided to overwrite()');
		}

		// $loki is not allowed as a property name. However we do allow `meta` (even though it is
		// also a reserved property inside loki documents).
		if (content.$loki) {
			throw Error('$loki is a reserved property, writes to it are not allowed');
		}

		content = mapValues(content, unresolve);
		const documentId = fragment && toDocumentId(fragment);

		console.log(`mutation: ${JSON.stringify({ $loki: documentId, content })}`);
		if (totalInsertions++ > MAX_INSERTIONS) {
			throw Error(`Reached ${MAX_INSERTIONS} insertions! Is there an infinite loop?`);
		}

		const fragmentsCollection = await getFragmentsDB();

		fragmentsCollection.findAndUpdate(
			{ $loki: documentId },
			document => { document.content = content; },
		);
	}
}

export default function installWritePlugin(scope) {
	const writePlugin = new WritePlugin();
	return scope.extendAPI({
		create: writePlugin.create,
		update: writePlugin.update,
		overwrite: writePlugin.overwrite,
	});
}
