import installLokiDBClient from './db.js';
import installWritePlugin from './write.js';
import installReadPlugin from './read.js';
import installSetEmulationPlugin from './set-emulation.js';

import installScopedAPIPlatform from '@interstices/scoped-api-platform';
import installPubSubPlatform from '@interstices/pub-sub-platform';
import installCorePubSubPlugin from '@interstices/core-pub-sub';

import { installLokiJSQueryUnsafePlugin } from './query.js';
import installMappingPlugin from '@interstices/fast-mapping';
import installLokiJSResolverPlugin from './resolve.js';
import installCoreUtilsPolyfill from '@interstices/core-utils-polyfill';
import installFragmentInterface from '@interstices/fragment';
import installFragmentFormatValidation from '@interstices/fragment-format-validation';
import installValidatedQueryPlugin from '@interstices/query-format-validation';

/**
 * LokiJS Core is actually a collection of plugins that are all installed at once to make it easier
 * for developers (so they don't have to install them one by one).
 * These plugins include:
 * 	API Platform
 *    Pub Sub Platform
 *    LokiJS Resolver
 *    LokiJS Write
 *    LokiJS Read
 * 	LokiJS Identity
 *    Core Pub Sub
 */
export default function installLokiJSCore(scope) {
	return scope
		.extend({
			// We add a suffix so every client has a different namespace.
			// TODO: use a public key instead of a timestamp.
			NAMESPACE_ID: 'LokiDB_' + Date.now(), // TODO: retrieve existing namespace id from database if it exists, or generate a new one.
		})
		.apply(installScopedAPIPlatform)
		.apply(installLokiDBClient)
		.apply(installFragmentInterface)
		.apply(installPubSubPlatform)
		.apply(installLokiJSResolverPlugin)
		.apply(installWritePlugin)
		.apply(installReadPlugin)
		.apply(installSetEmulationPlugin)
		.extendAPI({
			// This should belong to a plugin, not LokiJSCore (which is just supposed to be a group of plugins)
			// Maybe Core Utils Polyfill?
			areFragmentsEqual: (fragment1, fragment2) => fragment1.equals(fragment2),
		})
		.apply(installCorePubSubPlugin)
		.apply(installLokiJSQueryUnsafePlugin);
}

/**
 * Adds format validation, mapping, and core utils polyfills
 *
 * TODO: The scope passed in should already have lokijs-core installed, instead of installing it inside here.
 *       This way, the caller can customize the lokijs-core however they like before passing it in.
 */
export function installLokiJSCoreExtended(scope) {
	return scope
		.apply(installLokiJSCore)
		.apply(installFragmentFormatValidation) // TODO: can we bypass read checks if we are checking all writes? What if somebody else is writing to the same indexeddb?
		.apply(installValidatedQueryPlugin)
		.apply(installMappingPlugin)
		.apply(installCoreUtilsPolyfill);
}
