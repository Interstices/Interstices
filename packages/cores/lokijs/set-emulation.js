import { NEXT_IN_ROOT_SET_KEY, HEAD_OF_ROOT_SET_KEY } from './utils.js';
import { SetFormat, SetItemFormat } from '@interstices/base-formats';

const ROOT_SET_CONTAINER_ID_TAIL = 'root';
const ITEM_ID_POSTFIX = '_item';

// See description of `NEXT_IN_ROOT_SET_KEY` for what the dummy 'END' value is for
const END_PTR = 'END';

/**
 * TODO: use MongoDB transactions instead of this home-grown synchronization system.
 */
class JobQueue {
	running = false;
	queue = [];

	addJob(job) {
		return new Promise((resolve, reject) => {
			// Notice how the block below is all synchronous. This is to prevent race conditions.
			if (this.running) {
				this.queue.push({ job, resolve, reject });
			} else {
				this.running = true;
				this.queue.push({ job, resolve, reject });
				this.runJobs();
			}
		});
	}

	async runJobs() {
		// Notice that if queue.length reaches 0, this.queues.delete(fragment) will be called
		// synchronously right afterwards to prevent race conditions
		while (this.queue.length > 0) {
			const { job, resolve, reject } = this.queue.shift();
			try {
				// eslint-disable-next-line no-await-in-loop
				resolve(await job());
			} catch (e) {
				reject(e);
			}
		}

		this.running = false;
	}
}

const jobQueue = new JobQueue();

/**
 * Serializes all calls to an asynchronous function.
 * CAUTION: if a synchronized function ever calls itself, it will deadlock!
 * Even if the call has different arguments, or is indirect recursion.
 * Consider the following:
 * factorial = (x) => x * g(x-1)
 * g = (x) => x === 0 ? 1 : factorial(x)
 * Trying to synchronize factorial() will cause a deadlock.
 * This is why it is best to use synchronize() within plugins that use Scoped API Platform,
 * since Scoped API Platform makes it so API calls will travel to outer scopes, and reduces
 * the chance that a function will call itself.
 */
function synchronize(asyncFn) {
	return async (...args) => await jobQueue.addJob(() => asyncFn(...args));
}

// TODO: look into https://github.com/ReactiveX/IxJS as an alternative
async function asyncIteratorToArray(iterable) {
	const array = [];
	for await (const item of iterable) {
		array.push(item);
	}

	return array;
}

function isVirtualFragment(scope, fragment) {
	if (fragment.getIdObject().prefix === scope.NAMESPACE_ID) {
		if (fragment.getIdObject().tail === ROOT_SET_CONTAINER_ID_TAIL) {
			return true;
		}

		if (fragment.getIdObject().tail.endsWith(ITEM_ID_POSTFIX)) {
			return true;
		}
	}

	return false;
}

const addToRootSet = synchronize(async (scope, fragment) => {
	if (isVirtualFragment(scope, fragment)) {
		throw Error('Cannot add virtual item fragments into their own database');
	}

	const { getFragmentsDB, toDocumentId } = scope.API.getAll();

	const fragmentsDB = await getFragmentsDB();
	const documentId = toDocumentId(fragment);

	// Get old head while rewriting head to point to `fragment`
	const oldHeadDoc = fragmentsDB.findOne({ [HEAD_OF_ROOT_SET_KEY]: { $exists: true } });
	const oldHeadId = oldHeadDoc?.[HEAD_OF_ROOT_SET_KEY] || END_PTR;
	if (oldHeadDoc) {
		oldHeadDoc[HEAD_OF_ROOT_SET_KEY] = documentId;
		fragmentsDB.update(oldHeadDoc);
	} else {
		fragmentsDB.insert({ [HEAD_OF_ROOT_SET_KEY]: documentId });
	}

	fragmentsDB.findAndUpdate(
		{ $loki: documentId },
		document => { document[NEXT_IN_ROOT_SET_KEY] = oldHeadId; },
	);
});

function _createItemFromValueId(scope, valueId) {
	const { fromDocumentId } = scope.API.getAll();
	return fromDocumentId(valueId + ITEM_ID_POSTFIX);
}

async function _getHeadValueDocId(scope) {
	const { getFragmentsDB } = scope.API.getAll();
	const fragmentsDB = await getFragmentsDB();
	const headValueDoc = fragmentsDB.findOne({ [HEAD_OF_ROOT_SET_KEY]: { $exists: true } });
	return headValueDoc?.[HEAD_OF_ROOT_SET_KEY];
}

const removeFromRootSet = synchronize(async (scope, fragment) => {
	const { getFragmentsDB, toDocumentId } = scope.API.getAll();
	const documentId = toDocumentId(fragment);

	const fragmentsDB = await getFragmentsDB();

	const { [NEXT_IN_ROOT_SET_KEY]: nextValueId } = fragmentsDB.findOne({ $loki: documentId });
	const prevItemQuery = { [NEXT_IN_ROOT_SET_KEY]: documentId };
	fragmentsDB.findAndUpdate(
		prevItemQuery,
		document => { document[NEXT_IN_ROOT_SET_KEY] = nextValueId; },
	);
	fragmentsDB.findAndUpdate(
		{ $loki: documentId },
		document => { delete document[NEXT_IN_ROOT_SET_KEY]; },
	);
});

function getSetContainer(scope) {
	const { fromDocumentId } = scope.API.getAll();
	return fromDocumentId(ROOT_SET_CONTAINER_ID_TAIL);
}

async function getHead(scope) {
	const headValueId = await _getHeadValueDocId(scope);
	return headValueId ? _createItemFromValueId(scope, headValueId) : null;
}

async function getRootSetContainerContent(scope) {
	const head = await getHead(scope);
	return { type: SetFormat.type, head };
}

async function getItemContent(scope, item) {
	const { getFragmentsDB, toDocumentId, fromDocumentId } = scope.API.getAll();
	const itemDocIdString = item.getIdObject().tail.toString();

	if (!itemDocIdString.endsWith(ITEM_ID_POSTFIX)) {
		throw Error('invalid root collection item');
	}

	const valueDocId = itemDocIdString.slice(0, -ITEM_ID_POSTFIX.length);
	const value = fromDocumentId(valueDocId);

	const fragmentsDB = await getFragmentsDB();
	const document = fragmentsDB.findOne({ $loki: toDocumentId(value) });
	const valueId = document?.[NEXT_IN_ROOT_SET_KEY];
	const next = valueId && valueId !== END_PTR
		? _createItemFromValueId(scope, valueId)
		: null;

	return { type: SetItemFormat.type, next, value };
}

async function * values(scope) {
	const { getFragmentsDB, fromDocumentId } = scope.API.getAll();

	const fragmentsDB = await getFragmentsDB();
	let currValueId = await _getHeadValueDocId(scope);
	while (currValueId && currValueId !== END_PTR) {
		yield fromDocumentId(currValueId);
		const document = fragmentsDB.findOne({ $loki: currValueId });
		currValueId = document?.[NEXT_IN_ROOT_SET_KEY];
	}
}

async function valueArray(scope) {
	return asyncIteratorToArray(values(scope));
}

// TODO: Figure out how to handle systems trying to directly modify the root set without using addToRootSet()
//       and removeFromRootSet(). Simplest solution might be to hook into update() and throw an error if fragment
//       belongs to root set.
export default function installSetEmulationPlugin(scope) {
	return scope
		.extendAPI({
			getRootSetValues: values,
			getRootSetValueArray: valueArray,
			getRootSetContainer: getSetContainer,
			addToRootSet,
			removeFromRootSet,
		})
		.replaceAPI('read', oldReadFn => (
			/**
			 * ExtraArgs: in case anybody else wrapped update() and added extra arguments before CorePubSub could wrap update(),
			 *            we need to forward those extra arguments.
			 */
			async (scope, fragment, ...extraArgs) => {
				if (fragment.getIdObject().prefix === scope.NAMESPACE_ID) {
					if (fragment.getIdObject().tail === ROOT_SET_CONTAINER_ID_TAIL) {
						return await getRootSetContainerContent(scope);
					}

					if (fragment.getIdObject().tail.endsWith(ITEM_ID_POSTFIX)) {
						return await getItemContent(scope, fragment);
					}
				}

				return await oldReadFn(scope, fragment, ...extraArgs);
			}
		))
		.replaceAPI('readProperty', oldReadPropertyFn => (
			/**
			 * ExtraArgs: in case anybody else wrapped update() and added extra arguments before CorePubSub could wrap update(),
			 *            we need to forward those extra arguments.
			 */
			async (scope, fragment, propertyName, ...extraArgs) => {
				if (fragment.getIdObject().prefix === scope.NAMESPACE_ID) {
					if (fragment.getIdObject().tail === ROOT_SET_CONTAINER_ID_TAIL) {
						return (await getRootSetContainerContent(scope))[propertyName];
					}

					if (fragment.getIdObject().tail.endsWith(ITEM_ID_POSTFIX)) {
						return (await getItemContent(scope, fragment))[propertyName];
					}
				}

				return await oldReadPropertyFn(scope, fragment, propertyName, ...extraArgs);
			}
		));
}
