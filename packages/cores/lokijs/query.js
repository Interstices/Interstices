import { mapValues, NEXT_IN_ROOT_SET_KEY } from './utils.js';

/**
 * Helper to build queries that match against fragment contents.
 * Only works for top-level fields. Eg if you had a fragment like:
 *
 * `{ age: 20, ptr: fromDocumentId(111), name: { first: 'Bob' } }`
 *
 * then the corresponding Loki document would look like
 *
 * `{ $loki: 1, meta: {...}, content: { age: 20, ptr: { $loki: 111 }, name: { first: 'Bob' } } }`
 *
 * and you can build a query like `buildQuery({ age: 20, ptr: fromDocumentId(111) })`,
 * but we don't support something like `buildQuery({ age: 20, name: { first: 'Bob' } })`.
 * This should be fine since fragment content should be a flat structure anyways.
 *
 * We also check that all fields match, we don't support ANY or OR style queries.
 *
 * @param {*} fields a list of fields to match
 * @returns a query to give to LokiJS
 */
export function buildQuery(scope, fields) {
	const { isFragment } = scope.API.getAll();

	const query = {};
	for (const [key, value] of Object.entries(fields)) {
		if (isFragment(value)) {
			// Nested object equality like `'content.subjectFragment': { $eq: { $loki: 10 } }` does NOT seem to work.
			// So for now we use dot notation instead.
			query[`content.${key}.prefix`] = value.getIdObject().prefix;
			query[`content.${key}.tail`] = value.getIdObject().tail;
		} else {
			query[`content.${key}`] = value;
		}
	}

	return query;
}

/**
 * TODO: MongoDB query function and LokiJS query function should use the same parameters and parameter formats.
 *       For example, we should define a common format for the `sort` argument.
 *
 * @param {*} scope
 * @param {object} query supports fragments directly inside the query, eg `{ type: ListItemFormat.type, next: someFragment }`
 * @param {Array} sort a two-item array of [ propertyName, 'descending' | 'ascending ]
 * @param {number} limit default 100
 * @returns an async generator for all the results, each result is an object containing { fragment, content }
 */
async function * queryRootSetUnsafe(scope, query, sort, limit = 100) {
	const { getFragmentsDB, resolve, fromDocumentId } = scope.API.getAll();

	if (sort && sort?.[1] !== 'ascending' && sort?.[1] !== 'descending') {
		throw Error('LokiJS core\'s queryRootSet() sort parameter requires either \'descending\' or \'ascending\' to be specified.');
	}

	const fragmentsCollection = await getFragmentsDB();
	const existsInRootSet = { [NEXT_IN_ROOT_SET_KEY]: { $exists: true } };
	const findResults = fragmentsCollection.chain().find({
		...buildQuery(scope, query), // `buildQuery()` handles unresolving any fragments into ids
		...existsInRootSet,
	});

	// See https://github.com/techfort/LokiJS/blob/v1.5.12/src/lokijs.js#L3184
	// TODO: support lokijs's compoundsort().
	const sorted = sort ? findResults.simplesort(`content.${sort[0]}`, { desc: sort[1] === 'descending' }) : findResults;
	const limited = sorted.limit(limit);
	const results = limited.data();

	for (const document of results) {
		const unresolvedContent = document.content;
		yield { fragment: fromDocumentId(document.$loki), content: mapValues(unresolvedContent, resolve) };
	}
}

export function installLokiJSQueryUnsafePlugin(scope) {
	return scope.extendAPI({
		queryRootSet: queryRootSetUnsafe,
	});
}
