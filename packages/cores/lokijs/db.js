/**
 * Currently we use LokiJS because it seems to be very popular (200K+ downloads per week),
 * is simple and works in NodeJS and browser (since it's in-memory), and it supports IndexedDB
 * using a persistence adapter.
 *
 * A few issues I've run into after using it for a while:
 * 1. documentation is lacking, basically makes you sift through code samples.
 * 2. maintained by one person. Tons of issues get automatically marked as "wontfix"
 *    and "Closed" because they go stale.
 * 3. "meta" is a reserved document field, which seems a bit restrictive.
 *    This forces me to put all fragment content inside a nested property to prevent collisions,
 *    which then requires me to make an extra db call on every write (see update() inside write.js).
 * 4. the way "update" and "findAndUpdate" works is a bit clunky. You have to first retrieve the
 *    document, mutate it, and then pass it to update(). You can't construct a new object from
 *    the document with the changes you want, eg `{...document, foo: 'bar'}` and then pass that
 *    to update(), it doesn't work for some reason.
 * 5. Nested object queries are buggy. See buildQuery() in utils.js
 *    See https://github.com/techfort/LokiJS/issues/284 and https://github.com/techfort/LokiJS/issues/816.
 * 6. Doesn't support foreign keys and uses regular integers as ids (unlike MongoDB which has
 *    a special datatype ObjectId), so if I want to embed a reference to another document,
 *    I have to manually wrap the id in a custom wrapper to distinguish it from a regular integer.
 *    See https://github.com/techfort/LokiJS/issues/9#issuecomment-59107419.
 *
 * Some alternatives to consider: PouchDB or LocalForage.
 */

const FRAGMENTS_DB = 'fragments';

async function start(state) {
	let LokiDB;

	if (typeof window !== 'undefined' && window.loki) {
		LokiDB = window.loki;
	} else {
		LokiDB = (await import('lokijs')).default;
	}

	const db = new LokiDB('IntersticesFS');
	state.fragmentsCollection = db.addCollection(FRAGMENTS_DB);
	console.log('Loki in-memory database created');
}

async function getFragmentsDB(state) {
	if (!state.fragmentsCollection) {
		await start(state);
	}

	return state.fragmentsCollection;
}

export default function installLokiDBClient(scope) {
	const state = {
		fragmentsCollection: null,
	};

	return scope.extendAPI({
		getFragmentsDB: () => getFragmentsDB(state),
	});
}
