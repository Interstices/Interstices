import { mapValues } from './utils.js';

/**
 * @param {IsxFragment} fragment
 * @returns all unresolved properties inside fragment
 */
async function getUnresolvedContent(scope, fragment) {
	const { getFragmentsDB, toDocumentId } = scope.API.getAll();
	const $loki = toDocumentId(fragment);
	const fragmentsCollection = await getFragmentsDB();

	const document = fragmentsCollection.findOne({ $loki });
	if (!document) {
		throw Error(`No fragment found with id ${$loki}`);
	}

	return document.content;
}

class ReadPlugin {
	/**
	 * If we only need a single property, this is faster than using read()
	 * since we are only calling resolve() on the property that is needed.
	 *
	 * @param {IsxFragment} fragment
	 * @param {string} propertyName
	 * @returns the property value, as a raw primitive or a IsxFragment
	 */
	async readProperty(scope, fragment, propertyName) {
		const { resolve } = scope.API.getAll();
		const value = (await getUnresolvedContent(scope, fragment))?.[propertyName];

		return resolve(value);
	}

	/**
	 * @param {IsxFragment} fragment
	 * @returns an object containing all property values
	 */
	async read(scope, fragment) {
		const { resolve } = scope.API.getAll();
		const values = await getUnresolvedContent(scope, fragment);

		// If any values are fragment ids, replace them with fragment objects
		return mapValues(values, resolve);
	}
}

export default function installReadPlugin(scope) {
	const readPlugin = new ReadPlugin();
	return scope.extendAPI({
		read: readPlugin.read,
		readProperty: readPlugin.readProperty,
	});
}
