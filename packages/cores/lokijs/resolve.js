import { PrefixedId } from '@interstices/identity';

/**
 * TODO: add description, discuss how resolve() and unresolve() are local to the data store scope
 *
 * Currently, these resolvers only support PrefixedIds and not any other id types.
 */

/**
 * @param {*} value a primitive value or a IsxFragment
 * @returns {*} a property value to give to create() or update()
 */
function unresolve(scope, value) {
	const { isFragment } = scope.API.getAll();
	// TODO: sanitize values
	return isFragment(value) ? value.getIdObject().asObject() : value;
}

/**
 *
 * @param {*} value a property value returned from read()
 * @returns {*} a primitive value or a IsxFragment
 */
function resolve(scope, value) {
	const { toFragment } = scope.API.getAll();
	const isIdObject = typeof value === 'object' && value !== null;
	return isIdObject ? toFragment(PrefixedId.fromObject(value)) : value;
}

function toDocumentId(scope, fragment) {
	// TODO: sanitize values
	// TODO: check namespace to make sure it matches lokiDB namespace?
	return Number(fragment.getIdObject().tail);
}

function fromDocumentId(scope, documentId) {
	const { toFragment } = scope.API.getAll();
	// TODO: sanitize values
	const { NAMESPACE_ID } = scope;
	return toFragment(new PrefixedId(NAMESPACE_ID, String(documentId)));
}

export default function installLokiJSResolverPlugin(scope) {
	return scope.extendAPI({
		unresolve,
		resolve,
		toDocumentId,
		fromDocumentId,
	});
}
