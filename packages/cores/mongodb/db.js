import mongo from 'mongodb';
const { MongoClient } = mongo;
const url = 'mongodb://127.0.0.1:27017';

const FRAGMENTS_DB = 'fragments';

async function start(state) {
	state.client = await MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });

	state.db = state.client.db('IntersticesFS');
	state.fragmentsCollection = state.db.collection(state.dbName);

	console.log(`MongoDB Connected: ${url}`);
}

async function getFragmentsDB(state) {
	// The following block is synchonous to prevent race conditions.
	if (!state.startedPromise) {
		state.startedPromise = start(state);
	}

	await state.startedPromise;

	return state.fragmentsCollection;
}

async function close(state) {
	console.log('closing database');
	// The following block is synchonous to prevent race conditions.
	if (state.startedPromise) {
		state.client.close();
		state.startedPromise = null;
	}
}

async function getDatabase(state) {	// The following block is synchonous to prevent race conditions.
	if (!state.startedPromise) {
		state.startedPromise = start(state);
	}

	await state.startedPromise;

	return state.db;
}

export default function installMongoDBClient(scope, { dbName = FRAGMENTS_DB } = {}) {
	const state = {
		dbName,
		startedPromise: null,
		client: null,
		db: null,
		fragmentsCollection: null,
	};

	return scope.extendAPI({
		getFragmentsDB: () => getFragmentsDB(state),
		closeMongoDB: () => close(state), // Special fn exposed by MongoDBCore, not actually a part of core plugin interface
		getDatabase: () => getDatabase(state),
	});
}

export { start, getFragmentsDB, getDatabase, close };
