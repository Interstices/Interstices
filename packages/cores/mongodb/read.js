import { mapValues, getContentFromDocument } from './utils.js';

async function _getUnresolvedContent(scope, fragment) {
	const { getFragmentsDB, toDocumentId } = scope.API.getAll();
	const _id = toDocumentId(fragment);
	const fragmentsCollection = await getFragmentsDB();

	const document = await fragmentsCollection.findOne({ _id });
	if (!document) {
		throw Error(`No document found with id ${_id}`);
	}

	return getContentFromDocument(document);
}

class ReadPlugin {
	/**
	 * If we only need a single property, this is faster than using read()
	 * since we are only calling resolve() on the property that is needed.
	 *
	 * @param {IsxFragment} fragment
	 * @param {string} propertyName
	 * @returns the property value, as a raw primitive or a IsxFragment
	 */
	async readProperty(scope, fragment, propertyName) {
		const { resolve } = scope.API.getAll();
		const value = (await _getUnresolvedContent(scope, fragment))?.[propertyName];

		return resolve(value);
	}

	/**
	 * @param {IsxFragment} fragment
	 * @returns an object containing all property values
	 */
	async read(scope, fragment) {
		const { resolve } = scope.API.getAll();
		const values = await _getUnresolvedContent(scope, fragment);

		// If any values are fragment ids, replace them with fragment objects
		return mapValues(values, resolve);
	}
}

export default function installReadPlugin(scope) {
	const readPlugin = new ReadPlugin();
	return scope.extendAPI({
		read: readPlugin.read,
		readProperty: readPlugin.readProperty,
	});
}
