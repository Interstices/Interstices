import { PrefixedId } from '@interstices/identity';
import mongo from 'mongodb';
const { ObjectId } = mongo;

/**
 * TODO: add description, discuss how resolve() and unresolve() are local to the data store scope
 *
 * Currently, these resolvers only support PrefixedIds and not any other id types.
 */

/**
 * TODO: sanitize data before putting it into mongodb
 *
 * @param {*} value a primitive value or an IsxFragment
 * @returns {*} a property value to give to create() or update()
 */
function unresolve(scope, value) {
	const { isFragment } = scope.API.getAll();
	return isFragment(value) ? value.getIdObject().asObject() : value;
}

/**
 *
 * @param {*} value a property value returned from read()
 * @returns {*} a primitive value or a IsxFragment
 */
function resolve(scope, value) {
	const { toFragment } = scope.API.getAll();
	const isIdObject = typeof value === 'object' && value !== null;
	return isIdObject ? toFragment(PrefixedId.fromObject(value)) : value;
}

/**
 * @param {IsxFragment} fragment a fragment
 * @returns {ObjectId} a mongodb document id
 */
function toDocumentId(scope, fragment) {
	// eslint-disable-next-line new-cap
	return ObjectId(fragment.getIdObject().tail);
}

/**
 * @param {ObjectId} documentId a mongodb document id
 * @returns {IsxFragment} a fragment
 */
function fromDocumentId(scope, documentId) {
	const { toFragment } = scope.API.getAll();
	const { NAMESPACE_ID } = scope;
	return toFragment(new PrefixedId(NAMESPACE_ID, documentId.toString()));
}

export default function installMongoDBResolverPlugin(scope) {
	return scope.extendAPI({
		unresolve,
		resolve,
		toDocumentId,
		fromDocumentId,
	});
}
