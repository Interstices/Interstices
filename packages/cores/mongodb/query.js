import { mapValues, getContentFromDocument, NEXT_IN_ROOT_SET_KEY } from './utils.js';

/**
 * TODO: MongoDB query function and LokiJS query function should use the same parameters and parameter formats.
 *       For example, we should define a common format for the `sort` argument.
 *
 * @param {*} scope
 * @param {object} query supports fragments directly inside the query, eg `{ type: ListItemFormat.type, next: someFragment }`
 * @param {Array} sort see https://mongodb.github.io/node-mongodb-native/4.3/interfaces/FindOptions.html#sort
 * @param {number} limit default 100, see https://mongodb.github.io/node-mongodb-native/4.3/interfaces/FindOptions.html#limit
 * @returns an async generator for all the results, each result is an object containing { fragment, content }
 */
async function * queryRootSetUnsafe(scope, query, sort, limit = 100) {
	const { getFragmentsDB, unresolve, resolve, fromDocumentId } = scope.API.getAll();

	const unresolvedQuery = mapValues(query, unresolve); // Convert any fragments into ids
	const existsInRootSet = { [NEXT_IN_ROOT_SET_KEY]: { $exists: true } };
	const fragmentsCollection = await getFragmentsDB();
	const cursor = await fragmentsCollection.find(
		{ ...unresolvedQuery, ...existsInRootSet },
		{ sort, limit },
	);
	for await (const document of cursor) {
		// Strip _id and set-emulation fields
		const unresolvedContent = getContentFromDocument(document);
		yield { fragment: fromDocumentId(document._id), content: mapValues(unresolvedContent, resolve) };
	}
}

export function installMongoDBQueryUnsafePlugin(scope) {
	return scope.extendAPI({
		queryRootSet: queryRootSetUnsafe,
	});
}
