import installWritePlugin from './write.js';
import installReadPlugin from './read.js';
import installMongoDBClient from './db.js';
import installSetEmulationPlugin from './set-emulation.js';

import installMongoDBResolverPlugin from './resolve.js';
import installMappingPlugin from '@interstices/fast-mapping';

import installScopedAPIPlatform from '@interstices/scoped-api-platform';
import installPubSubPlatform from '@interstices/pub-sub-platform';
import installCorePubSubPlugin from '@interstices/core-pub-sub';
import { installMongoDBQueryUnsafePlugin } from './query.js';
import installCoreUtilsPolyfill from '@interstices/core-utils-polyfill';
import installFragmentInterface from '@interstices/fragment';
import installFragmentFormatValidation from '@interstices/fragment-format-validation';
import installValidatedQueryPlugin from '@interstices/query-format-validation';
import installValidatedSetEmulationPlugin from '@interstices/set-emulation-format-validation';

/**
 * MongoDB Core is actually a collection of plugins that are all installed at once to make it easier
 * for developers (so they don't have to install them one by one).
 * These plugins include:
 * 	API Platform
 *    Pub Sub Platform
 *    MongoDB Resolver
 *    MongoDB Write
 *    MongoDB Read
 * 	Identity
 *    Core Pub Sub
 *    Query
 *
 * Bypass checks should only be used for certain demos (mongodb-explorer and apollo-server-sandbox).
 */
export function installMongoDBCore(scope, { dbName } = {}) {
	return scope
		.extend({
			NAMESPACE_ID: 'MongoDB', // TODO: retrieve existing namespace id from database if it exists, or generate a new one.
		})
		.apply(installScopedAPIPlatform)
		.apply(installMongoDBClient, { dbName })
		.apply(installFragmentInterface)
		.apply(installPubSubPlatform)
		.apply(installMongoDBResolverPlugin)
		.apply(installWritePlugin)
		.apply(installReadPlugin)
		.apply(installSetEmulationPlugin)
		.extendAPI({
			// This should belong to a plugin, not MongoDBCore (which is just supposed to be a group of plugins)
			// Maybe core utils polyfill plugin?
			areFragmentsEqual: (fragment1, fragment2) => fragment1.equals(fragment2),
		})
		.apply(installCorePubSubPlugin)
		.apply(installMongoDBQueryUnsafePlugin);
}

export function installMongoDBCoreExtensions(scope, { bypassWriteChecks, bypassReadChecks } = {}) {
	return scope
		// TODO: can we bypass read checks if we are checking all writes? What if somebody else is writing to the same database?
		.apply(installFragmentFormatValidation, { bypassWriteChecks, bypassReadChecks })
		.apply(installValidatedQueryPlugin)
		.apply(installValidatedSetEmulationPlugin)
		.apply(installMappingPlugin)
		.apply(installCoreUtilsPolyfill);
}

/**
 * TODO: currently we have both:
 *
 *         installMongoDBCoreExtensions() - expects the scope that is passed in to already have mongodb-core
 *         installMongoDBCoreExtended() - installs mongodb-core itself
 *
 *       We should choose one.
 */
export function installMongoDBCoreExtended(scope, { dbName, bypassWriteChecks, bypassReadChecks } = {}) {
	return scope
		.apply(installMongoDBCore, { dbName })
		.apply(installMongoDBCoreExtensions, { bypassWriteChecks, bypassReadChecks });
}

export default installMongoDBCore;
