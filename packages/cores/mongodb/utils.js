
export function getContentFromDocument(obj) {
	// Remove _id and set-emulation fields
	const { _id, ...rest } = obj;
	delete rest[NEXT_IN_ROOT_SET_KEY];
	delete rest[HEAD_OF_ROOT_SET_KEY];
	return rest;
}

// Works like lodash mapValues
export function mapValues(obj, fn) {
	return Object.fromEntries(Object.entries(obj).map(([key, val]) => [key, fn(val)]));
}

/**
 * Databases in Interstices should emulate Set structures.
 * The Set format defines a linked-list of item nodes to
 * hold all the values in the set.
 * While we don't actually create these item nodes,
 * we need to emulate them to any readers and observers.
 * Thus, every entry in the database has a NEXT_IN_ROOT_SET_KEY
 * field for two purposes:
 *
 *   1. indicate if the database entry is a member of the Set
 *   2. if so, indicate which is the next item
 *
 * While normally, the last item in a singly linked list does
 * not point to anything, since we are also indiciating membership
 * using this field, we use a dummy value of "END".
 */
export const NEXT_IN_ROOT_SET_KEY = '_next_in_root_set';
export const HEAD_OF_ROOT_SET_KEY = '_head_of_root_set';
