import { mapValues, NEXT_IN_ROOT_SET_KEY } from './utils.js';

const MAX_INSERTIONS = 1000; // A safeguard in case the system has an infinite loop
let totalInsertions = 0;

class WritePlugin {
	/**
	 * All newly created fragments are added to the root Set.
	 *
	 * TODO: should we support providing newValues to create()?
	 *       If we do, it makes it easier to initialize a new fragment, but it also adds
	 *       poor separation of responsibility between create() and update().
	 *
	 * @param {*} newValues (optional) initial content for the fragment
	 * @returns {IsxFragment} the new fragment (or existing fragment, if it was provided)
	 */
	async create(scope, newValues = {}) {
		const { getFragmentsDB, unresolve, fromDocumentId, addToRootSet } = scope.API.getAll();

		if (newValues._id) {
			throw Error('_id is a reserved property, writes to it are not allowed');
		}

		if (newValues[NEXT_IN_ROOT_SET_KEY]) {
			throw Error(`${NEXT_IN_ROOT_SET_KEY} is a reserved property, writes to it are not allowed`);
		}

		// If any values are fragments, replace the fragment with the fragment's _id
		newValues = mapValues(newValues, unresolve);

		if (totalInsertions++ > MAX_INSERTIONS) {
			throw Error(`Reached ${MAX_INSERTIONS} insertions! Is there an infinite loop?`);
		}

		const fragmentsCollection = await getFragmentsDB();

		const untouchedNewValues = { ...newValues }; // `insertOne(newValues)` modifies newValues for some reason
		const documentId = (await fragmentsCollection.insertOne(newValues)).ops[0]._id;
		console.log(`created: ${JSON.stringify({ _id: documentId, newValues: untouchedNewValues })}`);

		const fragment = fromDocumentId(documentId);
		await addToRootSet(fragment);

		return fragment;
	}

	/**
	 * @param {IsxFragment} fragment an existing fragment (required)
	 * @param {*} newValues new properties that will override existing properties of the fragment
	 */
	async update(scope, fragment, newValues) {
		const { getFragmentsDB, unresolve, toDocumentId } = scope.API.getAll();

		if (!fragment) {
			throw Error('No fragment provided to update()');
		}

		if (!newValues || Object.keys(newValues).length === 0) {
			return; // No update needed
		}

		if (newValues._id) {
			throw Error('_id is a reserved property, writes to it are not allowed');
		}

		// If any values are fragments, replace the fragment with the fragment's _id
		newValues = mapValues(newValues, unresolve);
		const documentId = toDocumentId(fragment);

		if (totalInsertions++ > MAX_INSERTIONS) {
			throw Error(`Reached ${MAX_INSERTIONS} insertions! Is there an infinite loop?`);
		}

		const fragmentsCollection = await getFragmentsDB();

		// TODO: if documentId was deleted previously, dynamically re-create it and emit a warning that it was re-created.
		await fragmentsCollection.updateOne({ _id: documentId }, { $set: newValues });

		console.log(`updated: ${JSON.stringify({ _id: documentId, newValues })}`);
	}

	// An optimization to reduce database calls
	async updateMultiple() {
		throw Error('unimplemented');
	}
}

export default function installWritePlugin(scope) {
	const writePlugin = new WritePlugin();
	return scope.extendAPI({
		create: writePlugin.create,
		update: writePlugin.update,
	});
}
