import baseScope from '@interstices/environment';
import { installMongoDBCore, installMongoDBCoreExtensions } from '@interstices/mongodb-core';
import installCoreUtilsPolyfill from '@interstices/core-utils-polyfill';
import installBaseFormats from '@interstices/base-formats';
import installBaseListPlugin from '@interstices/base-list-handler';
import installListPlugin from '@interstices/list-handler';
import loadJSONData from '@interstices/json-loader';
import { TodoListFormatGroup } from '@interstices/base-formats';
import mockData from '@interstices/mock-data/todo-list.js';

const MOCK_DATA_ROOT_KEY = 1;

(async function () {
	const scope = baseScope
		.apply(installMongoDBCore)
		.apply(installCoreUtilsPolyfill) // JSON-loader needs overwrite() provided by the unvalidated core API.
		.apply(installMongoDBCoreExtensions)
		.apply(installBaseFormats)
		.apply(installBaseListPlugin)
		.apply(installListPlugin);

	const { read, readProperty, loadList, closeMongoDB } = scope.API.getAll();

	const todoFragment = await loadJSONData(scope, mockData, [TodoListFormatGroup], MOCK_DATA_ROOT_KEY);
	console.log('Loaded Todo list:');
	console.log(''); // Newline for padding.
	console.log(`    ${await todoFragment.$(readProperty, 'title')}`);
	console.log('    -------------');
	const todoList = await todoFragment.$(readProperty, 'items');
	const listHandler = await loadList(todoList, TodoListFormatGroup);

	for await (const itemValue of listHandler.values()) {
		const { completed, content } = await itemValue.$(read);
		console.log(`    ${completed ? '[x]' : '[ ]'} ${content}`);
	}

	console.log(''); // Newline for padding.
	await closeMongoDB();
})();
