import baseScope from '@interstices/environment';
import { Any, required, optional, enumType, METADATA_SUBJECT_KEY } from '@interstices/base-formats';
import { installLokiJSCoreExtended } from '@interstices/lokijs-core';

const NumberFormat = {
	type: 'format-tests_number-format',
};

const SomeFormat = {
	type: 'format-tests_some-format',
	any: Any,
	recursive: null,
	optional: optional(String),
	required: required(String),
	fragment: required(NumberFormat),
	fragmentOptional: optional(NumberFormat),
	enum: enumType(Number, String, NumberFormat), // If required or optional not specified, it is required by default
	enumOptional: optional(enumType(Number, String, Boolean)),
	enumRequired: required(enumType(Number, String, Boolean)),
};
SomeFormat.recursive = optional(SomeFormat);

const CommentFormat = {
	type: 'format-tests_comment-format',
	[METADATA_SUBJECT_KEY]: required(SomeFormat),
	comment: required(String),
};

(async function () {
	const scope = baseScope
		.apply(installLokiJSCoreExtended)
		.addFormats([Any, NumberFormat, SomeFormat, CommentFormat]);

	const { read, create, update, writeMapping, readMapping } = scope.API.getAll();

	// Test fragment creation
	const emptyFragment = await create({ type: 'Any' });
	const num = await create({ type: 'format-tests_number-format' });
	const fragment = await create({
		type: SomeFormat.type,
		any: null, // `Any` format even accepts no value at all.
		recursive: null,
		required: 'required',
		fragment: num,
		enum: 'enum',
		enumRequired: 'enumRequired',
	});
	await writeMapping(CommentFormat, fragment, { comment: 'comment 1' });

	// Test writes
	await update(emptyFragment, { type: 'format-tests_number-format' });
	await update(fragment, { recursive: fragment });

	// Test reads
	console.log(JSON.stringify(await fragment.$(read)));
	console.log(JSON.stringify(await (await readMapping(CommentFormat, fragment)).$(read)));

	// TODO: test for malformed or unusual cases, like `required(Any)` or `enumType(Any, null, String)`
	// TODO: after adding Jest, test for cases where validation should fail.
})();
