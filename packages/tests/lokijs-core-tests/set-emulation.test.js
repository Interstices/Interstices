/**
 * Make sure parallel writes don't cause @interstices/mongodb-core/db.js to open multiple mongodb connections.
 */
import { expect, test, beforeAll } from '@jest/globals';
import baseScope from '@interstices/environment';
import installLokiJSCore from '@interstices/lokijs-core';
import { SetFormat, SetItemFormat } from '@interstices/base-formats';

let scope;

beforeAll(async () => {
	// We generally use a new db for every unit tests, so that
	// (1) test data is kept separate, making it easier to inspect
	// (2) we prevent race conditions when running multiple tests that each
	//     clear the database during setup.
	scope = baseScope
		.apply(installLokiJSCore);
});

test('set emulation', async () => {
	const { create, removeFromRootSet, getRootSetValues, getRootSetContainer,
		isFragment, read, readProperty } = scope.API.getAll();

	let fragment5;

	for (let i = 0; i < 10; i++) {
		// eslint-disable-next-line no-await-in-loop
		const fragment = await create({ 'lokijs set emulation test': i });
		if (i === 5) {
			fragment5 = fragment;
		}
	}

	await removeFromRootSet(fragment5);

	const arr = [];
	for await (const value of getRootSetValues()) {
		arr.push(await value.$(readProperty, 'lokijs set emulation test'));
	}

	expect(arr.length).toBe(9);
	expect(arr.includes(0)).toBe(true);
	expect(arr.includes(1)).toBe(true);
	expect(arr.includes(2)).toBe(true);
	expect(arr.includes(3)).toBe(true);
	expect(arr.includes(4)).toBe(true);
	expect(arr.includes(5)).toBe(false);
	expect(arr.includes(6)).toBe(true);
	expect(arr.includes(7)).toBe(true);
	expect(arr.includes(8)).toBe(true);
	expect(arr.includes(9)).toBe(true);

	// Test manual traversal to make sure content of virtual set items are valid.
	const arr2 = [];
	expect(await (await getRootSetContainer()).$(readProperty, 'type')).toBe(SetFormat.type);
	let currItem = await (await getRootSetContainer()).$(readProperty, 'head');
	while (isFragment(currItem)) {
		// eslint-disable-next-line no-await-in-loop
		expect(await currItem.$(readProperty, 'type')).toBe(SetItemFormat.type);
		// eslint-disable-next-line no-await-in-loop
		const { next, value } = await currItem.$(read);
		// eslint-disable-next-line no-await-in-loop
		arr2.push(await value.$(readProperty, 'lokijs set emulation test'));
		currItem = next;
	}

	expect(arr2.length).toBe(9);
	expect(arr2.includes(0)).toBe(true);
	expect(arr2.includes(1)).toBe(true);
	expect(arr2.includes(2)).toBe(true);
	expect(arr2.includes(3)).toBe(true);
	expect(arr2.includes(4)).toBe(true);
	expect(arr2.includes(5)).toBe(false);
	expect(arr2.includes(6)).toBe(true);
	expect(arr2.includes(7)).toBe(true);
	expect(arr2.includes(8)).toBe(true);
	expect(arr2.includes(9)).toBe(true);
});
