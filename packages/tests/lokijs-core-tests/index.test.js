import { expect, test } from '@jest/globals';

import baseScope from '@interstices/environment';
import { required, METADATA_SUBJECT_KEY } from '@interstices/base-formats';
import { installLokiJSCoreExtended } from '@interstices/lokijs-core';

async function asyncIteratorToArray(asyncIterator) {
	const results = [];
	for await (const result of asyncIterator) {
		results.push(result);
	}

	return results;
}

const TXTFORMAT = {
	type: 'lokijs-core-test-txtformat',
	txt: required(String),
};

const COMMENTFORMAT = {
	type: 'lokijs-core-test-commentformat',
	[METADATA_SUBJECT_KEY]: required(TXTFORMAT),
	comment: required(String),
};

test('lokijs-core tests', async () => {
	const scope = baseScope
		.apply(installLokiJSCoreExtended)
		.addFormats([TXTFORMAT, COMMENTFORMAT]);

	const { read, readProperty, create, update, writeMapping, readMapping, queryRootSet } = scope.API.getAll();

	// ------ writing fragments ------
	const fragment = await create({ type: TXTFORMAT.type, txt: 'hello' });
	await writeMapping(COMMENTFORMAT, fragment, { comment: 'comment 1' });
	expect(await fragment.$(read)).toEqual({ type: TXTFORMAT.type, txt: 'hello' });
	expect(await (await readMapping(COMMENTFORMAT, fragment)).$(readProperty, 'comment'))
		.toBe('comment 1');

	// ------ modifying fragments ------
	await update(fragment, { txt: 'hi' });
	await writeMapping(COMMENTFORMAT, fragment, { comment: 'modified comment 1' });
	expect(await fragment.$(readProperty, 'txt')).toBe('hi');
	expect(await (await readMapping(COMMENTFORMAT, fragment)).$(readProperty, 'comment'))
		.toBe('modified comment 1');

	// ------- dump ---------
	const dump = (await asyncIteratorToArray(await queryRootSet({})));
	// 	.map(({ fragment, content }) => ({ $loki: fragment.getIdObject().tail, content }));
	// console.log(JSON.stringify(dump, null, 2));
	expect(dump.length).toBe(2);
});
