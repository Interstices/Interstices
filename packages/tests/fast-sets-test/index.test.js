import { expect, test } from '@jest/globals';

import baseScope from '@interstices/environment';
import { installMongoDBCore, installMongoDBCoreExtensions } from '@interstices/mongodb-core';
import installBaseListPlugin from '@interstices/base-list-handler';
import installFastSetHandler from '@interstices/fast-sets';
import installBaseFormats from '@interstices/base-formats';

// TODO: make sure we support adding fragments to the set (and not just strings/primitives).
// TODO: test that everything works even if multiple sets share some values.

test('fast sets', async () => {
	const scope = baseScope
		.apply(installMongoDBCore)
		.apply(installMongoDBCoreExtensions)
		.apply(installBaseFormats)
		.apply(installBaseListPlugin)
		.apply(installFastSetHandler);

	// Same tests as in base-set-handler-test
	const { createSet, closeMongoDB } = scope.API.getAll();

	const set = await createSet();
	const MOCK_VALUES = [
		'1st item',
		'2nd item',
		'3rd item',
		'2nd item',
		'4th item',
	];

	await Promise.all(MOCK_VALUES.map(async value => {
		await set.add(value);
	}));

	expect(await set.getSize()).toBe(4);

	for (const value of MOCK_VALUES) {
		// eslint-disable-next-line no-await-in-loop
		expect(await set.has(value)).toBe(true);
	}

	await Promise.all([
		set.remove('1st item'),
		set.remove('3rd item'),
		set.remove('3rd item'),
		set.remove('xxxxxx'),
	]);

	expect(await set.getSize()).toBe(2);

	expect(await set.has('1st item')).toBe(false);
	expect(await set.has('2nd item')).toBe(true);
	expect(await set.has('3rd item')).toBe(false);
	expect(await set.has('4th item')).toBe(true);

	await closeMongoDB();
});
