import { expect, test, beforeAll } from '@jest/globals';
import baseScope from '@interstices/environment';
import { installMongoDBCoreExtended } from '@interstices/mongodb-core';
import installBaseFormats from '@interstices/base-formats';
import installTestFormats, { TestTextFragmentFormat } from '@interstices/test-formats';

let scope;

beforeAll(async () => {
	// We generally use a new db for every unit tests, so that
	// (1) test data is kept separate, making it easier to inspect
	// (2) we prevent race conditions when running multiple tests that each
	//     clear the database during setup.
	scope = baseScope
		.apply(installMongoDBCoreExtended, { dbName: 'mongodb-query-test' })
		.apply(installBaseFormats)
		.apply(installTestFormats);

	const { getFragmentsDB } = scope.unsafeScope.API.getAll();

	(await getFragmentsDB()).deleteMany({}); // Clear database
});

test('test mongodb query plugin', async () => {
	const { create, queryRootSet, closeMongoDB } = scope.API.getAll();

	await create({
		type: TestTextFragmentFormat.type,
		timestamp: 1626928396000,
		name: 'Text File 1',
		content: 'This is text file 1.',
	});

	await create({
		type: TestTextFragmentFormat.type,
		timestamp: 1626928397000,
		name: 'Text File 2',
		content: 'This is text file 2.',
	});

	await create({
		type: TestTextFragmentFormat.type,
		timestamp: 1626928398000,
		name: 'Text File 3',
		content: 'This is text file 3.',
	});

	// Retrieve most recent text files first
	const results = queryRootSet({ type: TestTextFragmentFormat.type }, [['timestamp', 'descending']], 10);
	let i = 3;
	for await (const { content } of results) {
		expect(content.content.includes(i)).toBe(true);
		i--;
	}

	await closeMongoDB();
});
