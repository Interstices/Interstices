/**
 * Make sure parallel writes don't cause @interstices/mongodb-core/db.js to open multiple mongodb connections.
 */
import baseScope from '@interstices/environment';
import installMongoDBCore from '@interstices/mongodb-core';

(async function () {
	const scope = baseScope
		.apply(installMongoDBCore);

	const { create, closeMongoDB } = scope.API.getAll();

	await Promise.all(['a', 'b', 'c'].map(async value => create({ 'mongodb parallel writes test': value })));

	await closeMongoDB();
	// If multiple mongodb connections were opened, then calling closeMongoDB would only close one connection
	// and you will see the process hang afterwards since there are still open connections.
})();
