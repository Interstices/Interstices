import baseScope from '@interstices/environment';
import { expect, test } from '@jest/globals';

test('environment extension', () => {
	const extended = baseScope.extend({
		NAMESPACE: 'extended.namespace',
		API: {
			someMethod: () => 'some method',
		},
	});

	const extended2 = extended.metaExtend(ScopeClass => class extends ScopeClass {
		extend(props) {
			return super.extend({
				overridedExtend: props.overridedExtend ? props.overridedExtend + 1 : 1,
				...props,
			});
		}

		extendAPI(methods) {
			return this.extend({ API: { ...this.API, ...methods } });
		}
	});

	const extended3 = extended2
		.extendAPI({
			anotherMethod: () => 'another method',
		})
		.extend({
			NAMESPACE: 'extended3.namespace',
		});

	expect(extended3.API.someMethod()).toBe('some method');
	expect(extended3.API.anotherMethod()).toBe('another method');

	expect(extended.NAMESPACE).toBe('extended.namespace'); // Make sure original property was preserved.
	expect(extended2.NAMESPACE).toBe('extended.namespace'); // Make sure property as inherited.
	expect(extended3.NAMESPACE).toBe('extended3.namespace'); // Make sure property was overridden.
});
