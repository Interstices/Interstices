// https://github.com/facebook/jest/issues/5356#issuecomment-942573724
export default {
	haste: {
		enableSymlinks: true,
	},
	watchman: false,
};
