import baseScope from '@interstices/environment';
import { installMongoDBCoreExtended } from '@interstices/mongodb-core';
import installBaseFormats from '@interstices/base-formats';
import installBaseListPlugin from '@interstices/base-list-handler';

// TODO: make sure we support adding fragments to the list.
// TODO: test that everything works even if multiple lists share some values.

(async function () {
	const scope = baseScope
		.apply(installMongoDBCoreExtended)
		.apply(installBaseFormats)
		.apply(installBaseListPlugin);

	const { createBaseList, closeMongoDB } = scope.API.getAll();

	const list = await createBaseList();
	const first = await list.insertAfter(null, 'first item');
	const second = await list.insertAfter(first, 'second item');
	const third = await list.insertAfter(second, 'third item');
	const fourth = await list.insertAfter(third, 'fourth item');
	await list.insertAfter(fourth, 'fifth item');

	for await (const itemValue of list.values()) {
		console.log(itemValue);
	}

	console.log('removing first and third items...');

	await list.removeItemAfter(null);
	await list.removeItemAfter(second);

	for await (const itemValue of list.values()) {
		console.log(itemValue);
	}

	console.log('finished!');
	await closeMongoDB();
})();
