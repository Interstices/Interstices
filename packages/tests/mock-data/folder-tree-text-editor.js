import { TestFolderTreeFormat, TestFolderFormat, TestTextFragmentFormat } from '@interstices/test-formats';

/**
 * Structure looks like:
 *
 *  Folder 1
 *  ├── Folder 2
 *  │   ├── Text Fragment 1
 *  │   └── Text Fragment 2
 *  ├── Folder 3
 *  └── Text Fragment 3
 *
 * Self-contained fragment collections like this can actually be treated like file systems
 * in their own right. So when we "load" this data into Loki DB, we are actually just
 * translating the javascript filesystem into a Loki-backed filesystem.
 */
export default {
	0: {
		type: TestFolderTreeFormat.type,
		rootFolder: { ref: 1 },
		createdTS: 1626942302584,
	},
	1: { // Root folder
		type: TestFolderFormat.type,
		timestamp: 1626942302584,
		name: 'Folder 1',
		children: [{ ref: 2 }, { ref: 3 }, { ref: 6 }],
	},
	2: {
		type: TestFolderFormat.type,
		timestamp: 1626853623012,
		name: 'Folder 2',
		children: [{ ref: 4 }, { ref: 5 }],
	},
	3: {
		type: TestFolderFormat.type,
		timestamp: 1626818345914,
		name: 'Folder 3',
		children: [],
	},
	4: {
		type: TestTextFragmentFormat.type,
		timestamp: 1626928396729,
		name: 'Text File 1',
		content: 'This is text file 1.',
	},
	5: {
		type: TestTextFragmentFormat.type,
		timestamp: 1626897261963,
		name: 'Text File 2',
		content: 'Some text for text file 2.',
	},
	6: {
		type: TestTextFragmentFormat.type,
		timestamp: 1626910637103,
		name: 'Text File 3',
		content: 'Hello, goodbye.',
	},
};
