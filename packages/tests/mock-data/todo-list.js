import { TodoItemContentFormat, TodoFormat } from '@interstices/base-formats';

/**
 * Self-contained fragment collections like this can actually be treated like file systems
 * in their own right. So when we "load" this data into Loki DB, we are actually just
 * translating the javascript filesystem into a Loki-backed filesystem.
 */
export default {
	1: { // Root folder
		type: TodoFormat.type,
		title: 'My Todo List',
		items: [{ ref: 2 }, { ref: 3 }],
	},
	2: {
		type: TodoItemContentFormat.type,
		completed: true,
		content: 'Walk',
	},
	3: {
		type: TodoItemContentFormat.type,
		completed: false,
		content: 'Run',
	},
};
