import { expect, test, beforeAll } from '@jest/globals';

import baseScope from '@interstices/environment';
import { installMongoDBCore, installMongoDBCoreExtensions } from '@interstices/mongodb-core';
import installBaseListPlugin from '@interstices/base-list-handler';
import installFastSetHandler from '@interstices/fast-sets';
import installQueryWithinSet from '@interstices/query-within-set';
import installBaseFormats from '@interstices/base-formats';
import installTestFormats, { TestTextFragmentFormat } from '@interstices/test-formats';

let scope;

beforeAll(async () => {
	// We generally use a new db for every unit tests, so that
	// (1) test data is kept separate, making it easier to inspect
	// (2) we prevent race conditions when running multiple tests that each
	//     clear the database during setup.
	scope = baseScope
		.apply(installMongoDBCore, { dbName: 'query-within-set-test' })
		.apply(installMongoDBCoreExtensions)
		.apply(installBaseListPlugin)
		.apply(installFastSetHandler)
		.apply(installQueryWithinSet)
		.apply(installBaseFormats)
		.apply(installTestFormats);

	const { getFragmentsDB } = scope.unsafeScope.API.getAll();

	(await getFragmentsDB()).deleteMany({}); // Clear database
});

test('query within set', async () => {
	// Same tests as in base-set-handler-test
	const { create, createSet, queryWithinSet, getRootSetContainer, closeMongoDB } = scope.API.getAll();

	const setHandler = await createSet();
	const rootSet = await getRootSetContainer();

	const txt1 = await create({
		type: TestTextFragmentFormat.type,
		timestamp: 1626928396000,
		name: 'Text File 1',
		content: 'This is text file 1.',
	});

	const txt2 = await create({
		type: TestTextFragmentFormat.type,
		timestamp: 1626928397000,
		name: 'Text File 2',
		content: 'This is text file 2.',
	});

	const txt3 = await create({
		type: TestTextFragmentFormat.type,
		timestamp: 1626928398000,
		name: 'Text File 3',
		content: 'This is text file 3.',
	});

	await Promise.all([txt1, txt2].map(async value => {
		await setHandler.add(value);
	}));

	const regularSetResults = queryWithinSet(
		{ type: TestTextFragmentFormat.type }, // Query
		[['timestamp', 'descending']], // Descending order
		10, // Limit
		await setHandler.getSetContainer(), // Set to query
	);

	expect((await regularSetResults.next()).value.fragment.getIdString()).toBe(txt2.getIdString());
	expect((await regularSetResults.next()).value.fragment.getIdString()).toBe(txt1.getIdString());
	expect((await regularSetResults.next()).done).toBe(true);

	const rootSetResults = queryWithinSet(
		{ type: TestTextFragmentFormat.type }, // Query
		[['timestamp', 'descending']], // Descending order
		10, // Limit
		rootSet,
	);

	expect((await rootSetResults.next()).value.fragment.getIdString()).toBe(txt3.getIdString());
	expect((await rootSetResults.next()).value.fragment.getIdString()).toBe(txt2.getIdString());
	expect((await rootSetResults.next()).value.fragment.getIdString()).toBe(txt1.getIdString());
	expect((await rootSetResults.next()).done).toBe(true);

	await closeMongoDB();
});
