export default {
	// Since Jest doesn't support symlinks, we have to manually specify
	// all the places to look for @interstices dependencies.
	// See https://github.com/facebook/jest/issues/1477#issuecomment-243113401
	moduleNameMapper: {
		'(@interstices/.*)': [
			'<rootDir>/node_modules/$1',
			'<rootDir>/node_modules/@interstices/mongodb-core/node_modules/$1',
		],
	},

	// See https://github.com/facebook/jest/issues/5356#issuecomment-942573724
	haste: {
		enableSymlinks: true,
	},
	watchman: false,
};
