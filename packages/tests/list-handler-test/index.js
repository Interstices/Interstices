import baseScope from '@interstices/environment';
import { installMongoDBCoreExtended } from '@interstices/mongodb-core';
import installBaseFormats from '@interstices/base-formats';
import installBaseListPlugin from '@interstices/base-list-handler';
import installListPlugin from '@interstices/list-handler';

// TODO: make sure lists support adding fragments to the list.
// TODO: test that everything works even if multiple lists share some values.
(async function () {
	const scope = baseScope
		.apply(installMongoDBCoreExtended)
		.apply(installBaseFormats)
		.apply(installBaseListPlugin)
		.apply(installListPlugin);

	const { createList, closeMongoDB } = scope.API.getAll();

	const list = await createList();
	const second = await list.push('second item');
	// eslint-disable-next-line no-unused-vars
	const first = await list.unshift('first item');
	const third = await list.insertAfter(second, 'third item');
	// eslint-disable-next-line no-unused-vars
	const fourth = await list.insertAfter(third, 'fourth item');
	await list.push('fifth item');

	console.log(`List root id: ${list.baseList.container.getIdString()}`);

	for await (const item of list.asIterable()) {
		const prevItem = await list.getPrev(item);
		console.log(`${await list.getValue(item)} has back-pointer to ${prevItem && await list.getValue(prevItem)}`);
	}

	console.log('removing first and third and fifth items...');

	await list.shift();
	await list.remove(third);

	await list.pop();

	for await (const itemValue of list.values()) {
		console.log(itemValue);
	}

	console.log('finished!');
	await closeMongoDB();
})();
